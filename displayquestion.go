package qgraph

import (
    "fmt"
    qu "gitlab.com/dgricci/qgraph/util"
    qt "gitlab.com/dgricci/qgraph/types"
)

// DisplayQuestion method writes question's text and, if necessary
// choices when the state is q multiple choices question.
//
func (q *QGraph) DisplayQuestion ( s *qt.State) {
    writer := s.OutputPeripheral()
    if writer == nil {
        qu.Log().Println(qu.T("unsupportedOutput"), s.Output().Name())
        return
    }
    if qt.NONE.Is(s.Output()) || s.Text() == "" {
        return
    }
    fmt.Fprintf(writer, qu.T("enterAnswer"))
    fmt.Fprintf(writer, "%s :\n", s.Text())
    if !s.Question().IsSimple() {
        mcq := s.Question().(*qt.ChoicesQuestion)
        if mcq.Display() {
            lc := mcq.Choices()
            for i, c := range lc {
               fmt.Fprintf(writer, "%2d.- %s\n", i, c.(*qt.Choice).Text())
            }
            if mcq.Learn() {
                fmt.Fprintf(writer, "%2d.- %s\n", len(lc)+1, qu.T("learnChoice"))
            }
        }
    }
}
