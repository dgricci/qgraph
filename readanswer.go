package qgraph

import (
    "fmt"
    "io"
    "strings"
    "strconv"
    "regexp"
    "reflect"

    qu "gitlab.com/dgricci/qgraph/util"
    qt "gitlab.com/dgricci/qgraph/types"
)

var (
    rTrue = regexp.MustCompile(`\A((O|o)(ui)?|(Y|y)(es)?)\z`)
    rFalse = regexp.MustCompile(`\A((N|n)(o(n?))?)\z`)
)
// ReadAnswer method reads the answer.
// It returns nil in case to answer is given or found.
//
func (q *QGraph) ReadAnswer ( s *qt.State ) (answer interface{}) {
    reader := s.InputPeripheral()
    if reflect.ValueOf(reader).IsNil() {// reader can be *os.File, *types.Buffer ...
                                        // testing directly == nil fails !
        if qt.MEMOBUFFER.Is(s.Input()) {
            qu.Log().Printf(qu.T("storageZoneNotFound"), s.ZoneID())
        }
        return nil
    }
    //if reader == os.Stdin {//usefull ?
    //    qu.Fflush(reader)
    //}
    b := make([]byte, 1)
    var resp string
    for _, err := reader.Read(b) ; b[0] != '\n' ; _, err = reader.Read(b) {
        if err != nil {//EOF?
            if err == io.EOF { break }
            qu.Log().Println(err)
            return
        }
        resp += string([]byte{b[0]})
    }
    if len(resp) > 0 {
        answer = resp
    }
    return
}

// ConvertAnswer checks whether the answer conforms to the expected format and
// returns the converted answer accordingly :
//
// - INT : int64
//
// - DBL : float64
//
// - BOOL : bool
//
// - STRING : string
//
// - IInINTERVAL : int64
//
// - DInINTERVAL : float64
//
// For multiple choices question, the converted answer is uint64
//
func (q *QGraph) ConvertAnswer ( s *qt.State, answer interface{} ) (interface{}, error) {
    var resp string
    switch answer.(type) {
    case []byte :
        resp = string(answer.([]byte)[:])
    case string :
        resp = answer.(string)
    default :
        return answer, nil
    }
    resp = strings.TrimSpace(resp)
    if s.Question().IsSimple() {
        qs := s.Question().(* qt.SimpleQuestion)
        t := qs.ResponseType()
        switch t {
        case qt.INT, qt.IInINTERVAL   :
            i, err := strconv.ParseInt(resp,10,64)
            if err != nil {
                return i, err
            }
            if t == qt.IInINTERVAL && !qs.IsInInterval(i) {
                err = fmt.Errorf(qu.T("valueNotInInterval", resp))
                return i, err
            }
            return i, nil
        case qt.DBL, qt.DInINTERVAL   :
            f, err := strconv.ParseFloat(resp,64)
            if err != nil {
                return f, err
            }
            if t == qt.DInINTERVAL && !qs.IsInInterval(f) {
                err = fmt.Errorf(qu.T("valueNotInInterval", resp))
                return f, err
            }
            return f, nil
        case qt.BOOL                    :
            if rTrue.MatchString(resp) { return true, nil }
            if rFalse.MatchString(resp) { return false, nil }
            b, err := strconv.ParseBool(resp)
            if err != nil {
                return b, err
            }
            return b, nil
        case qt.STRING                  :
            fallthrough
        default                         :
            return resp, nil
        }
    }
    i, err := strconv.ParseUint(resp,10,64)
    if err != nil {
        return 0, err
    }
    n := uint64(len(s.Question().(* qt.ChoicesQuestion).Choices()))
    if s.Question().(* qt.ChoicesQuestion).Learn() {
        n++
    }
    if i > n {
        err = fmt.Errorf(qu.T("invalidChoiceAnswer", resp))
        return 0, err
    }
    return i, nil
}
