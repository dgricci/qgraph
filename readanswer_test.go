package qgraph

import (
    "testing"
    "fmt"
    "os"

    qt "gitlab.com/dgricci/qgraph/types"
)

var (
    A *QGraph
)

func TestAnswerInt64 ( t *testing.T ) {
    // file with answers
    fn := fmt.Sprintf("%s%c%s", os.TempDir(), os.PathSeparator, "stdinInt64.txt")
    in, err := os.Create(fn)
    if err != nil {
        t.Errorf("Create : %s\n", err.Error())
    }
    defer func () {
        os.Remove(fn)
    }()
    // Simulate user's inputs
    var (
      s1 = "not an integer"
      s2 = "42"
    )
    _, err = in.WriteString(s1+"\n"+s2+"\n")
    if err != nil {
        t.Errorf("WriteString : %s\n", err.Error())
    }
    err = in.Close()
    if err != nil {
        t.Errorf("Close : %s\n", err.Error())
    }
    in, err = os.Open(fn)
    if err != nil {
        t.Errorf("Open : %s\n", err.Error())
    }

    //
    pn := A.Automat()
    ssi := qt.NewState(pn)
    ssi.SetID("INT")
    ssi.SetInput(qt.INPUT)
    ssi.SetInputPeripheral(in)
    ssi.SetOutput(qt.OUTPUT)
    ssiq := qt.NewSimpleQuestion(ssi)
    ssiq.SetResponseType(qt.INT)
    ssi.SetQuestion(ssiq)
    pn.SetState(ssi)
    //
    var line interface{}
    if line = A.ReadAnswer(ssi) ; line == nil {
        t.Errorf("Shouldn't' have failed to read '%s'", s1)
    } else {
        if line.(string) != s1 {
            t.Errorf("Expected string '%s', found '%v'", s1, line)
        }
        if _, err := A.ConvertAnswer(ssi,line) ; err == nil {
            t.Errorf("Should not have been converted to int : '%s'", line)
        }
    }
    if line = A.ReadAnswer(ssi) ; line == nil {
        t.Errorf("Shouldn't have failed to read '%s'", s2)
    } else {
        if line.(string) != s2 {
            t.Errorf("Expected string '%s', found '%v'", s2, line)
        }
        var (
            h2g2 interface{}
            err error
        )
        if h2g2, err = A.ConvertAnswer(ssi, line) ; err != nil {
            t.Errorf("Should have been converted to int : '%s'", line)
        }
        if h2g2.(int64) != 42 {
            t.Errorf("Hitch Hicker's Guide to the Galaxy failed ...")
        }
        ssiq.SetResponseType(qt.IInINTERVAL)
        ssiq.SetInterval(false,int64(41),int64(42),false)
        if h2g2, err = A.ConvertAnswer(ssi, line) ; err == nil {
            t.Errorf("Should not been converted to int in interval '%s': '%s'", ssiq.IntervalToString(), line)
        }
        ssiq.SetInterval(false,int64(41),int64(42),true)
        if h2g2, err = A.ConvertAnswer(ssi, line) ; err != nil {
            t.Errorf("Should have been converted to int in interval '%s': '%s'", ssiq.IntervalToString(), line)
        }
    }
    if line = A.ReadAnswer(ssi) ; line != nil {//test EOF
        t.Errorf("Should have failed, found '%v'", line)
    }
    in.Close()
    if line = A.ReadAnswer(ssi) ; line != nil {//test error when reading !
        t.Errorf("Should have failed, found '%v'", line)
    }
}

func TestAnswerFloat64 ( t *testing.T ) {
    // file with answers
    fn := fmt.Sprintf("%s%c%s", os.TempDir(), os.PathSeparator, "stdinFloat64.txt")
    in, err := os.Create(fn)
    if err != nil {
        t.Errorf("Create : %s\n", err.Error())
    }
    defer func () {
        os.Remove(fn)
    }()
    // Simulate user's inputs
    var (
      s1 = "pi is not here"
      s2 = "3.14"
    )
    _, err = in.WriteString(s1+"\n"+s2+"\n")
    if err != nil {
        t.Errorf("WriteString : %s\n", err.Error())
    }
    err = in.Close()
    if err != nil {
        t.Errorf("Close : %s\n", err.Error())
    }
    in, err = os.Open(fn)
    if err != nil {
        t.Errorf("Open : %s\n", err.Error())
    }
    defer func () {
        in.Close()
    }()

    //
    pn := A.Automat()
    ssf := qt.NewState(pn)
    ssf.SetID("FLOAT")
    ssf.SetInput(qt.INPUT)
    ssf.SetInputPeripheral(in)
    ssf.SetOutput(qt.OUTPUT)
    ssfq := qt.NewSimpleQuestion(ssf)
    ssfq.SetResponseType(qt.DBL)
    ssf.SetQuestion(ssfq)
    pn.SetState(ssf)
    //
    var line interface{}
    if line = A.ReadAnswer(ssf) ; line == nil {
        t.Errorf("Shouldn't' have failed to read '%s'", s1)
    } else {
        if line.(string) != s1 {
            t.Errorf("Expected string '%s', found '%v'", s1, line)
        }
        if _, err := A.ConvertAnswer(ssf,line) ; err == nil {
            t.Errorf("Should not have been converted to float : '%s'", line)
        }
    }
    if line = A.ReadAnswer(ssf) ; line == nil {
        t.Errorf("Shouldn't have failed to read '%s'", s2)
    } else {
        if line.(string) != s2 {
            t.Errorf("Expected string '%s', found '%v'", s2, line)
        }
        var (
            pi interface{}
            err error
        )
        if pi, err = A.ConvertAnswer(ssf, line) ; err != nil {
            t.Errorf("Should have been converted to float : '%s'", line)
        }
        if pi.(float64) - 3.14 > 0.001 {
            t.Errorf("Pi failed ...")
        }
        ssfq.SetResponseType(qt.DInINTERVAL)
        ssfq.SetInterval(false,float64(3.13),float64(3.14),false)
        if pi, err = A.ConvertAnswer(ssf, line) ; err == nil {
            t.Errorf("Should not been converted to int in interval '%s': '%s'", ssfq.IntervalToString(), line)
        }
        ssfq.SetInterval(false,float64(3.13),float64(3.15),true)
        if pi, err = A.ConvertAnswer(ssf, line) ; err != nil {
            t.Errorf("Should have been converted to int in interval '%s': '%s'", ssfq.IntervalToString(), line)
        }
    }
}

func TestAnswerBoolean ( t *testing.T ) {
    // file with answers
    fn := fmt.Sprintf("%s%c%s", os.TempDir(), os.PathSeparator, "stdinBoolean.txt")
    in, err := os.Create(fn)
    if err != nil {
        t.Errorf("Create : %s\n", err.Error())
    }
    defer func () {
        os.Remove(fn)
    }()
    // Simulate user's inputs
    var (
      s = []struct{k string;v bool}{
          {k:"Yeap"   , v:false}, 
          {k:"Y"      , v:true},
          {k:"y"      , v:true},
          {k:"Yes"    , v:true},
          {k:"yes"    , v:true},
          {k:"Noob"   , v:false},
          {k:"N"      , v:true},
          {k:"n"      , v:true},
          {k:"No"     , v:true},
          {k:"no"     , v:true},
          {k:"Oui!"   , v:false},
          {k:"O"      , v:true},
          {k:"o"      , v:true},
          {k:"Oui"    , v:true},
          {k:"oui"    , v:true},
          {k:"Nonnnnn", v:false},
          {k:"Non"    , v:true},
          {k:"non"    , v:true},
          {k:"2"      , v:false},
          {k:"1"      , v:true},
          {k:"T"      , v:true},
          {k:"True"   , v:true},
          {k:"t"      , v:true},
          {k:"true"   , v:true},
          {k:"0"      , v:true},
          {k:"F"      , v:true},
          {k:"False"  , v:true},
          {k:"f"      , v:true},
          {k:"false"  , v:true},
      }
    )
    for _, w := range s {
      _, err = in.WriteString(w.k+"\n")
    }
    if err != nil {
        t.Errorf("WriteString : %s\n", err.Error())
    }
    err = in.Close()
    if err != nil {
        t.Errorf("Close : %s\n", err.Error())
    }
    in, err = os.Open(fn)
    if err != nil {
        t.Errorf("Open : %s\n", err.Error())
    }
    defer func () {
        in.Close()
    }()

    //
    pn := A.Automat()
    ssb := qt.NewState(pn)
    ssb.SetID("BOOL")
    ssb.SetInput(qt.INPUT)
    ssb.SetInputPeripheral(in)
    ssb.SetOutput(qt.OUTPUT)
    ssbq := qt.NewSimpleQuestion(ssb)
    ssbq.SetResponseType(qt.BOOL)
    ssb.SetQuestion(ssbq)
    pn.SetState(ssb)
    //
    var line interface{}
    for _, w := range s {
        if line = A.ReadAnswer(ssb) ; line == nil {
            t.Errorf("Shouldn't' have failed to read '%s'", w.k)
        } else {
            if line.(string) != w.k {
                t.Errorf("Expected string '%s', found '%v'", w.k, line)
            }
            _, err = A.ConvertAnswer(ssb,line)
            if w.v && err != nil {
                t.Errorf("Should have been converted to bool : '%s'", line)
            }
            if !w.v && err == nil {
                t.Errorf("Should not have been converted to bool : '%s'", line)
            }
        }
    }
}

func TestConvertOther ( t *testing.T ) {
    // Simulate user's inputs
    var (
      s1 = "this is a string"
      s2 = []byte{'t','h','i','s',' ','i','s',' ','a',' ','[',']','b','y','t','e'}
      s3 = int64(42)
      s4 = float64(3.14)
      s5 = true
    )
    //
    pn := A.Automat()
    sso := qt.NewState(pn)
    sso.SetID("STRING")
    ssoq := qt.NewSimpleQuestion(sso)
    sso.SetQuestion(ssoq)
    pn.SetState(sso)
    //
    var (
        line interface{}
        err error
    )
    line, err = A.ConvertAnswer(sso,s1)
    if line.(string) != s1 || err != nil {
        t.Errorf("Should have been converted to string : '%s'", s1)
    }
    line, err = A.ConvertAnswer(sso,s2)
    if line.(string) != string(s2) || err != nil {
        t.Errorf("Should have been converted to string from []byte : '%s'", string(s2))
    }
    line, err = A.ConvertAnswer(sso,s3)
    if line.(int64) != s3 || err != nil {
        t.Errorf("Should have been kept as is int64 : '%d'", s3)
    }
    line, err = A.ConvertAnswer(sso,s4)
    if line.(float64) - s4 > 0.001 || err != nil {
        t.Errorf("Should have been kept as is float64 : '%f'", s4)
    }
    line, err = A.ConvertAnswer(sso,s5)
    if line.(bool) == false || err != nil {
        t.Errorf("Should have been kept as is bool : '%t'", s5)
    }
}

func TestAnswerMCQ ( t *testing.T ) {
    // file with answers
    fn := fmt.Sprintf("%s%c%s", os.TempDir(), os.PathSeparator, "stdinMCQ.txt")
    in, err := os.Create(fn)
    if err != nil {
        t.Errorf("Create : %s\n", err.Error())
    }
    defer func () {
        os.Remove(fn)
    }()
    // Simulate user's inputs
    var (
      s1 = "1"
      s2 = "3"
      s3 = "2"
      s4 = "3"
      s5 = "-1"
    )
    _, err = in.WriteString(s1+"\n"+s2+"\n"+s3+"\n"+s4+"\n"+s5+"\n")
    if err != nil {
        t.Errorf("WriteString : %s\n", err.Error())
    }
    err = in.Close()
    if err != nil {
        t.Errorf("Close : %s\n", err.Error())
    }
    in, err = os.Open(fn)
    if err != nil {
        t.Errorf("Open : %s\n", err.Error())
    }
    defer func () {
        in.Close()
    }()

    //
    pn := A.Automat()
    smc := qt.NewState(pn)
    smc.SetID("MCQ")
    smc.SetText("Make a choice")
    smc.SetInputPeripheral(in)
    mcq := qt.NewChoicesQuestion(smc)
    c := qt.NewChoice(smc)
    c.SetText("1st")
    c.SetMemorize(false)
    mcq.AddChoice(c)
    c = qt.NewChoice(smc)
    c.SetText("2nd")
    c.SetMemorize(true)
    mcq.AddChoice(c)
    smc.SetQuestion(mcq)
    pn.SetState(smc)
    //
    var line interface{}
    if line = A.ReadAnswer(smc) ; line == nil {
        t.Errorf("Shouldn't' have failed to read '%s'", s1)
    } else {
        if _, err := A.ConvertAnswer(smc,line) ; err != nil {
            t.Errorf("Should have been converted to uint64 : '%s'", line)
        }
    }
    if line = A.ReadAnswer(smc) ; line == nil {
        t.Errorf("Shouldn't have failed to read '%s'", s2)
    } else {
        if _, err := A.ConvertAnswer(smc,line) ; err == nil {
            t.Errorf("Should not have been converted to uint64 (learn is false) : '%s'", line)
        }
    }
    mcq.SetLearn(true)
    if line = A.ReadAnswer(smc) ; line == nil {
        t.Errorf("Shouldn't have failed to read '%s'", s3)
    } else {
        if _, err := A.ConvertAnswer(smc,line) ; err != nil {
            t.Errorf("Should have been converted to uint64 : '%s'", line)
        }
    }
    if line = A.ReadAnswer(smc) ; line == nil {
        t.Errorf("Shouldn't have failed to read '%s'", s4)
    } else {
        if _, err := A.ConvertAnswer(smc,line) ; err != nil {
            t.Errorf("Should have been converted to uint64 (learn is true) : '%s'", line)
        }
    }
    if line = A.ReadAnswer(smc) ; line == nil {
        t.Errorf("Shouldn't have failed to read '%s'", s5)
    } else {
        if _, err := A.ConvertAnswer(smc,line) ; err == nil {
            t.Errorf("Should not have been converted to uint64 : '%s'", line)
        }
    }
}

func TestAnswerMemo ( t *testing.T ) {
    //
    pn := A.Automat()
    ssm := qt.NewState(pn)
    ssm.SetID("MEMO")
    ssm.SetInput(qt.MEMOBUFFER)
    ssm.SetOutput(qt.OUTPUT)
    ssmq := qt.NewSimpleQuestion(ssm)
    ssmq.SetResponseType(qt.INT)
    ssm.SetQuestion(ssmq)
    pn.SetState(ssm)
    //
    var line interface{}
    if line = A.ReadAnswer(ssm) ; line != nil {
        t.Errorf("Should have failed, found '%v'", line)
    }
}

func setup () {
    A = NewQGraph("",nil)
}

func teardown() {
}

func TestMain(m *testing.M) {
    // call flag.Parse() here if TestMain uses flags
    setup()

    res := m.Run()

    teardown()

    os.Exit(res)
}
