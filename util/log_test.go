package util

import (
    "testing"
    "bytes"
    "fmt"
    "strings"
)


func TestLogOnError (t *testing.T) {
    if !LogOnError(nil) {
        t.Errorf("Should not have failed with nil")
    }
    var buf bytes.Buffer
    Log().SetOutput(&buf)
    Log().SetFlags(0)
    Log().SetPrefix("ERR")
    if LogOnError(fmt.Errorf("%s", "Fake")) {
        t.Errorf("Should not have failed with fake error")
    }
    if strings.TrimSpace(buf.String()) != Log().Prefix()+"Fake" {
        t.Errorf("Expected '"+Log().Prefix()+"Fake' error, got '%q'", strings.TrimSpace(buf.String()))
    }
}