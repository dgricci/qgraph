package util

import (
    "io"
)


// Fflush function empties an input stream.
//
func Fflush ( f io.Reader ) {
    b := make([]byte, 1)
    for n, _ := f.Read(b) ; n == 1 ; n, _ = f.Read(b) {
        if b[0] == '\n' { return }
    }
}
