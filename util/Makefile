# Copyright 2018 RICHARD Didier
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Adapted from https://github.com/vincentbernat/hellogopher/blob/master/Makefile
#
# To test without executing commands :
# $ make -n
# To debug :
# $ make --debug=[a|b|v|i|j|m]
#

# top package
PKGROOT := gitlab.com/dgricci/qgraph
PKGNAME	 = util
PKGDIR	 = ./$(PKGNAME)
# one can invoke with a sub-package by passing PACKAGE to the command line
PACKAGE ?= $(PKGROOT)/$(PKGNAME)
SUBPKGS	 =

include project.mk

# Default target (first target not starting with .)
all: main

packages:

main:: ; $(info … built done !)

vendor:

help:
	$(info $(S) In order to build and install this package $(PACKAGE), you need to have golang installed.)
	$(info $(S) One must run the make command from the $(PKGROOT) directory.)
	$(info $(S) One may use its own GOPATH environment for buidling this package :)
	$(info $(T)$$ make GOPATH=$${GOPATH} -f ./util/Makefile)
	$(info $(T)By default, this makefile uses is own GOPATH, current value is :)
	$(info $(T)$$ GOPATH=$(GOPATH) -f ./util/Makefile)
	$(info $(S) To run the tests, use :)
	$(info $(T)$$ make [GOPATH=$(GOPATH)] -f ./util/Makefile tests)
	$(info $(T)One can pass GOTESTOPTS in the form GOTESTOPTS="options" as in:)
	$(info $(T)$$ make [GOPATH=$(GOPATH)] GOTESTOPTS="-v" -f ./util/Makefile tests)
	$(info $(T)One can pass GOARCH and GOOS for cross-compiling :)
	$(info $(T)$$ GOARM=7 make [GOPATH=$(GOPATH)] GOARCH=arm GOOS=linux -f ./util/Makefile install)
	$(info Default target both runs the tests and installs the package)
	@$(MAKE) --silent check-env
