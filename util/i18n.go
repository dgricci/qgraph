package util

import (
    "github.com/nicksnyder/go-i18n/i18n"
    "github.com/nicksnyder/go-i18n/i18n/language"
)

var (
    // T function to translate. Use SetLang to assign.
    T = i18n.IdentityTfunc()
    // L current translation language. Use SetLang to assign.
    L *language.Language
)

// SetLang function assigns a language for translations.
// Most language tags are a two character language code (ISO 639-1),
// optionally followed by a dash and a two character country code (ISO 3166-1)
// (e.g. en, fr-fr).
func SetLang (lang string) error {
    _t, _l, err := i18n.TfuncAndLanguage(lang)
    if err == nil {
        T= _t
        L= _l
    } else {
        _= LogOnError(err)
    }
    return err
}
