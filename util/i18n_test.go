package util

import (
    "testing"
)

func TestSetLang (t *testing.T) {
    const id = "supportedLanguage"
    if T(id) != id {
        t.Errorf("No translation function set, but translation active ?!")
    }
    err := SetLang("en")
    if err != nil {
        t.Errorf(err.Error())
    }
    var en= T(id)
    if en == id {
        t.Errorf("'%s' not translated in %s ?!", id, L.String())
    }
    err = SetLang("it")
    if err == nil {
        t.Errorf("it supported ?!")
    }
    err = SetLang("fr")
    if err != nil {
        t.Errorf("fr not supported !?")
    }
    var fr= T(id)
    if fr == id {
        t.Errorf("'%s' not translated in %s ?!", id, L.String())
    }
    if en == fr {
        t.Errorf("'%s' not translated at all ?!", id)
    }
}