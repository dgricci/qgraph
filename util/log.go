// Package util for logging things
package util

import (
    "log"
)

var (
  qlog *log.Logger
  // LoggerPrefix for logged messages
  LoggerPrefix = "[qgraph]: "
)

// LogOnError function helper for logging error
//
func LogOnError (err error) bool {
    if err != nil {
        qlog.Printf("%v", err)
        return false
    }
    return true
}

// Log function returns the underlaying logger
//
func Log () *log.Logger {
    return qlog
}
