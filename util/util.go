// Package util for logging things
package util

import (
    "os"
    "log"
    "github.com/nicksnyder/go-i18n/i18n"
)

// init function to set global variables
//
func init () {
    i18n.MustLoadTranslationFile("i18n/en-us.all.json")
    i18n.MustLoadTranslationFile("i18n/fr-fr.all.json")

    qlog= log.New(os.Stderr, LoggerPrefix, log.LstdFlags)
    qlog.Println("gitlab.com/dgricci/qgraph/util loaded")
}

