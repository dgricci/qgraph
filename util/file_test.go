package util

import (
    "os"
    "io"
    "fmt"
    "testing"
)

func readInt ( in *os.File ) (userI int) {
    for {
        fmt.Println("Please enter an integer: ")
        _, err := fmt.Fscan(in, &userI)
        if err == nil || err == io.EOF {
            break
        }
        fmt.Printf("Sorry, invalid input (%s).\n", err.Error())
        Fflush(in)
    }
    return
}

func TestFflush ( t *testing.T ) {
    fn := fmt.Sprintf("%s%c%s", os.TempDir(), os.PathSeparator, "stdin.txt")
    in, err := os.Create(fn)
    if err != nil {
        t.Errorf("Create : %s\n", err.Error())
    }
    defer func () {
        os.Remove(fn)
    }()
    // Simulate user's inputs
    _, err = in.WriteString("not an integer\n42\n")
    if err != nil {
        t.Errorf("WriteString : %s\n", err.Error())
    }
    err = in.Close()
    if err != nil {
        t.Errorf("Close : %s\n", err.Error())
    }
    in, err = os.Open(fn)
    if err != nil {
        t.Errorf("Open : %s\n", err.Error())
    }
    h2g2 := readInt(in)
    if h2g2 != 42 {
        t.Errorf("Fflush failed, got %d, expected %d\n", h2g2, 42)
    }
}
