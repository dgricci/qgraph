package qgraph

import (
    "fmt"
    "regexp"
    qu "gitlab.com/dgricci/qgraph/util"
    qt "gitlab.com/dgricci/qgraph/types"
)

var (
    cmdPrefix = `*`
    cmd = compileCommandPrefix()
)

// compileCommandPrefix function parses the regular expression for analyzing commands
// and returns the associated RegExp object.
//
func compileCommandPrefix ( ) *regexp.Regexp {
    return regexp.MustCompile(`\A(:?`+regexp.QuoteMeta(cmdPrefix)+`)([[:upper:]]+)((:?\s*)(.+))?\z`)
}

// CommandPrefix function returns the current character used as command prefix.
// Default character command prefix is `*`.
//
func CommandPrefix ( ) byte {
    return ([]byte(cmdPrefix))[0]
}

// ChangeCommandPrefix function assigns the command prefix character
// and compiles the regular expression for analyzing commands.
//
func ChangeCommandPrefix ( prefix byte ) {
    cmdPrefix= string(([]byte{prefix})[:])
    cmd = compileCommandPrefix()
}

// CommandParser returns the source text of the regular expression used for
// analyzing commands.
//
func CommandParser ( ) *regexp.Regexp {
    return cmd
}

// ExecCommand function checks answer and, possibly, executes the
// command passed through the answer. A command is a special answer
// that begins with a command prefix character (`cmdPfx`) and is
// followed by uppercase letters and, possibly, an argument. `s` is the current
// State.
//
// So far, the following commands are supported :
// 
// - cmdPfx`PRED` : back to previous state in the history (one step undo);
//
// - cmdPfx`FIRST` : back to the first state in the history (undo at all steps);
//
// - cmdPfx`LANG aa` : change language (two character language code as in ISO 639-1,
//   optionally followed by a dash and a two character country code as in ISO 3166-1);
//
// - cmdPfx`QUIT fn?` : exits the automaton safely, saving history and storage zones
//   when no parameter is given; if fn (file's name) is ">", then prints out the
//   automaton without saving informations.
//   If fn is neither ">" nor "" (or missing), then saves in the given file's name.
//
// - cmdPfx`ABORT` : quits the automaton without saving !
//
func (q *QGraph) ExecCommand ( s *qt.State, answer interface{} ) (sn *qt.State, err error) {
    var resp string
    switch answer.(type) {
    case []byte :
        resp = string(answer.([]byte)[:])
    case string :
        resp = answer.(string)
    default :
        return nil, nil
    }
    ans := cmd.FindStringSubmatch(resp)
    if len(ans)!=6 {//[answer * command spacesAndArgs spaces args]
        return nil, nil
    }
    switch ans[2] {
    case "PRED" :
        if s == nil { return nil, fmt.Errorf(qu.T("unknownState"), resp) }
        // pop current state
        if s.Automat().Queue().Pop() != "" {
            // find backward the first state, not under condition
            for id := s.Automat().Queue().Pop() ; id != "" ; id = s.Automat().Queue().Pop() {
                sn = s.Automat().StateByID(id)
                if sn.Text() == "" {
                    // under condition state, skip it
                    continue
                }
                // Clean memo sn, it becomes the current node
                sn.Automat().Memo().DelZone(sn.ZoneID())
                sn.Automat().Queue().Append(sn.ID())
                return // sn, nil
            }
        }
        // no state found, push back current state in history
        s.Automat().Queue().Append(s.ID())
        sn = nil
        err = fmt.Errorf(qu.T("noPredState"), s.ID())
    case "FIRST":
        if s == nil { return nil, fmt.Errorf(qu.T("unknownState"), resp) }
        // pop current state
        // rewind the history up to the first state
        // remove current state
        _ = s.Automat().Queue().Pop()
        for id := s.Automat().Queue().Pop() ; id != "" ; id = s.Automat().Queue().Pop() {
            sn = s.Automat().StateByID(id)
            if sn.Text() == "" {
                // under condition state, skip it
                continue
            }
            // Clean memo sn
            sn.Automat().Memo().DelZone(sn.ZoneID())
        }
        // sn is the last state found in history
        if sn == nil {//there was no State in history ...
            return q.startState("", true)
        }
        err = nil
    case "LANG" :
        language := ans[5]
        if language != "" {
            err = qu.SetLang(language)
            if err != nil { sn = s }
        } else {
            sn = nil
            err = fmt.Errorf(qu.T("missingLanguage"))
        }
    case "QUIT" :
        // save back to disk and fall through ABORT case
        switch fileName := ans[5] ; fileName {
        case "" :// no argument given
            err = q.Save()
        case ">":// force os.Stdout
            err = q.Save("")
        default :
            err = q.Save(fileName)
        }
        fallthrough
    case "ABORT":
        sn = qt.EXITSTATE
    default     :
        return nil, fmt.Errorf(qu.T("unknownCommand"))
    }
    return sn, err
}
