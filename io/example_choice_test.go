package io

import (
    "fmt"

    "gitlab.com/dgricci/qgraph/types"
)

func ExampleNewAutomatFromFile_choice() {
    A := NewAutomatFromFile("testdata/simple-automat.yml",tasks)
    state := A.StateByID("Profile")
    question := state.Question()
    choicesQ := question.(*types.ChoicesQuestion)
    choices := choicesQ.Choices()
    if len(choices) > 0 {
        fmt.Println("Choices OK")
    }
    var c *types.Choice
    for _, choice := range choices {
        if choice.IsChoice() {
            fmt.Println("  Choice OK")
            c = choice.(*types.Choice)
            if c.Task() == nil {
                fmt.Println("  Task OK")
            }
            if c.NextState() != nil {
                fmt.Println("  Next OK")
            }
            if c.AnswerState() == state {
                fmt.Println("  State OK")
            }
            if c.Text() != "" {
                fmt.Println("  "+c.Text())
            }
            switch c.Text() {
            case "User" :
                if !c.Memorize() {
                    fmt.Println("  Memorize OK")
                }
            case "Admin":
                if c.Memorize() {
                    fmt.Println("  Memorize OK")
                }
            }
        }
    }
    // Output:
    // Choices OK
    //   Choice OK
    //   Task OK
    //   Next OK
    //   State OK
    //   User
    //   Memorize OK
    //   Choice OK
    //   Task OK
    //   Next OK
    //   State OK
    //   Admin
    //   Memorize OK
}

