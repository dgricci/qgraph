package io

import (
    "fmt"

    "gitlab.com/dgricci/qgraph/types"
    "gitlab.com/dgricci/qgraph/io/yaml"
)

func ExampleNewAutomatFromFile_simpleQuestion() {
    A := NewAutomatFromFile("testdata/simple-automat.yml",tasks)
    state := A.StateByID("Login")
    question := state.Question()
    if question.IsSimple() {
        fmt.Println("Simple OK")
    }
    simpleQ := question.(*types.SimpleQuestion)
    if simpleQ.NextState() != nil {
        fmt.Println("Next OK")
    }
    if simpleQ.QuestionState() == state {
        fmt.Println("State OK")
    }
    if simpleQ.ResponseType().Is(types.STRING) {
        fmt.Println(types.STRING.Name())
    }
    fuzz, unit := simpleQ.ResponseMetadata()
    if fuzz == 0 && unit == "" {
        fmt.Println("No metadata")
    }
    _, low, upper, _ := simpleQ.Interval()
    if low == nil && upper == nil {
        fmt.Println("No interval")
    }
    // Output:
    // Simple OK
    // Next OK
    // State OK
    // STRING
    // No metadata
    // No interval
}

func ExampleNewAutomatFromFile_simpleQuestion_intervalInt() {
    A := NewAutomatFromFile("testdata/age.yml",nil)
    state := A.StateByID("Age")
    question := state.Question()
    simpleQ := question.(*types.SimpleQuestion)
    fuzz, unit := simpleQ.ResponseMetadata()
    if fuzz == 1 && unit == "year" {
        fmt.Println("Metadata OK")
    }
    LowIn, low, upper, UpperIn := simpleQ.Interval()
    if !LowIn && low == int64(0) && UpperIn && upper == int64(120) {
        fmt.Println("Interval OK")
    }
    if simpleQ.NextState() == types.EXITSTATE {
        fmt.Println("Next OK")
    }
    if yaml.SerializeToYAML(A) != nil {
        fmt.Println("Serialize OK")
    }
    // Output:
    // Metadata OK
    // Interval OK
    // Next OK
    // Serialize OK
}

func ExampleNewAutomatFromFile_simpleQuestion_intervalFloat() {
    A := NewAutomatFromFile("testdata/height.yml",nil)
    state := A.StateByID("Height")
    question := state.Question()
    simpleQ := question.(*types.SimpleQuestion)
    fuzz, unit := simpleQ.ResponseMetadata()
    if fuzz == 1 && unit == "cm" {
        fmt.Println("Metadata OK")
    }
    LowIn, low, upper, UpperIn := simpleQ.Interval()
    if LowIn && low == float64(20.0) && !UpperIn && upper == float64(250.0) {
        fmt.Println("Interval OK")
    }
    if simpleQ.NextState() == types.EXITSTATE {
        fmt.Println("Next OK")
    }
    if yaml.SerializeToYAML(A) != nil {
        fmt.Println("Serialize OK")
    }
    // Output:
    // Metadata OK
    // Interval OK
    // Next OK
    // Serialize OK
}
