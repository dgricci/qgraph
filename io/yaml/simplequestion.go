package yaml

import (
    "fmt"
    "strings"

    "gitlab.com/dgricci/qgraph/util"
    "gitlab.com/dgricci/qgraph/types"
)

// processSimpleQuestion sets attributes of `SimpleQuestion` from unmarshalled YAML.
//
func processSimpleQuestion (s *types.State, ysq map[interface{}]interface{}) error {
    var exists bool
    var err error
    var lb, ub []interface{}
    var l, u map[interface{}]interface{} = nil, nil
    sq := types.NewSimpleQuestion(s)
    if existsMapKeyNotNil(ysq,"Type") && ysq["Type"].(string) != "" {
        var r types.ResponseType
        if r, err = types.RESPONSETYPES.Get(ysq["Type"].(string)) ; err != nil {
            return fmt.Errorf(util.T("yamlUnknownResponseType"),
                              "QS", "Type", ysq["Type"].(string), strings.Join(types.ResponseTypesNames,", "))
        }
        sq.SetResponseType(r)
    }
    if existsMapKeyNotNil(ysq,"Fuzziness") { sq.SetResponseFuzziness(ysq["Fuzziness"].(int)) }
    // TODO(me) : real unit check ?
    if existsMapKeyNotNil(ysq,"Unit") { sq.SetResponseUnit(ysq["Unit"].(string)) }
    if existsMapKey(ysq,"LowerBound") {
        if sq.ResponseType() != types.IInINTERVAL && sq.ResponseType() != types.DInINTERVAL {
            return fmt.Errorf(util.T("yamlTypeIntervalNotSet"),"LowerBound")
        }
        if lb, exists = ysq["LowerBound"].([]interface{}) ; exists {
            l= lb[0].(map[interface{}]interface{})
        }
        if len(l) == 0 {
            return fmt.Errorf(util.T("yamlMustBeSet"), "SQ", "LowerBound")
        }
    }
    if existsMapKey(ysq,"UpperBound") {
        if sq.ResponseType() != types.IInINTERVAL && sq.ResponseType() != types.DInINTERVAL {
            return fmt.Errorf(util.T("yamlTypeIntervalNotSet"),"UpperBound")
        }
        if ub, exists = ysq["UpperBound"].([]interface{}) ; exists {
            u= ub[0].(map[interface{}]interface{})
        }
        if len(u) == 0 {
            return fmt.Errorf(util.T("yamlMustBeSet"), "SQ", "UpperBound")
        }
    }
    if (len(l) == 0 && len(u) > 0) || (len(l) > 0 && len(u) == 0) {
        return fmt.Errorf(util.T("yamlIncorrectLowerUpperBoundSettings"), "SQ", "LowerBound", "SQ", "UpperBound")
    }
    if len(l) != 0 && !existsMapKey(l,"Value") {
        return fmt.Errorf(util.T("yamlMustBeSet"), "LowerBound", "Value")
    }
    if len(u) != 0 && !existsMapKey(u,"Value") {
        return fmt.Errorf(util.T("yamlMustBeSet"), "UpperBound", "Value")
    }
    if len(l) != 0 {// u is not empty too
        var il, iu bool = false, false
        if existsMapKeyNotNil(l,"Include") {
            il= l["Include"].(bool)
        }
        if existsMapKeyNotNil(u,"Include") {
            iu= u["Include"].(bool)
        }
        // yaml.v2 only converts into int and float64 for numbers 
        if sq.ResponseType() == types.IInINTERVAL {
            var lo, up int64
            switch l["Value"].(type) {
            case int        : lo = int64(l["Value"].(int))
            //case int32      : lo = int64(l["Value"].(int32))
            //case int64      : lo = int64(l["Value"].(int64))
            default         : return fmt.Errorf(util.T("yamlIntegerValueExpected"), "LowerBound", l["Value"])
            }
            switch u["Value"].(type) {
            case int        : up = int64(u["Value"].(int))
            //case int32      : up = int64(u["Value"].(int32))
            //case int64      : up = int64(u["Value"].(int64))
            default         : return fmt.Errorf(util.T("yamlIntegerValueExpected"), "UpperBound", u["Value"])
            }
            sq.SetInterval(il,lo,up,iu)
        } else {
            var lo, up float64
            switch l["Value"].(type) {
            case int        : lo = float64(l["Value"].(int))
            //case float32    : lo = float64(l["Value"].(float32))
            case float64    : lo = float64(l["Value"].(float64))
            default         : return fmt.Errorf(util.T("yamlFloatingValueExpected"), "LowerBound", l["Value"])
            }
            switch u["Value"].(type) {
            case int        : up = float64(u["Value"].(int))
            //case float32    : up = float64(u["Value"].(float32))
            case float64    : up = float64(u["Value"].(float64))
            default         : return fmt.Errorf(util.T("yamlFloatingValueExpected"), "UpperBound", u["Value"])
            }
            sq.SetInterval(il,lo,up,iu)
        }
    } else {
        if sq.ResponseType() == types.IInINTERVAL || sq.ResponseType() == types.DInINTERVAL {
            return fmt.Errorf(util.T("yamlIntervalMustBeSet"), sq.ResponseType().Name())
        }
    }
    if existsMapKeyNotNil(ysq,"Task") && ysq["Task"].(string) != "" {
        ctask := ysq["Task"].(string)
        err = checkIfTaskExists(s.Automat().Tasks(), ctask, util.T("yamlTaskNotFound"), "SQ", "Task", ctask)
        if err != nil {
            return err
        }
        sq.SetTaskID(ctask)
    }
    if existsMapKeyNotNil(ysq,"Next") && ysq["Next"].(string) != "" {
        sq.SetNextStateID(ysq["Next"].(string))
    }
    s.SetQuestion(sq)
    return nil
}

// serializeSimpleQuestionToYAML method marshals a `SimpleQuestion` struct.
//
func serializeSimpleQuestionToYAML (q *types.SimpleQuestion) []byte {
    yBytes := []byte("    SQ:\n")
    w := false
    if !q.ResponseType().Is(types.STRING) {
        w= true
        yBytes= append(yBytes, []byte("      Type: " + q.ResponseType().Name() + "\n")...)
    }
    f, u := q.ResponseMetadata()
    if f != 0 {
        w= true
        yBytes= append(yBytes, []byte("      Fuzzyness: " + fmt.Sprintf("%d\n",f))...)
    }
    if u != "" {
        w= true
        yBytes= append(yBytes, []byte("      Unit: " + u + "\n")...)
    }
    ilw, lw, up, iup := q.Interval()
    if lw != nil {
        w= true
        yBytes= append(yBytes, []byte("      LowerBound:\n")...)
        yBytes= append(yBytes, []byte("        - Value: " + fmt.Sprintf("%v\n", lw))...)
        yBytes= append(yBytes, []byte("          Include: " + fmt.Sprintf("%t\n", ilw))...)
        yBytes= append(yBytes, []byte("      UpperBound:\n")...)
        yBytes= append(yBytes, []byte("        - Value: " + fmt.Sprintf("%v\n", up))...)
        yBytes= append(yBytes, []byte("          Include: " + fmt.Sprintf("%t\n", iup))...)
    }
    if q.NextState() != types.EXITSTATE {
        w= true
        yBytes= append(yBytes, []byte("      Next: " + q.NextStateID() + "\n")...)
    }
    if !w {
        yBytes= append(yBytes, []byte("      Type:\n")...)
    }
    return yBytes
}
