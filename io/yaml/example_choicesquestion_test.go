package yaml

import (
    "fmt"

    "gitlab.com/dgricci/qgraph/types"
)

func ExampleNewAutomatFromYAML_choicesQuestion() {
    A := NewAutomatFromYAML(`%YAML 1.1
---
States:
- State:
  - Id: Login
    ToQ: What's is your identifier ?
    DefaultAnswer:
      Next: Login
    Prolog: Banner
    MemoZone: LID
    SQ:
      Next: Profile
- State:
  - Id: Profile
    ToQ: Choose your profile
    MemoZone: PID
    MCQ:
      Choices:
      - Choice:
        - ToC: User
          Memorize: false
          Next: Hello
      - Choice:
        - ToC: Admin
          Memorize: true
          Next: Hello
- State:
  - Id: Hello
    MemoZone: LID
    Input: MEMOBUFFER
    Epilog: Greetings
    SQ:
      Type:
...
`,tasks)
    state := A.StateByID("Profile")
    question := state.Question()
    if !question.IsSimple() {
        fmt.Println("Choices OK")
    }
    choicesQ := question.(*types.ChoicesQuestion)
    if choicesQ.NextState() == nil {
        fmt.Println("Next OK")
    }
    if choicesQ.QuestionState() == state {
        fmt.Println("State OK")
    }
    if choicesQ.Display() {
        fmt.Println("Display OK")
    }
    if !choicesQ.Learn() {
        fmt.Println("Learn OK")
    }
    if len(choicesQ.Choices()) == 2 {
        fmt.Println("Choices OK")
    }
    // Output:
    // Choices OK
    // Next OK
    // State OK
    // Display OK
    // Learn OK
    // Choices OK
}

func ExampleNewAutomatFromYAML_choicesQuestion_learn() {
    A := NewAutomatFromYAML(`%YAML 1.1
---
States:
- State:
  - Id: Learn
    ToQ: What is your first name ?
    DefaultAnswer:
      Task: Dump
      Next:
    MCQ:
      Learn: true
      Choices:
      - Choice:
        - ToC: Alice
        Next: Learn
      - Choice:
        - ToC: Bob
        Next: Learn
`, map[string]types.FTask{
    "Dump"  :   func (s *types.State, r interface{}) error {
        if SerializeToYAML(s.Automat()) == nil {
            return fmt.Errorf("Serialize KO")
        }
        return nil
    },
})
    state := A.StateByID("Learn")
    question := state.Question()
    if !question.IsSimple() {
        fmt.Println("Choices OK")
    }
    choicesQ := question.(*types.ChoicesQuestion)
    if choicesQ.Learn() {
        fmt.Println("Learn OK")
    }
    for _, c := range choicesQ.Choices() {
        if c.IsChoice() {
            fmt.Println("IsChoice OK")
        }
        fmt.Println(c.(*types.Choice).Text())
    }
    if SerializeToYAML(A) != nil {
        fmt.Println("Serialize OK")
    }
    // Unordered output:
    // Choices OK
    // Learn OK
    // IsChoice OK
    // Alice
    // IsChoice OK
    // Bob
    // Serialize OK
}
