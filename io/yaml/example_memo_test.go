package yaml

import (
    "fmt"
)

func ExampleNewAutomatFromYAML_memo() {
    A := NewAutomatFromYAML(`%YAML 1.1
---
States:
- State:
  - Id: S1
    ToQ: Step 1
    DefaultAnswer:
      Next:           # quit
    SQ:
      Next: S2
- State:
  - Id: S2
    ToQ: Step 2
    Memo: Z
    MCQ:
      Choices:
      - Choice:
        - ToC: A
          Memorize: true
          Next: S1
        - ToC: B
          Next: S2
Memo:
- Zone: Z
  Buff: A
...
`,nil)
    memo := A.Memo()
    for z, b := range memo.Zones() {
        fmt.Println(z)
        fmt.Printf("%v\n",b.Value())
    }
    if SerializeToYAML(A) != nil {
        fmt.Println("Serialize OK")
    }
    // Output:
    // Z
    // A
    // Serialize OK
}
