// Package yaml deserialization/serialization of types needed for qgraph package
package yaml

import (
    "fmt"

    "gopkg.in/yaml.v2"
    "github.com/nicksnyder/go-i18n/i18n"

    qu "gitlab.com/dgricci/qgraph/util"
    qt "gitlab.com/dgricci/qgraph/types"
)

// existsMapKey function checks whether a key exists in a mapping or not.
//
func existsMapKey (y map[interface{}]interface{}, key string) bool {
    if _, exists := y[key] ; !exists {
        return false
    }
    return true
}

// existsMapKeyNotNil function checks that a key exists in a mapping and its value is not nil.
//
func existsMapKeyNotNil (y map[interface{}]interface{}, key string) bool {
    if existsMapKey(y,key) && y[key] != nil {
        return true
    }
    return false
}

// checkIfTaskExists checks whether a task given by its id exists.
//
func checkIfTaskExists (tasks map[string]qt.FTask, taskName string, f string, what ...interface{}) error {
    if _, exists := tasks[taskName] ; !exists {
        return fmt.Errorf(f, what...)
    }
    return nil
}

// NewAutomatFromYAML Automat constructor.
// Reads a YAML string (Cf. http://yaml.org/spec/1.2/spec.html) and build an
// automaton. The automaton information is checked.
//
func NewAutomatFromYAML (data interface{}, tasks map[string]qt.FTask) *qt.Automat {
    var err error

    switch t := data.(type) {
    case string:
        return NewAutomatFromYAML([]byte(data.(string)), tasks)
    case []byte:
        bytes := data.([]byte)
        if bytes == nil || len(bytes) == 0 {
            err = fmt.Errorf(qu.T("yamlEmptyDataset"))
        }
        if !qu.LogOnError(err) { return nil }
        y := make(map[interface{}]interface{})
        err = yaml.Unmarshal(bytes, &y)
        if !qu.LogOnError(err) { return nil }
        return NewAutomatFromYAML(y,tasks)
    case map[interface{}]interface{}:
        return processYAML(data.(map[interface{}]interface{}),tasks)
    default:
        qu.LogOnError(fmt.Errorf(qu.T("yamlUnsupportedType"), t))
        return nil
    }
}

// SerializeToYAML marshals `Automat` struct to YAML.
// Returns an array of bytes representing the YAML data.
//
func SerializeToYAML (a *qt.Automat) []byte {
    yBytes := []byte("%YAML 1.1\n---\nStates:\n")
    for _, sid := range a.States() {
        s := a.StateByID(sid)
        var ysBytes []byte
        if ysBytes = serializeStateToYAML(s) ; ysBytes != nil {
            yBytes= append(yBytes, ysBytes...)
        }
    }
    if q := a.Queue() ; q != nil {
        var yqBytes []byte
        if yqBytes = serializeQueueToYAML(q) ; yqBytes != nil {
            yBytes= append(yBytes, yqBytes...)
        }
    }
    if m := a.Memo() ; m != nil {
        var ymBytes []byte
        if ymBytes = serializeMemoToYAML(m) ; ymBytes != nil {
            yBytes= append(yBytes, ymBytes...)
        }
    }
    yBytes= append(yBytes,[]byte("...\n")...)
    return yBytes
}

// init function to set global variables
//
func init () {
    i18n.MustLoadTranslationFile("i18n/en-us.all.json")
    i18n.MustLoadTranslationFile("i18n/fr-fr.all.json")

    qu.Log().Println("gitlab.com/dgricci/qgraph/io/yaml loaded")
}
