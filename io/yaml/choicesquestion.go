package yaml

import (
    "fmt"

    qu "gitlab.com/dgricci/qgraph/util"
    qt "gitlab.com/dgricci/qgraph/types"
)

// processMultipleChoicesQuestion sets attributes of `MultipleChoicesQuestion` from unmarshalled YAML.
//
func processMultipleChoicesQuestion (s *qt.State, ymcq map[interface{}]interface{}) error {
    cq := qt.NewChoicesQuestion(s)
    if existsMapKeyNotNil(ymcq,"Display") {
        cq.SetDisplay(ymcq["Display"].(bool))
    }
    if existsMapKeyNotNil(ymcq,"Learn") {
        cq.SetLearn(ymcq["Learn"].(bool))
    }
    if existsMapKeyNotNil(ymcq,"Task") && ymcq["Task"].(string) != "" {
        ctask := ymcq["Task"].(string)
        err := checkIfTaskExists(s.Automat().Tasks(), ctask, qu.T("yamlTaskNotFound"), "MCQ", "Task", ctask)
        if err != nil {
            return err
        }
        cq.SetTaskID(ctask)
    }
    if !existsMapKey(ymcq,"Choices") {
        return fmt.Errorf(qu.T("yamlExpectedKeyword"), "Choices")
    }
     if ymcq["Choices"] == nil {
        return fmt.Errorf(qu.T("yamlEmptyContent"), "Choices")
    }
    if _, exists := ymcq["Choices"].([]interface{}) ; !exists {
        return fmt.Errorf(qu.T("yamlNotASequence"),"Choices")
    }
    if err := processChoices(cq,ymcq["Choices"].([]interface{})) ; err != nil {
        return err
    }
    s.SetQuestion(cq)
    return nil
}

// serializeChoicesQuestionToYAML method marshals a `ChoicesQuestion` struct.
//
func serializeChoicesQuestionToYAML (q *qt.ChoicesQuestion) []byte {
    yBytes := []byte("    MCQ:\n")
    if !q.Display() {
        yBytes= append(yBytes, []byte("      Display: " + fmt.Sprintf("%t\n",q.Display()))...)
    }
    if q.Learn() {
        yBytes= append(yBytes, []byte("      Learn: " + fmt.Sprintf("%t\n",q.Learn()))...)
    }
    yBytes= append(yBytes, []byte("      Choices:\n")...)
    var ycBytes []byte
    for _, c := range q.Choices() {
        if ycBytes = serializeChoiceToYAML(c.(*qt.Choice)) ; ycBytes != nil {
            yBytes= append(yBytes, ycBytes...)
        }
    }
    return yBytes
}
