package yaml

import (
    "fmt"

    "gitlab.com/dgricci/qgraph/util"
    "gitlab.com/dgricci/qgraph/types"
)

// processQueue sets attributes of `Queue` from unmarshalled YAML.
//
func processQueue (a *types.Automat, m map[interface{}]interface{}) error {
    var qs string
    if !existsMapKeyNotNil(m, "Queue") { return nil }
    for _, qsid := range m["Queue"].([]interface{}) {
        qs= qsid.(string)
        if a.StateByID(qs) == nil {
            return fmt.Errorf(util.T("yamlStateNotFound"), "Queue", qs)
        }
        a.Queue().Append(qs)
    }
    return nil
}

// serializeQueueToYAML method marshals a `Queue` struct.
//
func serializeQueueToYAML (q *types.Queue) []byte {
    if len(q.History()) > 0 {
        yBytes := []byte("Queue:\n")
        for _, s := range q.History() {
            yBytes= append(yBytes, []byte("- " + s + "\n")...)
        }
        return yBytes
    }
    return []byte("")
}
