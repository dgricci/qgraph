package yaml

import (
    "fmt"

    "gitlab.com/dgricci/qgraph/types"
)

func ExampleNewAutomatFromYAML() {
    A := NewAutomatFromYAML(`%YAML 1.1
---
States:
- State:
  - Id: Login
    ToQ: What's is your identifier ?
    DefaultAnswer:
      Next: Login
    Prolog: Banner
    MemoZone: LID
    SQ:
      Next: Profile
- State:
  - Id: Profile
    ToQ: Choose your profile
    MemoZone: PID
    MCQ:
      Choices:
      - Choice:
        - ToC: User
          Memorize: false
          Next: Hello
      - Choice:
        - ToC: Admin
          Memorize: true
          Next: Hello
- State:
  - Id: Hello
    MemoZone: LID
    Input: MEMOBUFFER
    Epilog: Greetings
    SQ:
      Type:
...
`,tasks)
    for _, id := range A.States() {
        state := A.StateByID(id)
        if state != nil {
            fmt.Println(id)
        }
    }
    if A.StateByID("Foo") == nil {
        fmt.Println("Foo not found")
    }
    if A.Queue() != nil {
        fmt.Println("Queue()")
    }
    if A.Memo() != nil {
        fmt.Println("Memo()")
    }
    if A.Tasks() != nil {
        fmt.Println("Tasks()")
    }
    // Unordered output:
    // Login
    // Profile
    // Hello
    // Foo not found
    // Queue()
    // Memo()
    // Tasks()
}

func ExampleSerializeToYAML() {
    A := NewAutomatFromYAML(`%YAML 1.1
---
States:
- State:
  - Id: FirstName
    ToQ: What is your firstname
    MemoZone: FirstName
    DefaultAnswer:
      Next:                       # no answer ? quit!
    SQ:
      Next: Age
- State:
  - Id: Age
    ToQ: |
      What is your age ?
      (Not your date of birth)
    Prolog: Hello
    MemoZone: Age
    MCQ:
      Choices:
      - Choice:
        - ToC: Less than 18 years old
          Memorize: true
          Next: Height
      - Choice:
        - ToC: 18 years old or more
          Memorize: true
          Next: Height
- State:
  - Id: Height
    ToQ: What is your size
    MemoZone: Size
    SQ:
      Type: DInINTERVAL
      Fuzzyness: 2
      Unit: cm
      LowerBound:
      - Value: 1.60
        Include: true
      UpperBound:
      - Value: 2.50
        Include: false
      Next: Studies
- State:
  - Id: Studies
    ToQ: Have you got your baccalaureat
    MemoZone: Bac
    SQ:
      Type: BOOL
      Next: Check
- State:
  - Id: Check
    Input: MEMOBUFFER
    Output: NONE
    MemoZone: Age
    MCQ:
      Display: false
      Choices:
      - Choice:
        - ToC:    # < 18
          Task: Wow
          Next:
      - Choice:
        - ToC:    # ≥ 18
          Next: Bye
- State:
  - Id: Bye
    ToQ: Do you practice Golang programming language
    Epilog: Conclusion
    SQ:
      Type: BOOL
      Next:
...
`, map[string]types.FTask{
        "Hello"     :   func (s *types.State, r interface{}) error {
            fmt.Printf("Hello '%s' !\n", s.Automat().Memo().Zone("FirstName").Value().(string))
            return nil
        },
        "Wow"       :   func (s *types.State, r interface{}) error {
            fmt.Println("Wow !!!")
            return nil
        },
        "Conclusion":   func (s *types.State, r interface{}) error {
            fmt.Printf("Bye\n")
            return nil
        },
    })
    if A != nil {
        fmt.Println("Unmarshalling OK")
    }
    if SerializeToYAML(A) != nil {
        fmt.Println("Marshalling OK")
    }
    // Output:
    // Unmarshalling OK
    // Marshalling OK
}
