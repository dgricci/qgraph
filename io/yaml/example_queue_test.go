package yaml

import (
    "fmt"
)

func ExampleNewAutomatFromYAML_queue() {
    A := NewAutomatFromYAML(`%YAML 1.1
---
States:
- State:
  - Id: S1
    ToQ: Step 1
    DefaultAnswer:
      Next:           # quit
    SQ:
      Next: S2
- State:
  - Id: S2
    ToQ: Step 2
    MCQ:
      Choices:
      - Choice:
        - ToC: A
          Next: S1
        - ToC: B
          Next: S2
Queue:
- S1
- S2
- S2
...
`,nil)
    queue := A.Queue()
    for _, s := range queue.History() {
        fmt.Println(s)
    }
    if SerializeToYAML(A) != nil {
        fmt.Println("Serialize OK")
    }
    // Output:
    // S1
    // S2
    // S2
    // Serialize OK
}
