package yaml

import (
    qu "gitlab.com/dgricci/qgraph/util"
    qt "gitlab.com/dgricci/qgraph/types"
)

// processDefaultAnswer function sets attributes of `DefaultAnswer` from unmarshalled YAML.
//
func processDefaultAnswer (s *qt.State, o map[interface{}]interface{}) error {
    d := qt.NewDefaultAnswer(s)
    if existsMapKeyNotNil(o,"Task") && o["Task"].(string) != "" {
        dtask := o["Task"].(string)
        err := checkIfTaskExists(s.Automat().Tasks(), dtask, qu.T("yamlTaskNotFound"), "State", "DefaultAnswer", dtask)
        if err != nil {
            return err
        }
      d.SetTaskID(dtask)
    }
    if existsMapKeyNotNil(o,"Next") && o["Next"].(string) != "" {
        d.SetNextStateID(o["Next"].(string))
    }
    s.SetDefaultAnswer(d)
    return nil
}

// serializeDefaultAnswerToYAML method marshals a `DefaultAnswer`.
//
func serializeDefaultAnswerToYAML (a *qt.DefaultAnswer) []byte {
    yBytes := []byte("    DefaultAnswer:\n")
    w := false
    if a.Task() != nil {
        w = true
        yBytes = append(yBytes, []byte("      Task: " + a.TaskID() + "\n")...)
    }
    if a.NextState() != qt.EXITSTATE || !w {
        w = true
        yBytes = append(yBytes, []byte("      Next: " + a.NextStateID() + "\n")...)
    }
    return yBytes
}
