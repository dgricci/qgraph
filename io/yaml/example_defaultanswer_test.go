package yaml

import (
    "fmt"

    "gitlab.com/dgricci/qgraph/types"
)

func ExampleNewAutomatFromYAML_defaultAnswer() {
    A := NewAutomatFromYAML(`
%YAML 1.1
---
States:
- State:
  - Id: Login
    ToQ: What's is your identifier ?
    DefaultAnswer:
      Next: Login
    Prolog: Banner
    MemoZone: LID
    SQ:
      Next: Profile
- State:
  - Id: Profile
    ToQ: Choose your profile
    MemoZone: PID
    MCQ:
      Choices:
      - Choice:
        - ToC: User
          Memorize: false
          Next: Hello
      - Choice:
        - ToC: Admin
          Memorize: true
          Next: Hello
- State:
  - Id: Hello
    MemoZone: LID
    Input: MEMOBUFFER
    Epilog: Greetings
    SQ:
      Type:
...
`,tasks)
    state := A.StateByID("Login")
    answer := state.DefaultAnswer()
    if answer != nil {
        fmt.Println("DefaultAnswer OK")
    }
    if !answer.IsChoice() {
        fmt.Println("IsChoice OK")
    }
    if answer.Task() == nil {
        fmt.Println("Task OK")
    }
    if answer.NextState() != types.EXITSTATE {
        fmt.Println("Next OK")
    }
    if answer.AnswerState() == state {
        fmt.Println("State OK")
    }
    // Output:
    // DefaultAnswer OK
    // IsChoice OK
    // Task OK
    // Next OK
    // State OK
}

func ExampleNewAutomatFromYAML_defaultAnswer_next() {
    A := NewAutomatFromYAML(`%YAML 1.1
---
States:
- State:
  - Id: Hello
    ToQ: Who are you ?
    DefaultAnswer:
      Next:
    SQ:
      Type:
`, nil)
    state := A.StateByID("Hello")
    answer := state.DefaultAnswer()
    if answer != nil {
        fmt.Println("DefaultAnswer OK")
    }
    if !answer.IsChoice() {
        fmt.Println("IsChoice OK")
    }
    if answer.Task() == nil {
        fmt.Println("Task OK")
    }
    if answer.NextState() == types.EXITSTATE {
        fmt.Println("Next OK")
    }
    if answer.AnswerState() != nil {
        fmt.Println("State OK")
    }
    if SerializeToYAML(A) != nil {
        fmt.Println("Serialize OK")
    }
    // Output:
    // DefaultAnswer OK
    // IsChoice OK
    // Task OK
    // Next OK
    // State OK
    // Serialize OK
}
