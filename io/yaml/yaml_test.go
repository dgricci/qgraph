package yaml

import (
    "testing"
    "fmt"
    "os"

    qu "gitlab.com/dgricci/qgraph/util"
    qt "gitlab.com/dgricci/qgraph/types"
)

var (
    tasks = map[string]qt.FTask{
        "Banner"    :   func (s *qt.State, r interface{}) error {
            fmt.Println("Welcome ...")
            return nil
        },
        "Greetings" :   func (s *qt.State, r interface{}) error {
            fmt.Printf("Hello '%s' !\n", r.(string))
            return nil
        },
        "Fake"      :   func (s *qt.State, r interface{}) error {
            fmt.Printf("Fake !\n")
            return nil
        },
    }
    messages = map[string]string{
        "missingYAMLKey"    :   "Should have failed with YAML missing '%s' ?!",
        "emptyYAMLKey"      :   "Should have failed with YAML empty '%s' ?!",
        "missingTask"       :   "Should have failed with task missing '%s' ?!",
        "wrongValue"        :   "Should have failed with YAML wrong '%s' value ?!",
        "shouldntHaveFailed":   "Failed to construct Automat with :\n'%s'",
        "shouldHaveFailed"  :   "Success to construct Automat with :\n'%s'",
    }
    scenarii = map[string]string{
        "BadYaml"           :
`\nbad\n`,
        "MissingStates"     :
`Fake: true`,
        "EmptyStates"       :
`States:
`,
        "MissingState"      :
`States:
 - Oops: foo
 - Oops: bar
`,
        "StatesNotMapping"  :
`States:
   State:
`,
        "StateNotMapping"   :
`States:
- State:
  Oops: foo
`,
        "MissingStateId"    :
`States:
- State:
  - Oops: foo
`,
        "StateNotSequence"  :
`States:
- State:
  - foo
`,
        "MissingProlog"     :
`States:
- State:
  - Id: S1
    Prolog: None
`,
        "MissingEpilog"     :
`States:
- State:
  - Id: S1
    Epilog: None
`,
        "WrongInput"        :
`States:
- State:
  - Id: S1
    Input: foo
`,
        "WrongOutput"       :
`States:
- State:
  - Id: S1
    Output: bar
`,
        "AnswerMissingTask" :
`States:
- State:
  - Id: S1
    DefaultAnswer:
      Task: foo
`,
        "WrongType"         :
`States:
- State:
  - Id: S1
    SQ:
      Type: foo
`,
        "MissingQuestion"   :
`States:
- State:
  - Id: S1
`,
        "EmptySQuestion"    :
`States:
- State:
  - Id: S1
    SQ:
`,
        "SQNotMapping"      :
`States:
- State:
  - Id: Q2
    SQ:
      - Type:
`,
        "EmptyLowerBound"   :
`States:
- State:
  - Id: S1
    SQ:
      Type: IInINTERVAL
      LowerBound:
`,
        "EmptyUpperBound"   :
`States:
- State:
  - Id: S1
    SQ:
      Type: IInINTERVAL
      UpperBound:
`,
        "MissingLowerBound" :
`States:
- State:
  - Id: S1
    SQ:
      Type: IInINTERVAL
      UpperBound:
      - Value: 26
`,
        "MissingUpperBound" :
`States:
- State:
  - Id: S1
    SQ:
      Type: IInINTERVAL
      LowerBound:
      - Value: 1
`,
        "MissingLBValue"    :
`States:
- State:
  - Id: S1
    SQ:
      Type: IInINTERVAL
      LowerBound:
      - Oops: foo
      UpperBound:
      - Value: 26
`,
        "MissingUBValue"    :
`States:
- State:
  - Id: S1
    SQ:
      Type: IInINTERVAL
      LowerBound:
      - Value: 1
      UpperBound:
      - Oops: foo
`,
        "BadTypeLB"         :
`States:
- State:
  - Id: S1
    SQ:
      Type: INT
      LowerBound:
      - Value: 1
`,
        "BadTypeUB"         :
`States:
- State:
  - Id: S1
    SQ:
      Type: INT
      UpperBound:
      - Value: 2
`,
        "MissingBounds"     :
`States:
- State:
  - Id: S1
    SQ:
      Type: IInINTERVAL
`,
        "BadLBIValue"       :
`States:
- State:
  - Id: S1
    SQ:
      Type: IInINTERVAL
      LowerBound:
      - Value: A
      UpperBound:
      - Value: 2
`,
        "BadUBIValue"       :
`States:
- State:
  - Id: S1
    SQ:
      Type: IInINTERVAL
      LowerBound:
      - Value: 1
      UpperBound:
      - Value: Z
`,
        "BadLBFValue"       :
`States:
- State:
  - Id: S1
    SQ:
      Type: DInINTERVAL
      LowerBound:
      - Value: A
      UpperBound:
      - Value: 2.0
`,
        "BadUBFValue"       :
`States:
- State:
  - Id: S1
    SQ:
      Type: DInINTERVAL
      LowerBound:
      - Value: 1.0
      UpperBound:
      - Value: Z
`,
        "MissingTaskSQ"     :
`States:
- State:
  - Id: Q1
    ToQ: What height
    MemoZone: Z1
    Input: INPUT
    Output: OUTPUT
    SQ:
      Type: DInINTERVAL
      Fuzziness: 10
      Unit: cm
      LowerBound:
      - Value: 0.0
        Include: false
      UpperBound:
      - Value: 250
        Include: true
      Task: None
      Next: Q1
`,
        "BadNextSQuestion"  :
`States:
- State:
  - Id: Q1
    MemoZone: Z1
    SQ:
      Type: DInINTERVAL
      Fuzziness: 10
      Unit: cm
      LowerBound:
      - Value: 0.0
        Include: false
      UpperBound:
      - Value: 250.0
        Include: true
      Next: Q2
`,
        "OkSimpleQuestion"  :
`States:
- State:
  - Id: Q1
    ToQ: What height
    MemoZone: Z1
    Input: INPUT
    Output: OUTPUT
    SQ:
      Type: DInINTERVAL
      Fuzziness: 10
      Unit: cm
      LowerBound:
      - Value: 0.0
        Include: false
      UpperBound:
      - Value: 250
        Include: true
      Task: Fake
      Next: Q1
`,
        "EmptyMCQuestion"   :
`States:
- State:
  - Id: Q2
    ToQ: T2
    MCQ:
`,
        "MissingChoices"    :
`States:
- State:
  - Id: Q2
    ToQ: T2
    MCQ:
      Oops: foo
`,
        "EmptyChoices"      :
`States:
- State:
  - Id: Q2
    ToQ: T2
    MCQ:
      Choices:
`,
        "ChoicesNotSequence":
`States:
- State:
  - Id: Q2
    ToQ: T2
    MCQ:
      Choices:
        Choice:
`,
        "ChoicesNotMapping" :
`States:
- State:
  - Id: Q2
    ToQ: T2
    MCQ:
      Choices:
        - Choice
`,
        "MissingChoice"     :
`States:
- State:
  - Id: Q2
    ToQ: T2
    MCQ:
      Choices:
      - OOps: bar
`,
        "EmptyChoice"       :
`States:
- State:
  - Id: Q2
    ToQ: T2
    MCQ:
      Choices:
      - Choice:
`,
        "MissingTaskChoice" :
`States:
- State:
  - Id: Q2
    ToQ: T2
    MCQ:
      Choices:
      - Choice:
        - Task: None
`,
        "WrongStateIdQueue" :
`States:
- State:
  - Id: S1
    ToQ: T1
    SQ:
      Type:
Queue:
- E1
`,
        "EmptyZone"         :
`States:
- State:
  - Id: S1
    ToQ: T1
    SQ:
      Type:
Memo:
- Zone:
`,
        "EmptyAgainZone"    :
`States:
- State:
  - Id: S1
    ToQ: T1
    SQ:
      Type:
Memo:
- Zone: ""
`,
        "MCQNotMapping"     :
`States:
- State:
  - Id: Q2
    ToQ: T2
    MCQ:
      - Choices:
`,
        "ChoiceNotMapping"  :
`States:
- State:
  - Id: Q2
    ToQ: T2
    MCQ:
      Choices:
        - Choice:
          - blah
`,
        "ChoiceNotSequence" :
`States:
- State:
  - Id: Q2
    ToQ: T2
    MCQ:
      Choices:
        - Choice:
            blah:
`,
        "BadNextMCQuestion" :
`States:
- State:
  - Id: Q2
    ToQ: T2
    MCQ:
      Display: true
      Learn: false
      Choices:
      - Choice:
        - ToC: blah
          Memorize: false
          Next: Q1
`,
        "MissingTaskMCQ"    :
`States:
- State:
  - Id: Q2
    ToQ: T2
    MCQ:
      Display: true
      Learn: false
      Task: None
      Choices:
      - Choice:
        - ToC: blah
          Memorize: false
          Task: Fake
          Next: Q2
`,
        "OkMCQuestion"      :
`States:
- State:
  - Id: Q2
    ToQ: T2
    MCQ:
      Display: true
      Learn: false
      Task: Greetings
      Choices:
      - Choice:
        - ToC: blah
          Memorize: false
          Task: Fake
          Next: Q2
`,
        "OkNoXXXlog"        :
`States:
- State:
  - Id: S1
    ToQ: T1
    SQ:
      Type:
`,
        "OkAnswerTask"      :
`States:
- State:
  - Id: S1
    ToQ: T1
    DefaultAnswer:
      Task: Fake
    SQ:
      Type:
`,
        "OkAnswerNext"      :
`States:
- State:
  - Id: S1
    ToQ: T1
    DefaultAnswer:
      Next: S1
    SQ:
      Type:
`,
        "OkEmptyQueue"      :
`States:
- State:
  - Id: S1
    ToQ: T1
    SQ:
      Type:
Queue:
`,
        "OkQueue"           :
`States:
- State:
  - Id: S1
    ToQ: T1
    SQ:
      Type:
Queue:
- S1
`,
        "OkEmptyMemo"       :
`States:
- State:
  - Id: S1
    ToQ: T1
    SQ:
      Type:
Memo:
`,
        "OkMemo"            :
`States:
- State:
  - Id: S1
    ToQ: T1
    SQ:
      Type:
Memo:
- Zone: Z1
  Buff: foo
`,
        "Login"             :
`%YAML 1.1
---

States:
- State:
  - Id: Stage1
    ToQ: What's your name ?
    Prolog: Banner
    Epilog: Greetings
    SQ:
      Type: STRING
`,
    }
)

func TestAutomatWithBadYAML(t *testing.T) {
    var A *qt.Automat

    A =  NewAutomatFromYAML(scenarii["BadYaml"],nil)
    if A != nil {
        t.Errorf("Should have failed with incorrect YAML ?!")
    }
}

func TestAutomatFromString(t *testing.T) {
    var A *qt.Automat

    t.Run("A=MissingStates", func(t *testing.T) {
        A = NewAutomatFromYAML("",tasks)
        if A != nil {
            t.Errorf("Should have failed with empty YAML string ?!")
        }
        A = NewAutomatFromYAML(scenarii["MissingStates"],nil)
        if A != nil {
            t.Errorf(messages["missingYAMLKey"], "States")
        }
        A = NewAutomatFromYAML(scenarii["EmptyStates"],nil)
        if A != nil {
            t.Errorf(messages["emptyYAMLKey"], "State")
        }
        A = NewAutomatFromYAML(scenarii["StatesNotMapping"],nil)
        if A != nil {
            t.Errorf(messages["emptyYAMLKey"], "States")
        }
        A = NewAutomatFromYAML(scenarii["MissingState"],nil)
        if A != nil {
            t.Errorf(messages["missingYAMLKey"], "State")
        }
        A = NewAutomatFromYAML(scenarii["StateNotMapping"],nil)
        if A != nil {
            t.Errorf(messages["missingYAMLKey"], "State[]")
        }
        A = NewAutomatFromYAML(scenarii["StateNotSequence"],nil)
        if A != nil {
            t.Errorf(messages["missingYAMLKey"], "State[0]")
        }
    })
    t.Run("A=MissingFields", func(t *testing.T) {
        A = NewAutomatFromYAML(scenarii["MissingStateId"],nil)
        if A != nil {
            t.Errorf(messages["missingYAMLKey"], "State.Id")
        }
        A = NewAutomatFromYAML(scenarii["MissingProlog"],nil)
        if A != nil {
            t.Errorf(messages["missingTask"], "State.Prolog")
        }
        A = NewAutomatFromYAML(scenarii["MissingEpilog"],nil)
        if A != nil {
            t.Errorf(messages["missingTask"], "State.Epilog")
        }
        A = NewAutomatFromYAML(scenarii["AnswerMissingTask"],nil)
        if A != nil {
            t.Errorf(messages["missingTask"], "State.DefaultAnswer.Task")
        }
        A = NewAutomatFromYAML(scenarii["MissingTaskChoice"],nil)
        if A != nil {
            t.Errorf(messages["missingTask"], "State.MCQ.Choices.Choice.Task")
        }
        A = NewAutomatFromYAML(scenarii["Login"],nil)
        if A != nil {
            t.Errorf("Should have failed with no tasks defined but identified.")
        }
    })
    t.Run("B=WrongValues", func(t *testing.T) {
        A = NewAutomatFromYAML(scenarii["WrongInput"],nil)
        if A != nil {
            t.Errorf(messages["wrongValue"], "State.Input")
        }
        A = NewAutomatFromYAML(scenarii["WrongOutput"],nil)
        if A != nil {
            t.Errorf(messages["wrongValue"], "State.Output")
        }
        A = NewAutomatFromYAML(scenarii["WrongType"],nil)
        if A != nil {
            t.Errorf(messages["wrongValue"], "State.SQ.Type")
        }
        A = NewAutomatFromYAML(scenarii["WrongStateIdQueue"],nil)
        if A != nil {
            t.Errorf(messages["wrongValue"], "Queue.Id")
        }
    })
    t.Run("C=SimpleQuestion", func(t *testing.T) {
        A = NewAutomatFromYAML(scenarii["MissingQuestion"],nil)
        if A != nil {
            t.Errorf(messages["missingTask"], "State.SQ|State.MCQ")
        }
        A = NewAutomatFromYAML(scenarii["EmptySQuestion"],nil)
        if A != nil {
            t.Errorf(messages["emptyYAMLKey"], "State.SQ")
        }
        A = NewAutomatFromYAML(scenarii["SQNotMapping"],nil)
        if A != nil {
            t.Errorf(messages["wrongValue"], "State.SQ{}")
        }
        A = NewAutomatFromYAML(scenarii["EmptyLowerBound"],nil)
        if A != nil {
            t.Errorf(messages["emptyYAMLKey"], "State.SQ.LowerBound")
        }
        A = NewAutomatFromYAML(scenarii["EmptyUpperBound"],nil)
        if A != nil {
            t.Errorf(messages["emptyYAMLKey"], "State.SQ.UpperBound")
        }
        A = NewAutomatFromYAML(scenarii["MissingLowerBound"],nil)
        if A != nil {
            t.Errorf(messages["missingYAMLKey"], "State.SQ.LowerBound")
        }
        A = NewAutomatFromYAML(scenarii["MissingUpperBound"],nil)
        if A != nil {
            t.Errorf(messages["missingYAMLKey"], "State.SQ.UpperBound")
        }
        A = NewAutomatFromYAML(scenarii["MissingLBValue"],nil)
        if A != nil {
            t.Errorf(messages["missingYAMLKey"], "State.SQ.LowerBound.Value")
        }
        A = NewAutomatFromYAML(scenarii["MissingUBValue"],nil)
        if A != nil {
            t.Errorf(messages["missingYAMLKey"], "State.SQ.UpperBound.Value")
        }
        A = NewAutomatFromYAML(scenarii["BadTypeLB"],nil)
        if A != nil {
            t.Errorf(messages["missingYAMLKey"], "State.SQ.Type")
        }
        A = NewAutomatFromYAML(scenarii["BadTypeUB"],nil)
        if A != nil {
            t.Errorf(messages["missingYAMLKey"], "State.SQ.Type")
        }
        A = NewAutomatFromYAML(scenarii["MissingBounds"],nil)
        if A != nil {
            t.Errorf(messages["missingYAMLKey"], "State.SQ.*Bound")
        }
        A = NewAutomatFromYAML(scenarii["BadLBIValue"],nil)
        if A != nil {
            t.Errorf(messages["missingYAMLKey"], "State.SQ.*Bound")
        }
        A = NewAutomatFromYAML(scenarii["BadUBIValue"],nil)
        if A != nil {
            t.Errorf(messages["missingYAMLKey"], "State.SQ.*Bound")
        }
        A = NewAutomatFromYAML(scenarii["BadLBFValue"],nil)
        if A != nil {
            t.Errorf(messages["missingYAMLKey"], "State.SQ.*Bound")
        }
        A = NewAutomatFromYAML(scenarii["BadUBFValue"],nil)
        if A != nil {
            t.Errorf(messages["missingYAMLKey"], "State.SQ.*Bound")
        }
        A = NewAutomatFromYAML(scenarii["MissingTaskSQ"],nil)
        if A != nil {
            t.Errorf(messages["wrongValue"], "State.SQ.Task")
        }
        A = NewAutomatFromYAML(scenarii["BadNextSQuestion"],nil)
        if A != nil {
            t.Errorf(messages["wrongValue"], "State.SQ.Next")
        }
        A = NewAutomatFromYAML(scenarii["OkSimpleQuestion"], tasks)
        if A == nil {
            t.Errorf(messages["shouldntHaveFailed"], scenarii["OkSimpleQuestion"])
        }
    })
    t.Run("D=MultipleChoicesQuestion", func(t *testing.T) {
        A = NewAutomatFromYAML(scenarii["EmptyMCQuestion"],nil)
        if A != nil {
            t.Errorf(messages["emptyYAMLKey"], "State.MCQ")
        }
        A = NewAutomatFromYAML(scenarii["MissingChoices"],nil)
        if A != nil {
            t.Errorf(messages["missingYAMLKey"], "State.Choices")
        }
        A = NewAutomatFromYAML(scenarii["EmptyChoices"],nil)
        if A != nil {
            t.Errorf(messages["emptyYAMLKey"], "State.Choices")
        }
        A = NewAutomatFromYAML(scenarii["ChoicesNotSequence"],nil)
        if A != nil {
            t.Errorf(messages["wrongValue"], "State.Choices[]")
        }
        A = NewAutomatFromYAML(scenarii["ChoicesNotMapping"],nil)
        if A != nil {
            t.Errorf(messages["wrongValue"], "State.Choices{}")
        }
        A = NewAutomatFromYAML(scenarii["MissingChoice"],nil)
        if A != nil {
            t.Errorf(messages["missingYAMLKey"], "State.Choices.Choice")
        }
        A = NewAutomatFromYAML(scenarii["EmptyChoice"],nil)
        if A != nil {
            t.Errorf(messages["emptyYAMLKey"], "State.Choices.Choice")
        }
        A = NewAutomatFromYAML(scenarii["MCQNotMapping"],nil)
        if A != nil {
            t.Errorf(messages["wrongValue"], "State.Choices{}")
        }
        A = NewAutomatFromYAML(scenarii["BadNextMCQuestion"],nil)
        if A != nil {
            t.Errorf(messages["wrongValue"], "State.Choices.Choice.Next")
        }
        A = NewAutomatFromYAML(scenarii["ChoiceNotMapping"],nil)
        if A != nil {
            t.Errorf(messages["wrongValue"], "State.Choices.Choice{}")
        }
        A = NewAutomatFromYAML(scenarii["ChoiceNotSequence"],nil)
        if A != nil {
            t.Errorf(messages["wrongValue"], "State.Choices.Choice[]")
        }
        A = NewAutomatFromYAML(scenarii["MissingTaskMCQ"],tasks)
        if A != nil {
            t.Errorf(messages["wrongValue"], "MCQ.Task")
        }
        A = NewAutomatFromYAML(scenarii["OkMCQuestion"],tasks)
        if A == nil {
            t.Errorf(messages["shouldntHaveFailed"], scenarii["OkMCQuestion"])
        }
    })
    t.Run("E=Queue", func(t *testing.T) {
        A = NewAutomatFromYAML(scenarii["OkEmptyQueue"],nil)
        if A == nil {
            t.Errorf(messages["shouldntHaveFailed"], scenarii["OkEmptyQueue"])
        }
        A = NewAutomatFromYAML(scenarii["OkQueue"],nil)
        if A == nil {
            t.Errorf(messages["shouldntHaveFailed"], scenarii["OkQueue"])
        }
    })
    t.Run("F=Memo", func(t *testing.T) {
        A = NewAutomatFromYAML(scenarii["OkEmptyMemo"],nil)
        if A == nil {
            t.Errorf(messages["shouldntHaveFailed"], scenarii["OkEmptyMemo"])
        }
        A = NewAutomatFromYAML(scenarii["EmptyZone"],nil)
        if A != nil {
            t.Errorf(messages["emptyYAMLKey"], "Memo.Zone")
        }
        A = NewAutomatFromYAML(scenarii["EmptyAgainZone"],nil)
        if A != nil {
            t.Errorf(messages["emptyYAMLKey"], "Memo.Zone")
        }
        A = NewAutomatFromYAML(scenarii["OkMemo"],nil)
        if A == nil {
            t.Errorf(messages["shouldntHaveFailed"], scenarii["OkMemo"])
        }
        if len(A.Memo().Zones()) != 1 {
            t.Errorf(messages["shouldntHaveFailed"], "One zone expected")
        }
    })


    t.Run("G=Ok", func(t *testing.T) {
        A = NewAutomatFromYAML(scenarii["OkNoXXXlog"],nil)
        if A == nil {
            t.Errorf(messages["shouldntHaveFailed"], scenarii["OkNoXXXlog"])
        }
        A = NewAutomatFromYAML(scenarii["OkAnswerTask"],tasks)
        if A == nil {
            t.Errorf(messages["shouldntHaveFailed"], scenarii["OkAnswerTask"])
        }
        A = NewAutomatFromYAML(scenarii["OkAnswerNext"],nil)
        if A == nil {
            t.Errorf(messages["shouldntHaveFailed"], scenarii["OkAnswerNext"])
        }
        A = NewAutomatFromYAML(scenarii["Login"],tasks)
        if A == nil {
            t.Errorf(messages["shouldntHaveFailed"], scenarii["Login"])
        }
    })
}

func TestAutomatFromBytes(t *testing.T) {
    var A *qt.Automat

    A = NewAutomatFromYAML(nil,nil)
    if A != nil {
        t.Errorf("Should have failed with empty YAML data ?!")
    }
}

func TestMarshalling(t *testing.T) {
    var A *qt.Automat
    var data []byte

    A = NewAutomatFromYAML(`%YAML 1.1
---
States:
- State:
  - Id: Login
    ToQ: What's is your identifier ?
    DefaultAnswer:
      Next: Login
    Prolog: Banner
    MemoZone: LID
    SQ:
      Next: Profile
- State:
  - Id: Profile
    ToQ: Choose your profile
    MemoZone: PID
    MCQ:
      Choices:
      - Choice:
        - ToC: User
          Memorize: false
          Next: Hello
      - Choice:
        - ToC: Admin
          Memorize: true
          Next: Hello
- State:
  - Id: Hello
    MemoZone: LID
    Input: MEMOBUFFER
    Epilog: Greetings
    SQ:
      Type:
...
`,tasks)
    data = SerializeToYAML(A)
    if data == nil {
        t.Errorf(messages["shouldntHaveFailed"], "SerializeToYAML")
    }
    fmt.Printf("\ndump :\n%s\n\n", string(data))
}

func setup () {
    qu.SetLang("en")
}

func teardown() {
}

func TestMain(m *testing.M) {
    // call flag.Parse() here if TestMain uses flags
    setup()

    res := m.Run()

    teardown()

    os.Exit(res)
}
