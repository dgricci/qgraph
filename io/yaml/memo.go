package yaml

import (
    "gitlab.com/dgricci/qgraph/types"
)

// processMemo function sets attributes of `Memo` from unmarshalled YAML.
//
func processMemo (a *types.Automat, m map[interface{}]interface{}) error {
    if !existsMapKeyNotNil(m,"Memo") { return nil }
    var yzm map[interface{}]interface {}
    for _, yz := range m["Memo"].([]interface{}) {
        yzm, _ = yz.(map[interface{}]interface {})
        if err := processBuffer(a.Memo(),yzm); err != nil {
            return err
        }
    }
    return nil
}

// serializeMemoToYAML method marshals a `Memo` struct.
//
func serializeMemoToYAML (m *types.Memo) []byte {
    if len(m.Zones()) > 0 {
        yBytes := []byte("Memo:\n")
        var ybBytes []byte
        for k, z := range m.Zones() {
            yBytes= append(yBytes, []byte("- Zone: " + k + "\n")...)
            if ybBytes = serializeBufferToYAML(z) ; ybBytes != nil {
                yBytes= append(yBytes, ybBytes...)
            }
        }
        return yBytes
    }
    return []byte("")
}
