package yaml

import (
    "fmt"
    "gitlab.com/dgricci/qgraph/util"
    "gitlab.com/dgricci/qgraph/types"
)

// processBuffer function sets attributes of `Buffer` from unmarshalled YAML.
//
func processBuffer (m *types.Memo, b map[interface{}]interface{}) error {
    if !existsMapKeyNotNil(b,"Zone") || b["Zone"].(string) == "" {
        return fmt.Errorf(util.T("yamlMandatoryField"), "Memo", "Zone")
    }
    if existsMapKeyNotNil(b,"Buff") {
        m.SetZone(b["Zone"].(string), b["Buff"])
    }
    return nil
}

// serializeBufferToYAML method marshals a `Buffer` struct.
//
func serializeBufferToYAML (b *types.Buffer) []byte {
    return []byte("  Buff: " + fmt.Sprintf("%v\n", b.Value()))
}
