package yaml

import (
    "gitlab.com/dgricci/qgraph/util"
    "gitlab.com/dgricci/qgraph/types"
)

// processYAML function constructs an `Automat` struct.
//
func processYAML (y map[interface{}]interface{}, tasks map[string]types.FTask) *types.Automat {
    var err error

    a := types.NewAutomat(tasks)
    err = processStates(a,y)
    if !util.LogOnError(err) { return nil }
    err = processQueue(a,y)
    if !util.LogOnError(err) { return nil }
    err = processMemo(a,y)
    if !util.LogOnError(err) { return nil }

    err = a.Check()
    if !util.LogOnError(err) { return nil }

    return a
}
