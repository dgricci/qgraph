package yaml

import (
    "fmt"

    qu "gitlab.com/dgricci/qgraph/util"
    qt "gitlab.com/dgricci/qgraph/types"
)

// processQuestion sets attributes of `Question` (`SimpleQuestion` or `MultipleChoicesQuestion`) from unmarshalled YAML.
//
func processQuestion (s *qt.State, yq map[interface{}]interface{}) error {
    var exists bool

    if existsMapKey(yq,"SQ") {
        if yq["SQ"] != nil {
            if _, exists = yq["SQ"].(map[interface{}]interface{}) ; !exists {
                return fmt.Errorf(qu.T("yamlNotAMapping"),"SQ")
            }
            return processSimpleQuestion(s,yq["SQ"].(map[interface{}]interface{}))
        }
        return fmt.Errorf(qu.T("yamlEmptyContent"), "State.SQ")
    }
    if existsMapKey(yq,"MCQ") {
        if yq["MCQ"] != nil {
            if _, exists = yq["MCQ"].(map[interface{}]interface{}) ; !exists {
                return fmt.Errorf(qu.T("yamlNotAMapping"),"MCQ")
            }
            return processMultipleChoicesQuestion(s,yq["MCQ"].(map[interface{}]interface{}))
        }
        return fmt.Errorf(qu.T("yamlEmptyContent"), "State.MCQ")
    }
    return fmt.Errorf(qu.T("yamlExpectedKeyword"), "State.SQ or State.MCQ")
}

