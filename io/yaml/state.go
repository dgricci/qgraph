package yaml

import (
    "fmt"
    "strings"

    qu "gitlab.com/dgricci/qgraph/util"
    qt "gitlab.com/dgricci/qgraph/types"
)

// processStates function sets attributes of `States` from unmarshalled YAML.
//
func processStates (a *qt.Automat, m map[interface{}]interface{}) error {
    var exists bool
    var err error

    if !existsMapKey(m,"States") {
        return fmt.Errorf(qu.T("yamlExpectedKeyword"), "States")
    }
    if m["States"] == nil {
        return fmt.Errorf(qu.T("yamlEmptyContent"), "States")
    }
    if _, exists = m["States"].([]interface{}) ; !exists {
        return fmt.Errorf(qu.T("yamlNotASequence"), "States")
    }
    for _, ys := range m["States"].([]interface{}) {
        ysm, _ := ys.(map[interface{}]interface{})
        if !existsMapKey(ysm,"State") {
            return fmt.Errorf(qu.T("yamlExpectedKeyword"), "State")
        }
        var yso interface{} = ysm["State"]
        var ysom []interface{}
        if ysom, exists = yso.([]interface{}) ; !exists {
            return fmt.Errorf(qu.T("yamlNotASequence"), "State")
        }
        var ysom0 map[interface{}]interface{}
        if ysom0, exists = ysom[0].(map[interface{}]interface{}) ; !exists {
            return fmt.Errorf(qu.T("yamlNotAMapping"), "State")
        }
        s := qt.NewState(a)
        if !existsMapKeyNotNil(ysom0,"Id") || ysom0["Id"].(string) == "" {
            return fmt.Errorf(qu.T("yamlMandatoryField"), "State", "Id")
        }
        s.SetID(ysom0["Id"].(string))
        if existsMapKeyNotNil(ysom0,"ToQ") { s.SetText(ysom0["ToQ"].(string)) }
        checkXXXlog := func(yfName string, ofPtr *string) error {
            *ofPtr = ""
            if existsMapKey(ysom0,yfName) {
                *ofPtr= ysom0[yfName].(string)
                if *ofPtr != "" {
                    return checkIfTaskExists(a.Tasks(), *ofPtr, qu.T("yamlTaskNotFound"), "State", yfName, *ofPtr)
                }
            }
            return nil
        }// checkXXXlog
        var xlog string
        err = checkXXXlog("Prolog", &xlog)
        if err != nil {
            return err
        }
        s.SetPrologID(xlog)
        err = checkXXXlog("Epilog", &xlog)
        if err != nil {
            return err
        }
        s.SetEpilogID(xlog)
        if existsMapKeyNotNil(ysom0,"MemoZone") { s.SetZoneID(ysom0["MemoZone"].(string)) }
        var o map[interface{}]interface{}
        if o, exists = ysom0["DefaultAnswer"].(map[interface{}]interface{}) ; exists {
            if err= processDefaultAnswer(s,o); err != nil {
                return err
            }
        }
        if existsMapKeyNotNil(ysom0,"Input") && ysom0["Input"].(string) != "" {
            var i qt.Peripheral
            if i, err = qt.PERIPHERALS.Get(ysom0["Input"].(string)) ; err != nil {
                return fmt.Errorf(qu.T("yamlUnknownPeripheralType"),
                                  "State", "Input", ysom0["Input"].(string), strings.Join(qt.PeripheralsNames,", "))
            }
            // TODO(me) : not all values for stdin (INPUT, MEMOBUFFER)
            s.SetInput(i)
        }
        if existsMapKeyNotNil(ysom0,"Output") && ysom0["Output"].(string) != "" {
            var o qt.Peripheral
            if o, err = qt.PERIPHERALS.Get(ysom0["Output"].(string)) ; err != nil {
                return fmt.Errorf(qu.T("yamlUnknownPeripheralType"),
                                  "State", "Output", ysom0["Output"].(string), strings.Join(qt.PeripheralsNames,", "))
            }
            // TODO(me) : not all values for stdout/stderr (OUTPUT, MEMOBUFFER)
            s.SetOutput(o)
        }
        err = processQuestion(s, ysom0)
        if err != nil {
            return err
        }
        a.SetState(s)
    }
    return nil
}

// serializeStateToYAML method marshals a `State` to YAML bytes.
//
func serializeStateToYAML (s *qt.State) []byte {
    yBytes := []byte("- State:\n")
    yBytes = append(yBytes, []byte("  - Id: " + s.ID() + "\n")...)
    t := s.Text()
    if t != "" {
        if ! strings.Contains(t,"\n") {
            yBytes = append(yBytes, []byte("    ToQ: " + t + "\n")...)
        } else {
            yBytes = append(yBytes, []byte("    ToQ: |\n" + t + "\n")...)
        }
    }
    if da := s.DefaultAnswer() ; da != nil {
        var ydaBytes []byte
        if ydaBytes = serializeDefaultAnswerToYAML((da).(*qt.DefaultAnswer)) ; ydaBytes != nil {
            yBytes = append(yBytes, ydaBytes...)
        }
    }
    if s.Prolog() != nil {
        yBytes = append(yBytes, []byte("    Prolog: " + s.PrologID() + "\n")...)
    }
    if s.Epilog() != nil {
        yBytes = append(yBytes, []byte("    Epilog: " + s.EpilogID() + "\n")...)
    }
    if !s.Input().Is(qt.INPUT) {
        yBytes = append(yBytes, []byte("    Input: " + s.Input().Name() + "\n")...)
    }
    if !s.Output().Is(qt.OUTPUT) {
        yBytes = append(yBytes, []byte("    Output: " + s.Output().Name() + "\n")...)
    }
    if s.ZoneID() != "" {
        yBytes = append(yBytes, []byte("    MemoZone: " + s.ZoneID() + "\n")...)
    }
    q := s.Question()
    var yqBytes []byte
    if q.IsSimple() {
        if yqBytes = serializeSimpleQuestionToYAML((q).(*qt.SimpleQuestion)) ; yqBytes != nil {
            yBytes = append(yBytes, yqBytes...)
        }
    } else {
        if yqBytes = serializeChoicesQuestionToYAML((q).(*qt.ChoicesQuestion)) ; yqBytes != nil {
            yBytes = append(yBytes, yqBytes...)
        }
    }

    return yBytes
}
