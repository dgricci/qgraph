package yaml

import (
    "fmt"

    "gitlab.com/dgricci/qgraph/types"
)

func ExampleNewAutomatFromYAML_state() {
    A := NewAutomatFromYAML(`%YAML 1.1
---
States:
- State:
  - Id: Login
    ToQ: What's is your identifier ?
    DefaultAnswer:
      Next: Login
    Prolog: Banner
    MemoZone: LID
    SQ:
      Next: Profile
- State:
  - Id: Profile
    ToQ: Choose your profile
    MemoZone: PID
    MCQ:
      Choices:
      - Choice:
        - ToC: User
          Memorize: false
          Next: Hello
      - Choice:
        - ToC: Admin
          Memorize: true
          Next: Hello
- State:
  - Id: Hello
    MemoZone: LID
    Input: MEMOBUFFER
    Epilog: Greetings
    SQ:
      Type:
...
`,tasks)
    state := A.StateByID("Login")
    if state.ID() == "Login" {
        fmt.Println("Login")
    }
    if state.Text() == "What's is your identifier ?" {
        fmt.Println("Text OK")
    }
    if state.DefaultAnswer() != nil {
        fmt.Println("DefaultAnswer OK")
    }
    if state.Prolog() != nil {
        fmt.Println("Banner OK")
    }
    if state.Epilog() == nil {
        fmt.Println("Epilog OK")
    }
    if state.ZoneID() != "" {
        fmt.Println("MemoZone OK")
    }
    if state.Input().Is(types.INPUT) {
        fmt.Println("INPUT")
    }
    if state.Output().Is(types.OUTPUT) {
        fmt.Println("OUTPUT")
    }
    if state.Question() != nil {
        fmt.Println("Question OK")
    }
    if state.Automat() == A {
        fmt.Println("Automat OK")
    }
    // Output:
    // Login
    // Text OK
    // DefaultAnswer OK
    // Banner OK
    // Epilog OK
    // MemoZone OK
    // INPUT
    // OUTPUT
    // Question OK
    // Automat OK
}
