package yaml

import (
    "fmt"

    qu "gitlab.com/dgricci/qgraph/util"
    qt "gitlab.com/dgricci/qgraph/types"
)

// processChoices sets attributes of `Choice` from unmarshalled YAML.
//
func processChoices (cq *qt.ChoicesQuestion, ycs []interface{}) error {
    var exists bool
    var err error

    s := cq.QuestionState()
    for _, yc := range ycs {
        var ycm map[interface{}]interface{}
        if ycm, exists = yc.(map[interface{}]interface{}) ; !exists {
            return fmt.Errorf(qu.T("yamlNotAMapping"), "Choice")
        }
        if !existsMapKey(ycm,"Choice") {
            return fmt.Errorf(qu.T("yamlExpectedKeyword"), "Choice")
        }
        if ycm["Choice"] == nil {
            return fmt.Errorf(qu.T("yamlEmptyContent"), "Choice")
        }
        var yco []interface{}
        if yco, exists = ycm["Choice"].([]interface{}) ; !exists {
            return fmt.Errorf(qu.T("yamlNotASequence"), "Choice")
        }
        var yco0 map[interface{}]interface{}
        if yco0, exists = yco[0].(map[interface{}]interface{}) ; !exists {
            return fmt.Errorf(qu.T("yamlNotAMapping"), "Choice")
        }
        c := qt.NewChoice(s)
        if existsMapKeyNotNil(yco0,"ToC") {
            c.SetText(yco0["ToC"].(string))
        }
        if existsMapKeyNotNil(yco0,"Memorize") {
            c.SetMemorize(yco0["Memorize"].(bool))
        }
        if existsMapKeyNotNil(yco0,"Task") && yco0["Task"].(string) != "" {
            ctask := yco0["Task"].(string)
            err = checkIfTaskExists(s.Automat().Tasks(), ctask, qu.T("yamlTaskNotFound"), "Choice", "Task", ctask)
            if err != nil {
                return err
            }
            c.SetTaskID(ctask)
        }
        if existsMapKeyNotNil(yco0,"Next") && yco0["Next"].(string) != "" {
            c.SetNextStateID(yco0["Next"].(string))
        }
        cq.AddChoice(c)
    }
    return nil
}

// serializeChoiceToYAML method marshals a `Choice` struct.
//
func serializeChoiceToYAML (c *qt.Choice) []byte {
    yBytes := []byte("      - Choice:\n")
    yBytes= append(yBytes, []byte("        - ToC: " + c.Text() + "\n")...)
    yBytes= append(yBytes, []byte("          Memorize: " + fmt.Sprintf("%t\n", c.Memorize()))...)
    if c.Task() != nil {
        yBytes= append(yBytes, []byte("          Task: " + c.TaskID() + "\n")...)
    }
    if c.NextState() != qt.EXITSTATE {
        yBytes= append(yBytes, []byte("          Next: " + c.NextStateID() + "\n")...)
    }
    return yBytes
}
