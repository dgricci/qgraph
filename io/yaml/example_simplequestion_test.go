package yaml

import (
    "fmt"

    "gitlab.com/dgricci/qgraph/types"
)

func ExampleNewAutomatFromYAML_simpleQuestion() {
    A := NewAutomatFromYAML(`%YAML 1.1
---
States:
- State:
  - Id: Login
    ToQ: What's is your identifier ?
    DefaultAnswer:
      Next: Login
    Prolog: Banner
    MemoZone: LID
    SQ:
      Next: Profile
- State:
  - Id: Profile
    ToQ: Choose your profile
    MemoZone: PID
    MCQ:
      Choices:
      - Choice:
        - ToC: User
          Memorize: false
          Next: Hello
      - Choice:
        - ToC: Admin
          Memorize: true
          Next: Hello
- State:
  - Id: Hello
    MemoZone: LID
    Input: MEMOBUFFER
    Epilog: Greetings
    SQ:
      Type:
...
`,tasks)
    state := A.StateByID("Login")
    question := state.Question()
    if question.IsSimple() {
        fmt.Println("Simple OK")
    }
    simpleQ := question.(*types.SimpleQuestion)
    if simpleQ.NextState() != nil {
        fmt.Println("Next OK")
    }
    if simpleQ.QuestionState() == state {
        fmt.Println("State OK")
    }
    if simpleQ.ResponseType().Is(types.STRING) {
        fmt.Println(types.STRING.Name())
    }
    fuzz, unit := simpleQ.ResponseMetadata()
    if fuzz == 0 && unit == "" {
        fmt.Println("No metadata")
    }
    _, low, upper, _ := simpleQ.Interval()
    if low == nil && upper == nil {
        fmt.Println("No interval")
    }
    // Output:
    // Simple OK
    // Next OK
    // State OK
    // STRING
    // No metadata
    // No interval
}

func ExampleNewAutomatFromYAML_simpleQuestion_intervalInt() {
    A := NewAutomatFromYAML(`%YAML 1.1
---
States:
- State:
  - Id: Age
    ToQ: Gimme your age
    SQ:
      Type: IInINTERVAL
      Fuzziness: 1
      Unit: year
      LowerBound:
        - Value: 0
          Include: false
      UpperBound:
        - Value: 120
          Include: true
      Next:
`,nil)
    state := A.StateByID("Age")
    question := state.Question()
    simpleQ := question.(*types.SimpleQuestion)
    fuzz, unit := simpleQ.ResponseMetadata()
    if fuzz == 1 && unit == "year" {
        fmt.Println("Metadata OK")
    }
    LowIn, low, upper, UpperIn := simpleQ.Interval()
    if !LowIn && low == int64(0) && UpperIn && upper == int64(120) {
        fmt.Println("Interval OK")
    }
    if simpleQ.NextState() == types.EXITSTATE {
        fmt.Println("Next OK")
    }
    if SerializeToYAML(A) != nil {
        fmt.Println("Serialize OK")
    }
    // Output:
    // Metadata OK
    // Interval OK
    // Next OK
    // Serialize OK
}

func ExampleNewAutomatFromYAML_simpleQuestion_intervalFloat() {
    A := NewAutomatFromYAML(`%YAML 1.1
---
States:
- State:
  - Id: Height
    ToQ: Gimme your height
    SQ:
      Type: DInINTERVAL
      Fuzziness: 1
      Unit: cm
      LowerBound:
        - Value: 20
          Include: true
      UpperBound:
        - Value: 250.0
          Include: false
`,nil)
    state := A.StateByID("Height")
    question := state.Question()
    simpleQ := question.(*types.SimpleQuestion)
    fuzz, unit := simpleQ.ResponseMetadata()
    if fuzz == 1 && unit == "cm" {
        fmt.Println("Metadata OK")
    }
    LowIn, low, upper, UpperIn := simpleQ.Interval()
    if LowIn && low == float64(20.0) && !UpperIn && upper == float64(250.0) {
        fmt.Println("Interval OK")
    }
    if simpleQ.NextState() == types.EXITSTATE {
        fmt.Println("Next OK")
    }
    if SerializeToYAML(A) != nil {
        fmt.Println("Serialize OK")
    }
    // Output:
    // Metadata OK
    // Interval OK
    // Next OK
    // Serialize OK
}
