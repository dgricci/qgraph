package io

import (
    "fmt"

    "gitlab.com/dgricci/qgraph/types"
    "gitlab.com/dgricci/qgraph/io/yaml"
)

func ExampleNewAutomatFromFile_defaultAnswer() {
    A := NewAutomatFromFile("testdata/simple-automat.yml",tasks)
    state := A.StateByID("Login")
    answer := state.DefaultAnswer()
    if answer != nil {
        fmt.Println("DefaultAnswer OK")
    }
    if !answer.IsChoice() {
        fmt.Println("IsChoice OK")
    }
    if answer.Task() == nil {
        fmt.Println("Task OK")
    }
    if answer.NextState() != types.EXITSTATE {
        fmt.Println("Next OK")
    }
    if answer.AnswerState() == state {
        fmt.Println("State OK")
    }
    // Output:
    // DefaultAnswer OK
    // IsChoice OK
    // Task OK
    // Next OK
    // State OK
}

func ExampleNewAutomatFromFile_defaultAnswer_next() {
    A := NewAutomatFromFile("testdata/defaultanswer.yml", nil)
    state := A.StateByID("Hello")
    answer := state.DefaultAnswer()
    if answer != nil {
        fmt.Println("DefaultAnswer OK")
    }
    if !answer.IsChoice() {
        fmt.Println("IsChoice OK")
    }
    if answer.Task() == nil {
        fmt.Println("Task OK")
    }
    if answer.NextState() == types.EXITSTATE {
        fmt.Println("Next OK")
    }
    if answer.AnswerState() != nil {
        fmt.Println("State OK")
    }
    if yaml.SerializeToYAML(A) != nil {
        fmt.Println("Serialize OK")
    }
    // Output:
    // DefaultAnswer OK
    // IsChoice OK
    // Task OK
    // Next OK
    // State OK
    // Serialize OK
}
