package io

import (
    "fmt"

    "gitlab.com/dgricci/qgraph/io/yaml"
)

func ExampleNewAutomatFromFile_memo() {
    A := NewAutomatFromFile("testdata/memo.yml",nil)
    memo := A.Memo()
    for z, b := range memo.Zones() {
        fmt.Println(z)
        fmt.Printf("%v\n",b.Value())
    }
    if yaml.SerializeToYAML(A) != nil {
        fmt.Println("Serialize OK")
    }
    // Output:
    // Z
    // A
    // Serialize OK
}

func ExampleNewAutomatFromFile_check() {
    A := NewAutomatFromFile("testdata/storage-checks.yml",nil)
    if A != nil && A.Check() == nil {
        fmt.Println("Check OK")
    }
    // Output:
    // Check OK
}
