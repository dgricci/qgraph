// Package io from reading/writing automaton
package io

import (
    "fmt"
    "regexp"
    "io/ioutil"

    "github.com/nicksnyder/go-i18n/i18n"

    "gitlab.com/dgricci/qgraph/util"
    "gitlab.com/dgricci/qgraph/types"
    "gitlab.com/dgricci/qgraph/io/yaml"
)

// formatMatchers for checking whether the file's format may be supported based on the file's extension
var formatMatchers= map[string]*regexp.Regexp {
  "yaml" : regexp.MustCompile(`(.*)\.(?i)(ya?ml)$`),
}

// builder for constructing automaton
type builder func (interface{}, map[string]types.FTask) *types.Automat

// formatBuilders for launching constructor based on the recognition of the file's format
var formatBuilders= map[string]builder {
    "yaml"  :   yaml.NewAutomatFromYAML,
}

// saver for storing automaton
type saver func (*types.Automat) []byte

// formatSerializers for writing automaton to file based on the recognition of the file's format
var formatSerializers= map[string]saver {
    "yaml"  :   yaml.SerializeToYAML,
}

// unsupportedFormat returns a preformatted error
func unsupportedFormat (name string) error {
    return fmt.Errorf(util.T("unsupportedFormat"), name)
}

// NewAutomatFromFile Automat constructor.
// Reads a YAML file (Cf. http://yaml.org/spec/1.2/spec.html) and build an
// automaton.
// `path` is the OS path to YAML file.
// `tasks` is a map of function of type `FTask`.
//
func NewAutomatFromFile (path string, tasks map[string]types.FTask) *types.Automat {
    var data []byte
    var err error

    data, err = ioutil.ReadFile(path)
    if !util.LogOnError(err) { return nil }
    for k, m := range formatMatchers {
        if m.MatchString(path) {
            return formatBuilders[k](data,tasks)
        }
    }
    util.LogOnError(unsupportedFormat(path))
    return nil
}

// SerializeAutomat returns a bytes array holding the
// automaton's data.
//
func SerializeAutomat ( a *types.Automat, format string ) ([]byte, error) {
    for k, m := range formatMatchers {
        if m.MatchString(format) {
            data := formatSerializers[k](a)
            return data, nil
        }
    }
    err := unsupportedFormat(format)
    util.LogOnError(err)
    return []byte(""), err
}

// SaveAutomatToFile writes the automaton to a file.
// If no file's name is given, a YAML string is sent to os.Stdout.
func SaveAutomatToFile ( a *types.Automat, path ...string ) error {
    if len(path) == 0 {
        data := formatSerializers["yaml"](a)
        fmt.Printf("%s", data)
        return nil
    }
    data, err := SerializeAutomat(a,path[0])
    if err != nil {
        return err
    }
    err = ioutil.WriteFile(path[0],data,0644)
    util.LogOnError(err)
    return err
}

// init function to set global variables
//
func init () {
    i18n.MustLoadTranslationFile("i18n/en-us.all.json")
    i18n.MustLoadTranslationFile("i18n/fr-fr.all.json")

    util.Log().Println("gitlab.com/dgricci/qgraph/io loaded")
}
