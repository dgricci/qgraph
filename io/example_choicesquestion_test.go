package io

import (
    "fmt"

    "gitlab.com/dgricci/qgraph/types"
    "gitlab.com/dgricci/qgraph/io/yaml"
)

func ExampleNewAutomatFromFile_choicesQuestion() {
    A := NewAutomatFromFile("testdata/simple-automat.yml",tasks)
    state := A.StateByID("Profile")
    question := state.Question()
    if !question.IsSimple() {
        fmt.Println("Choices OK")
    }
    choicesQ := question.(*types.ChoicesQuestion)
    if choicesQ.NextState() == nil {
        fmt.Println("Next OK")
    }
    if choicesQ.QuestionState() == state {
        fmt.Println("State OK")
    }
    if choicesQ.Display() {
        fmt.Println("Display OK")
    }
    if !choicesQ.Learn() {
        fmt.Println("Learn OK")
    }
    if len(choicesQ.Choices()) == 2 {
        fmt.Println("Choices OK")
    }
    // Output:
    // Choices OK
    // Next OK
    // State OK
    // Display OK
    // Learn OK
    // Choices OK
}

func ExampleNewAutomatFromFile_choicesQuestion_learn() {
    A := NewAutomatFromFile("testdata/learn.yml", map[string]types.FTask{
    "Dump"  :   func (s *types.State, r interface{}) error {
        if yaml.SerializeToYAML(s.Automat()) == nil {
            return fmt.Errorf("Serialize KO")
        }
        return nil
    },
})
    state := A.StateByID("Learn")
    question := state.Question()
    if !question.IsSimple() {
        fmt.Println("Choices OK")
    }
    choicesQ := question.(*types.ChoicesQuestion)
    if choicesQ.Learn() {
        fmt.Println("Learn OK")
    }
    for _, c := range choicesQ.Choices() {
        if c.IsChoice() {
            fmt.Println("IsChoice OK")
        }
        fmt.Println(c.(*types.Choice).Text())
    }
    if yaml.SerializeToYAML(A) != nil {
        fmt.Println("Serialize OK")
    }
    // Unordered output:
    // Choices OK
    // Learn OK
    // IsChoice OK
    // Alice
    // IsChoice OK
    // Bob
    // Serialize OK
}

