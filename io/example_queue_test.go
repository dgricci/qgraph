package io

import (
    "fmt"

    "gitlab.com/dgricci/qgraph/io/yaml"
)

func ExampleNewAutomatFromFile_queue() {
    A := NewAutomatFromFile("testdata/queue.yml",nil)
    queue := A.Queue()
    for _, s := range queue.History() {
        fmt.Println(s)
    }
    if yaml.SerializeToYAML(A) != nil {
        fmt.Println("Serialize OK")
    }
    // Output:
    // S1
    // S2
    // S2
    // Serialize OK
}

