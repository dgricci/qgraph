package io

import (
    "testing"
    "fmt"
    "os"

    "gitlab.com/dgricci/qgraph/util"
    "gitlab.com/dgricci/qgraph/types"
)

var (
    tasks = map[string]types.FTask{
        "Banner"    :   func (s *types.State, r interface{}) error {
            fmt.Println("Welcome ...")
            return nil
        },
        "Greetings" :   func (s *types.State, r interface{}) error {
            fmt.Printf("Hello '%s' !\n", r.(string))
            return nil
        },
        "Fake"      :   func (s *types.State, r interface{}) error {
            fmt.Printf("Fake !\n")
            return nil
        },
        "Check"     :   func (s *types.State, r interface{}) error {
            fmt.Printf("Checked !\n")
            return nil
        },
    }
)

func TestAutomatFromFile(t *testing.T) {
    var A *types.Automat

    A = NewAutomatFromFile("testdata/noExtensionFile",nil)
    if A != nil {
        t.Errorf("Should have failed with file without extension ?!")
    }
    A = NewAutomatFromFile("testdata/nonSupportedFormatFile.txt",nil)
    if A != nil {
        t.Errorf("Should have failed with non supported file format ?!")
    }
    A = NewAutomatFromFile("nonExistingFile.yml",nil)
    if A != nil {
        t.Errorf("Should have failed with non existing file ?!")
    }
    A = NewAutomatFromFile("testdata/bad.yml",nil)
    if A != nil {
        t.Errorf("Should have failed with non YAML file ?!")
    }
    A = NewAutomatFromFile("testdata/emptyFile.yml",nil)
    if A != nil {
        t.Errorf("Should have failed with an empty file ?!")
    }
    A = NewAutomatFromFile("testdata/simple-automat.yml",tasks)
    if A == nil {
        t.Errorf("Failed to construct Automat with '%s'", "testdata/simple-automat.yml")
    }
    if len(A.States()) != 3 {
        t.Errorf("Expected %d states, got %d ?!\n", 3, len(A.States()))
    }
}

func TestMarshalling(t *testing.T) {
    var A *types.Automat
    var err error

    A = NewAutomatFromFile("testdata/simple-automat.yml",tasks)
    fn := "testdata/_sa"
    if err = SaveAutomatToFile(A,fn) ; err == nil {
        t.Errorf("Shoud have failed to save Automat : '%s'", err)
    }
    fn = "testdata/_sa.txt"
    if err = SaveAutomatToFile(A,fn) ; err == nil {
        t.Errorf("Shoud have failed to save Automat : '%s'", err)
    }
    fn = "testdata/_sa.yml"
    defer os.Remove(fn)
    if err = SaveAutomatToFile(A,fn) ; err != nil {
        t.Errorf("Failed to save Automat : '%s'", err)
    }
    fmt.Printf("\nPrinting to stdout :\n>>>\n")
    if err = SaveAutomatToFile(A) ; err != nil {
        t.Errorf("Failed to print on stdout : '%s'", err)
    }
    fmt.Printf(">>>\n")
}

func setup () {
    util.SetLang("en")
}

func teardown() {
}

func TestMain(m *testing.M) {
    // call flag.Parse() here if TestMain uses flags
    setup()

    res := m.Run()

    teardown()

    os.Exit(res)
}
