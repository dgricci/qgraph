package io

import (
    "fmt"
    "os"

    "gitlab.com/dgricci/qgraph/types"
)

func ExampleNewAutomatFromFile() {
    A := NewAutomatFromFile("testdata/simple-automat.yml",tasks)
    for _, id := range A.States() {
        state := A.StateByID(id)
        if state != nil {
            fmt.Println(id)
        }
    }
    if A.StateByID("Foo") == nil {
        fmt.Println("Foo not found")
    }
    if A.Queue() != nil {
        fmt.Println("Queue()")
    }
    if A.Memo() != nil {
        fmt.Println("Memo()")
    }
    if A.Tasks() != nil {
        fmt.Println("Tasks()")
    }
    // Unordered output:
    // Login
    // Profile
    // Hello
    // Foo not found
    // Queue()
    // Memo()
    // Tasks()
}

func ExampleSaveAutomatToFile() {
    A := NewAutomatFromFile("testdata/whoareyou.yml", map[string]types.FTask{
        "Hello"     :   func (s *types.State, r interface{}) error {
            fmt.Printf("Hello '%s' !\n", s.Automat().Memo().Zone("FirstName").Value().(string))
            return nil
        },
        "Wow"       :   func (s *types.State, r interface{}) error {
            fmt.Println("Wow !!!")
            return nil
        },
        "Conclusion":   func (s *types.State, r interface{}) error {
            fmt.Printf("Bye\n")
            return nil
        },
    })
    if A != nil {
        fmt.Println("Unmarshalling OK")
    }
    fn := "testdata/_way.yml"
    defer os.Remove(fn)
    if SaveAutomatToFile(A,fn) == nil {
        fmt.Println("Marshalling OK")
    }
    // Output:
    // Unmarshalling OK
    // Marshalling OK
}
