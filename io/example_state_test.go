package io

import (
    "fmt"

    "gitlab.com/dgricci/qgraph/types"
)

func ExampleNewAutomatFromFile_state() {
    A := NewAutomatFromFile("testdata/simple-automat.yml",tasks)
    state := A.StateByID("Login")
    if state.ID() == "Login" {
        fmt.Println("Login")
    }
    if state.Text() == "What's is your identifier ?" {
        fmt.Println("Text OK")
    }
    if state.DefaultAnswer() != nil {
        fmt.Println("DefaultAnswer OK")
    }
    if state.Prolog() != nil {
        fmt.Println("Banner OK")
    }
    if state.Epilog() == nil {
        fmt.Println("Epilog OK")
    }
    if state.ZoneID() != "" {
        fmt.Println("MemoZone OK")
    }
    if state.Input().Is(types.INPUT) {
        fmt.Println("INPUT")
    }
    if state.Output().Is(types.OUTPUT) {
        fmt.Println("OUTPUT")
    }
    if state.Question() != nil {
        fmt.Println("Question OK")
    }
    if state.Automat() == A {
        fmt.Println("Automat OK")
    }
    // Output:
    // Login
    // Text OK
    // DefaultAnswer OK
    // Banner OK
    // Epilog OK
    // MemoZone OK
    // INPUT
    // OUTPUT
    // Question OK
    // Automat OK
}

