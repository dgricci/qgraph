# Copyright 2017-2018 Didier Richard
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Adapted from https://github.com/vincentbernat/hellogopher/blob/master/Makefile
#
# To test without executing commands :
# $ make -n
# To debug :
# $ make --debug=[a|b|v|i|j|m]
#

# top package
PKGROOT := gitlab.com/dgricci/qgraph
PKGNAME  =
PKGDIR   = ./$(PKGNAME)
# one can invoke with a sub-package by passing PACKAGE to the command line
PACKAGE ?= $(PKGROOT)
SUBPKGS  =util types io/yaml io

include project.mk

# Default target (first target not starting with .)
all: main

ifdef WITHPKGS
packages:
	@for p in $(SUBPKGS) ; do \
	    $(MAKE) GOPATH=$(GOPATH) -f ./$${p}/Makefile ;\
    done
else
packages:
endif

main:: ; $(info … built done !)

ifeq ($(PACKAGE),$(PKGROOT))
# Dependencies through dep :
dep-init: $(BASE) ; $(info $(T)$(T)$(S) creating dependencies from scratch …)
	@cd $(BASE) && ( [ ! -f Gopkg.toml ] && $(DEP) init || touch Gopkg.toml )
dep-install: Gopkg.toml ; $(info $(T)$(T)$(S) retrieving dependencies …)
	@$(DEP) ensure

# protect lf -> \n\
# protect "  -> \"
# protect $  -> $$
# protect $0 from being replaced by /bin/sh -> \$$0
# protect \n -> \\\n
define AWKSCRIPT
#!/usr/bin/awk -f\n\
BEGIN{\n\
  P=0\n\
  U=\"\"\n\
}\n\
/^#/{\n\
  next\n\
}\n\
/^$$/{\n\
  next\n\
}\n\
/^\[\[projects\]\]/{\n\
  P=1\n\
  next\n\
}\n\
{\n\
  if (P==1 && match(\$$0,/\s*name\s*=\s*\"([^\"]*)\"/,v)>0) {\n\
    U=v[1]\n\
    next\n\
  }\n\
  if (P==1 && U!=\"\" && match(\$$0,/\s*packages\s*=\s*\[([^]]*)\]/,p)>0) {\n\
    n = split(p[1],pks,/,/)\n\
    if (n>0) {\n\
      pkg=pks[1]\n\
      gsub(/\"/,\"\",pkg)\n\
      if (pkg == \".\") {\n\
        printf \"%s\\\n\", U\n\
      } else {\n\
        printf \"%s/%s\\\n\", U, pkg\n\
      }\n\
      P=0\n\
      U=\"\"\n\
      next\n\
    }\n\
  }\n\
}\n\
END{}\n
endef

Gopkg.awk: Gopkg.toml Gopkg.lock | $(BASE) dep
	@echo "$(AWKSCRIPT)" > $@

vendor: Gopkg.toml Gopkg.lock Gopkg.awk | $(BASE) dep ; $(info $(T)$(T)$(S) installing dependencies …)
	@cd $(BASE) && $(DEP) ensure -update
	@for p in `awk -f Gopkg.awk Gopkg.lock` ; do cd vendor/$$p && $(GO) install . && cd - >/dev/null ; done
	@rm -f Gopkg.awk
endif

%.all.json: %.toml
	$(eval lang=$(shell basename `echo $*`))
	$(eval i18n=$(shell dirname `echo $@`))
	@$(GOI18N) -sourceLanguage $(lang) -outdir $(i18n) ./$(i18n)/$(lang).toml
	@$(GOI18N) merge -sourceLanguage $(lang) -outdir $(i18n)  $(foreach p,$(call ReverseWordList,$(SUBPKGS)),./$(p)/$@) $@

help:
	$(info $(S) In order to build and install this package $(PACKAGE), you need to have golang installed.)
	$(info $(S) One can use its own GOPATH environment for buidling this package :)
	$(info $(T)$$ make GOPATH=$${GOPATH})
	$(info $(S) To run the tests, use :)
	$(info $(T)$$ make tests)
	$(info $(T)One can pass GOTESTOPTS in the form GOTESTOPTS="options" as in:)
	$(info $(T)$$ make GOTESTOPTS="-v" tests)
	$(info In order to test and build a sub-package, one needs :)
	$(info $(T)$$ PKGROOT=gitlab.com/dgricci/qgraph make PACKAGE=$(PKGROOT)/util tests)
	$(info $(T)$$ PKGROOT=gitlab.com/dgricci/qgraph make PACKAGE=$(PKGROOT)/types tests)
	$(info $(T)$$ PKGROOT=gitlab.com/dgricci/qgraph make PACKAGE=$(PKGROOT)/io/yaml tests)
	$(info $(T)$$ PKGROOT=gitlab.com/dgricci/qgraph make PACKAGE=$(PKGROOT)/io tests)
	$(info In order to document a sub-package, one needs :)
	$(info $(T)$$ PKGROOT=gitlab.com/dgricci/qgraph make PACKAGE=$(PKGROOT)/util doc)
	$(info $(T)$$ PKGROOT=gitlab.com/dgricci/qgraph make PACKAGE=$(PKGROOT)/types doc)
	$(info $(T)$$ PKGROOT=gitlab.com/dgricci/qgraph make PACKAGE=$(PKGROOT)/io/yaml doc)
	$(info $(T)$$ PKGROOT=gitlab.com/dgricci/qgraph make PACKAGE=$(PKGROOT)/io doc)
	$(info $(S) To install the package, use :)
	$(info $(T)$$ make install)
	$(info In order to install each sub-package, one needs FIXME :)
	$(info $(T)$$ PKGROOT=gitlab.com/dgricci/qgraph make PACKAGE=$(PKGROOT)/util install)
	$(info $(T)$$ PKGROOT=gitlab.com/dgricci/qgraph make PACKAGE=$(PKGROOT)/types install)
	$(info $(T)$$ PKGROOT=gitlab.com/dgricci/qgraph make PACKAGE=$(PKGROOT)/io/yaml install)
	$(info $(T)$$ PKGROOT=gitlab.com/dgricci/qgraph make PACKAGE=$(PKGROOT)/io install)
	$(info Default target both runs the tests and installs the package)
	@$(MAKE) --silent check-env

# always execute these targets when invoked
.PHONY: all tools tests clean build-tests doc install help

