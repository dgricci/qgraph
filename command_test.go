package qgraph

import (
    "os"
    "strings"
    "testing"

    qu "gitlab.com/dgricci/qgraph/util"
    qt "gitlab.com/dgricci/qgraph/types"
)

func TestCommandPrefix ( t *testing.T ) {
    var star, dollar = byte(42), byte(36)
    if CommandPrefix() != star {
        t.Errorf("Expected command prefix '%c', got '%c'", star, CommandPrefix())
    }
    ChangeCommandPrefix(dollar)
    if CommandPrefix() != dollar {
        t.Errorf("Expected command prefix '%c', got '%c'", dollar, CommandPrefix())
    }
}

func TestAllowedCommands ( t *testing.T ) {
    ChangeCommandPrefix(([]byte("*"))[0])
    parser := CommandParser()
    if parser == nil {
        t.Errorf("Unexpected nil command parser")
    }
    ic := "*PRED"
    a := parser.FindStringSubmatch(ic)
    if len(a) != 6 || a[2] != "PRED" {
        t.Errorf("Failed to parse '%s' with '%s'", ic, parser.String())
    }
    ic = "*FIRST"
    a = parser.FindStringSubmatch(ic)
    if len(a) != 6 || a[2] != "FIRST" {
        t.Errorf("Failed to parse '%s' with '%s'", ic, parser.String())
    }
    ic = "*LANG fr"
    a = parser.FindStringSubmatch(ic)
    if len(a) != 6 || !(a[2] == "LANG" && a[5] == "fr") {
        t.Errorf("Failed to parse '%s' with '%s'", ic, parser.String())
    }
    ic = "*QUIT"
    a = parser.FindStringSubmatch(ic)
    if len(a) != 6 || a[2] != "QUIT" {
        t.Errorf("Failed to parse '%s' with '%s'", ic, parser.String())
    }
    ic = "*ABORT"
    a = parser.FindStringSubmatch(ic)
    if len(a) != 6 || a[2] != "ABORT" {
        t.Errorf("Failed to parse '%s' with '%s'", ic, parser.String())
    }
}

func TestNotAllowedCommands ( t *testing.T ) {
    parser := CommandParser()
    if parser == nil {
        t.Errorf("Unexpected nil command parser")
    }
    ic := "*FAKECMD"
    a := parser.FindStringSubmatch(ic)
    if len(a) != 6 && a[2] != "FAKECMD" {
        t.Errorf("Failed to parse '%s' with '%s'", ic, parser.String())
    }
    ic = "*pred"
    if len(a) != 6 && a[2] != "pred" {
        t.Errorf("Failed to parse '%s' with '%s'", ic, parser.String())
    }
    ic = "*PREDE"
    if len(a) != 6 && a[2] != "PREDE" {
        t.Errorf("Failed to parse '%s' with '%s'", ic, parser.String())
    }
}

func TestEasyCommands ( t *testing.T ) {
    A := NewQGraph(automatFn, tasks)
    s0 := A.Automat().StateByID("ID1")
    var err error
    var sn *qt.State
    if sn, err = A.ExecCommand(s0,1) ; err == nil && sn != nil {
        t.Errorf("Expected command to fail for state '%s' with command '%d'", s0.ID(), 1)
    }
    cmd := "*LANG fr"
    if sn, err = A.ExecCommand(s0,cmd) ; err != nil || (sn != s0 && qu.T("supportedLanguage") != "Français") {
        t.Errorf("Expected command to succeed for state '%s' with command '%s'", s0.ID(), cmd)
    }
    cmd = "*LANG en"
    if sn, err = A.ExecCommand(s0,cmd) ; err != nil || (sn != s0 && qu.T("supportedLanguage") != "English") {
        t.Errorf("Expected command to succeed for state '%s' with command '%s'", s0.ID(), cmd)
    }
    cmd = "*LANG"
    if sn, err = A.ExecCommand(s0,cmd) ; err == nil {
        t.Errorf("Expected command to fail for state '%s' with command '%s'", s0.ID(), cmd)
    }
    cmd = "*LANG zz"
    if sn, err = A.ExecCommand(s0,cmd) ; err == nil {
        t.Errorf("Expected command to fail for state '%s' with command '%s'", s0.ID(), cmd)
    }
    cmd = "*ABORT"
    if sn, err = A.ExecCommand(s0,cmd) ; err != nil || sn != qt.EXITSTATE {
        t.Errorf("Expected command to succeed for state '%s' with command '%s'", s0.ID(), cmd)
    }
    cmd = "*QUIT >"
    if sn, err = A.ExecCommand(s0,cmd) ; err != nil || sn != qt.EXITSTATE {
        t.Errorf("Expected command to succeed for state '%s' with command '%s'", s0.ID(), cmd)
    }
    A.SetFilename("")
    cmd = "*QUIT"
    if sn, err = A.ExecCommand(s0,cmd) ; err != nil || sn != qt.EXITSTATE {
        t.Errorf("Expected command to succeed for state '%s' with command '%s'", s0.ID(), cmd)
    }
    fn := "testdata/Filename.yml"
    defer os.Remove(fn)
    cmd = "*QUIT "+fn
    if sn, err = A.ExecCommand(s0,cmd) ; err != nil || sn != qt.EXITSTATE {
        t.Errorf("Expected command to succeed for state '%s' with command '%s'", s0.ID(), cmd)
    }
    B := NewQGraph(fn, tasks)
    if B == nil {
        t.Errorf("Expected command '%s' to serialize into '%s' in success", cmd, fn)
    }
    cmd = "DUMMY"
    bcmd := []byte(cmd)
    if sn, err = A.ExecCommand(s0,bcmd) ; err == nil && sn != nil {
        t.Errorf("Expected command to fail for state '%s' with command '%s'", s0.ID(), cmd)
    }
    cmd = "*DUMMY"
    bcmd = []byte(cmd)
    if sn, err = A.ExecCommand(s0,bcmd) ; err == nil {
        t.Errorf("Expected command to fail for state '%s' with command '%s'", s0.ID(), cmd)
    }
}

func TestPREDCommand ( t *testing.T ) {
    A := NewQGraph(automatFn, tasks)
    s0 := A.Automat().StateByID("ID1")
    var err error
    var sn *qt.State
    cmd := "*PRED"
    if sn, err = A.ExecCommand(nil,cmd) ; err == nil {
        t.Errorf("Expected command to fail for state '%s' with command '%s'", s0.ID(), cmd)
    }
    if sn, err = A.ExecCommand(s0,cmd) ; err != nil || sn == nil || sn.ID() != "ID2"  {
        t.Errorf("Expected command to succeed for state '%s' with command '%s'", s0.ID(), cmd)
    }
    h := A.Automat().Queue().History()
    if len(h) != 2 && h[1] != "ID2" && h[0] != "ID1" {
        t.Errorf("Queue is incorrect after '%s' : expected length 2, got %d, expected 'ID1,ID2', got '%s'", cmd, len(h), strings.Join(h,","))
    }
    z := A.Automat().Memo().Zones()
    if len(z) != 1 && z["ZID2"] != nil {
        t.Errorf("Memo is incorrect after '%s' : expected length 1, got %d, expected '<nil>', got '%v'", cmd, len(z), z["ZID2"])
    }
    s0 = sn
    if sn, err = A.ExecCommand(s0,cmd) ; err != nil || sn == nil || sn.ID() != "ID1"  {
        t.Errorf("Expected command to succeed for state '%s' with command '%s'", s0.ID(), cmd)
    }
    h = A.Automat().Queue().History()
    if len(h) != 1 && h[0] != "ID1" {
        t.Errorf("Queue is incorrect after '%s' : expected length 1, got %d, expected 'ID1', got '%s'", cmd, len(h), strings.Join(h,","))
    }
    if len(z) != 0 {
        t.Errorf("Memo is incorrect after '%s' : expected length 0", cmd)
    }
    s0 = sn
    if sn, err = A.ExecCommand(s0,cmd) ; err == nil {
        t.Errorf("Expected command to fail for state '%s' with command '%s'", s0.ID(), cmd)
    }
}

func TestFIRSTCommand ( t *testing.T ) {
    A := NewQGraph(automatFn, tasks)
    // add ID2 in history as current node
    A.Automat().Queue().Append("ID2")
    // set the start node
    s0, _ := A.startState("")
    if s0 == nil || s0.ID() != "ID2" {
        t.Errorf("Expected start state as 'ID2', found '%v'", s0)
    }
    var err error
    var sn *qt.State
    cmd := "*FIRST"
    if sn, err = A.ExecCommand(nil,cmd) ; err == nil {
        t.Errorf("Expected command to fail for state '%s' with command '%s'", s0.ID(), cmd)
    }
    if sn, err = A.ExecCommand(s0,cmd) ; err != nil || sn == nil || sn.ID() != "ID1"  {
        t.Errorf("Expected command to succeed for state '%s' with command '%s'", s0.ID(), cmd)
    }
    h := A.Automat().Queue().History()
    if len(h) != 0 {
        t.Errorf("Queue is incorrect after '%s' : expected length 0, got %d, with '%s'", cmd, len(h), strings.Join(h,","))
    }
    z := A.Automat().Memo().Zones()
    if len(z) != 0 {
        t.Errorf("Memo is incorrect after '%s' : expected length 0, got %d, expected '<nil>', got '%v'", cmd, len(z), z)
    }
    s0 = sn
    A.Automat().Queue().Append(s0.ID()) // push s0 as current state
    if sn, err = A.ExecCommand(s0,cmd) ; err != nil || sn == nil || sn.ID() != "ID1"  {
        t.Errorf("Expected command to succeed for state '%s' with command '%s'", s0.ID(), cmd)
    }
}