package qgraph

import (
    "testing"

    qt "gitlab.com/dgricci/qgraph/types"
)

var (
    tasks = map[string]qt.FTask{
        "doSomething"   :   func (s *qt.State, r interface{}) error {
            return nil
        },
        "checkFirst"    :   func (s *qt.State, r interface{}) error {
            return nil
        },
        "computeIt"     :   func (s *qt.State, r interface{}) error {
            return nil
        },
        "processIt"     :   func (s *qt.State, r interface{}) error {
            return nil
        },
        "workIt"        :   func (s *qt.State, r interface{}) error {
            return nil
        },
    }
    automatFn = "testdata/automat.yml"
)

// BEWARE : internals testing ...

func TestStartState ( t *testing.T ) {
    var s0 *qt.State
    A := NewQGraph("",nil)
    A.SetAutomat(nil) // dangerous !
    if s0, _ = A.startState("") ; s0 != nil {
        t.Errorf("Expected failed retrieving start node when inner automat is nil")
    }
    A = NewQGraph(automatFn, tasks)
    if s0, _ = A.startState("") ; s0 == nil || s0.ID() != "ID1" {
        t.Errorf("Expected retrieving start node ID1 in history, but failed")
    }
    if s0, _ = A.startState("ID2") ; s0 == nil || s0.ID() != "ID2" {
        t.Errorf("Expected retrieving start node ID2, but failed")
    }
    if s0, _ = A.startState("IDn") ; s0 != nil {
        t.Errorf("Expected failed retrieving non-existant node as start node")
    }
    if s0, _ = A.startState("IDn",true) ; s0 == nil || s0.ID() != "ID1" {
        t.Errorf("Expected retrieving start node ID1 as first node, but failed")
    }
    A.Automat().Queue().Append("IDn")
    if s0, _ = A.startState("") ; s0 != nil {
        t.Errorf("Expected failed retrieving non-existant node as start node even put in history")
    }
    // empty history
    for A.Automat().Queue().Pop() != "" { ; }
    if s0, _ = A.startState("") ; s0 != nil {
        t.Errorf("Expected failed retrieving non-existant node as start node even put in history")
    }
    // empty automat :
    B := NewQGraph("",nil)
    if s0, _ = B.startState("", true) ;  s0 != nil {
        t.Errorf("Expected failed retrieving non-existant node in an empty automat")
    }
}

func TestBasics ( t *testing.T ) {
    A := NewQGraph("%YAML 1.1",nil)
    if A != nil {
        t.Errorf("Expected failed creating automat with an empty YAML string")
    }
    A = NewQGraph(automatFn, tasks)
    if A == nil {
        t.Errorf("Failed building the automat from '%s'", automatFn)
    }
    if A.Filename() != automatFn {
        t.Errorf("Expected '%s', got '%s'", automatFn, A.Filename())
    }
    if A.Save("") != nil {
        t.Errorf("Failed printing to stdout")
    }
}
