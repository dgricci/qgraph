package types

import (
    "testing"
)

func TestMemo(t *testing.T) {
    m := NewMemo()
    m.SetZone("","foo")
    if m.Zone("") != nil {
        t.Errorf(messages["shouldHaveFailed"], "Storage, Zone identifier is empty")
    }
    m.SetZone("1", "foo")
    b := m.Zone("1")
    if b == nil {
        t.Errorf(messages["shouldntHaveFailed"], "Storage, Zone '1'")
    }
    if b.Value().(string) != "foo" {
        t.Errorf(messages["shouldntHaveFailed"], "Storage, Zone '1' foo")
    }
    bar := []byte{'b', 'a', 'r'}
    m.SetZone("1", bar)
    b= m.Zone("1")
    if b == nil {
        t.Errorf(messages["shouldntHaveFailed"], "Storage, Zone '1' again")
    }
    v := b.Value()
    switch v.(type) {
    case []byte : break
    default     :
        t.Errorf(messages["shouldntHaveFailed"], "Storage, Zone '1' is []byte")
    }
    vb := v.([]byte)
    if len(vb) != len(bar) {
        t.Errorf(messages["shouldntHaveFailed"], "Storage, Zone '1' has different length")
    }
    for i, o := range vb {
        if bar[i] != o {
            t.Errorf(messages["shouldntHaveFailed"], "Storage, Zone '1' '%d' byte differs : '%s' -> '%s'",i,bar[i],o)
        }
    }
    m.SetZone("1",map[string]FTask{}) // []FTask not supported => removing of the value !
    if m.Zone("1").Value() != nil {
        t.Errorf(messages["shouldntHaveFailed"], "Storage, Zone '1' value has been removed.")
    }
    m.DelZone("1")
    if m.Zone("1") != nil {
        t.Errorf(messages["shouldntHaveFailed"], "Storage, Zone '1' has been removed.")
    }
}

func TestMemo_check (t *testing.T) {
    A := NewAutomat(nil)
    S := NewState(A)
    S.SetID("S1")
    S.SetText("Text1")
    S.SetZoneID("Z1")
    SQ := NewSimpleQuestion(S)
    SQ.SetNextStateID("S2")
    S.SetQuestion(SQ)
    A.SetState(S)

    S = NewState(A)
    S.SetID("S2")
    S.SetZoneID("Z2")
    S.SetInput(MEMOBUFFER)
    SQ = NewSimpleQuestion(S)
    SQ.SetNextStateID("M3")
    S.SetQuestion(SQ)
    A.SetState(S)

    S = NewState(A)
    S.SetID("M3")
    S.SetText("Text2")
    S.SetZoneID("Z1")
    MCQ := NewChoicesQuestion(S)
    C := NewChoice(S)
    C.SetText("Choice1")
    C.SetMemorize(true)
    C.SetNextStateID("S3")
    MCQ.AddChoice(C)
    C = NewChoice(S)
    C.SetText("Choice2")
    C.SetMemorize(false)
    C.SetNextStateID("S3")
    MCQ.AddChoice(C)
    S.SetQuestion(MCQ)
    A.SetState(S)

    S = NewState(A)
    S.SetID("S3")
    S.SetText("Text3")
    S.SetZoneID("Z2")
    SQ = NewSimpleQuestion(S)
    SQ.SetNextStateID("")
    S.SetQuestion(SQ)
    A.SetState(S)

    if A.Check() != nil {//1
        t.Errorf(messages["shouldntHaveFailed"], "storage-check")
    }

    S = A.StateByID("S1")
    S.SetZoneID("Z0")
    if A.Check() != nil {//2
        t.Errorf(messages["shouldntHaveFailed"], "Z0")
    }

    cs := MCQ.Choices()
    cs[0].(*Choice).SetMemorize(false)
    if A.Check() == nil {//3
        t.Errorf(messages["shouldHaveFailed"], "memorize")
    }
    S.SetZoneID("Z1")
    cs[0].(*Choice).SetMemorize(true)
    if A.Check() != nil {//4
        t.Errorf(messages["shouldntHaveFailed"], "memorize")
    }

    S = A.StateByID("S3")
    S.SetZoneID("Z3")
    if A.Check() == nil {//5
        t.Errorf(messages["shouldHaveFailed"], "Z3")
    }
    S.SetZoneID("Z2")

    S = A.StateByID("S2")
    S.SetInput(INPUT)
    if A.Check() == nil {//6
        t.Errorf(messages["shouldHaveFailed"], "MEMOBUFFER")
    }
    S.SetInput(MEMOBUFFER)
    S.SetZoneID("")
    if A.Check() == nil {//7
        t.Errorf(messages["shouldHaveFailed"], "no MemoZone")
    }
    S.SetZoneID("Z2")
    if A.Check() != nil {//8
        t.Errorf(messages["shouldntHaveFailed"], "storage-check again")
    }
}