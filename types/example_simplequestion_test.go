package types

import (
    "fmt"
)

func ExampleSimpleQuestion() {
    A := createSimpleAutomat()

    if A.Check() == nil {
        fmt.Println("Check OK")
    }
    state := A.StateByID("Login")
    question := state.Question()
    if question.IsSimple() {
        fmt.Println("Simple OK")
    }
    simpleQ := question.(*SimpleQuestion)
    if simpleQ.NextState() != nil {
        fmt.Println("Next OK")
    }
    if simpleQ.QuestionState() == state {
        fmt.Println("State OK")
    }
    if simpleQ.ResponseType().Is(STRING) {
        fmt.Println(STRING.Name())
    }
    fuzz, unit := simpleQ.ResponseMetadata()
    if fuzz == 0 && unit == "" {
        fmt.Println("No metadata")
    }
    _, low, upper, _ := simpleQ.Interval()
    if low == nil && upper == nil {
        fmt.Println("No interval")
    }
    // Output:
    // Check OK
    // Simple OK
    // Next OK
    // State OK
    // STRING
    // No metadata
    // No interval
}

func ExampleSimpleQuestion_intervalInt() {
    A := NewAutomat(nil)
    S := NewState(A)
    S.SetID("Age")
    S.SetText("Gimme your age")
    SQ := NewSimpleQuestion(S)
    SQ.SetResponseType(IInINTERVAL)
    SQ.SetResponseFuzziness(1)
    SQ.SetResponseUnit("year")
    SQ.SetInterval(false,int64(0),int64(120),true)
    S.SetQuestion(SQ)
    A.SetState(S)

    if A.Check() == nil {
        fmt.Println("Check OK")
    }
    state := A.StateByID("Age")
    question := state.Question()
    simpleQ := question.(*SimpleQuestion)
    fuzz, unit := simpleQ.ResponseMetadata()
    if fuzz == 1 && unit == "year" {
        fmt.Println("Metadata OK")
    }
    LowIn, low, upper, UpperIn := simpleQ.Interval()
    if !LowIn && low == int64(0) && UpperIn && upper == int64(120) {
        fmt.Println("Interval OK")
    }
    if simpleQ.NextState() == EXITSTATE {
        fmt.Println("Next OK")
    }
    // Output:
    // Check OK
    // Metadata OK
    // Interval OK
    // Next OK
}

func ExampleSimpleQuestion_intervalFloat() {
    A := NewAutomat(nil)
    S := NewState(A)
    S.SetID("Height")
    S.SetText("Gimme your height")
    SQ := NewSimpleQuestion(S)
    SQ.SetResponseType(DInINTERVAL)
    SQ.SetResponseFuzziness(1)
    SQ.SetResponseUnit("cm")
    SQ.SetInterval(true,float64(20.0),float64(250.0),false)
    S.SetQuestion(SQ)
    A.SetState(S)

    if A.Check() == nil {
        fmt.Println("Check OK")
    }
    state := A.StateByID("Height")
    question := state.Question()
    simpleQ := question.(*SimpleQuestion)
    fuzz, unit := simpleQ.ResponseMetadata()
    if fuzz == 1 && unit == "cm" {
        fmt.Println("Metadata OK")
    }
    LowIn, low, upper, UpperIn := simpleQ.Interval()
    if LowIn && low == float64(20.0) && !UpperIn && upper == float64(250.0) {
        fmt.Println("Interval OK")
    }
    if simpleQ.NextState() == EXITSTATE {
        fmt.Println("Next OK")
    }
    // Output:
    // Check OK
    // Metadata OK
    // Interval OK
    // Next OK
}
