package types

import (
    "fmt"

    "gitlab.com/dgricci/qgraph/util"
)

// Queue type for storing states' history path.
//
type Queue struct {
    history []string                `yaml:"Queue.*"`            // history of states
}

// NewQueue function initializes a `Queue` struct.
//
func NewQueue () *Queue {
    return &Queue{
        history: make([]string,0),
    }
}

// History method returns the visited states from the older to the newer.
//
func (q *Queue) History () []string {
    h := make([]string,len(q.history))
    copy(h, q.history)
    return h
}

// Append method adds a new state to the history.
func (q *Queue) Append (id string) {
    q.history = append(q.history, id)
}

// Pop method removes the last visited state to the history.
//
func (q *Queue) Pop () string {
    l := len(q.history)
    switch l {
    case 0  :
        return ""
    default :
        var id string
        id, q.history = q.history[l-1], q.history[:l-1]
        return id
    }
}

// Check method verifies the `Queue` content.
// It checks what follows :
// * All referenced `State` exist.
func (q *Queue) Check (a *Automat) error {
    for _, sid := range q.History() {
        if s := a.StateByID(sid) ; s == nil {
            return fmt.Errorf(util.T("stateInQueueNotDefined"), sid)
        }
    }
    return nil
}
