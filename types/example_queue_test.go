package types

import (
    "fmt"
)

func ExampleQueue() {
    A := NewAutomat(nil)
    S := NewState(A)
    S.SetID("S1")
    S.SetText("Step 1")
    DA := NewDefaultAnswer(S)
    S.SetDefaultAnswer(DA)
    SQ := NewSimpleQuestion(S)
    SQ.SetNextStateID("S2")
    S.SetQuestion(SQ)
    A.SetState(S)

    S = NewState(A)
    S.SetID("S2")
    S.SetText("Step 2")
    S.SetZoneID("Z")
    MCQ := NewChoicesQuestion(S)
    C := NewChoice(S)
    C.SetText("A")
    C.SetMemorize(true)
    C.SetNextStateID("S1")
    MCQ.AddChoice(C)
    C = NewChoice(S)
    C.SetText("B")
    C.SetNextStateID("S2")
    S.SetQuestion(MCQ)
    A.SetState(S)

    A.Queue().Append("S1")
    A.Queue().Append("S2")
    A.Queue().Append("S2")

    if A.Check() == nil {
        fmt.Println("Check OK")
    }
    queue := A.Queue()
    for _, s := range queue.History() {
        fmt.Println(s)
    }
    // Output:
    // Check OK
    // S1
    // S2
    // S2
}
