package types

import (
    "gitlab.com/dgricci/enum"
)

// Peripherals type for devices enumeration where to read/write
// answers/responses.
//
type Peripherals    enum.Enumeration

// Peripheral type for device enumeration item.
//
type Peripheral     enum.EnumerationItem

// Some known peripherals :
//
// - `OUTPUT` as stdout ;
//
// - `INPUT` as stdin ;
//
// - `MEMOBUFFER` which is one of the memorisation zone.
//
// - `NONE` as /dev/null
//
var (
    PERIPHERALS         Peripherals
    OUTPUT              Peripheral
    INPUT               Peripheral
    MEMOBUFFER          Peripheral
    NONE                Peripheral
    PeripheralsNames                = []string{"OUTPUT","INPUT","MEMOBUFFER","NONE"}
)
