package types

import (
    "fmt"
)

func ExampleMemo() {
    A := NewAutomat(nil)
    S := NewState(A)
    S.SetID("S1")
    S.SetText("Step 1")
    DA := NewDefaultAnswer(S)
    S.SetDefaultAnswer(DA)
    SQ := NewSimpleQuestion(S)
    SQ.SetNextStateID("S2")
    S.SetQuestion(SQ)
    A.SetState(S)

    S = NewState(A)
    S.SetID("S2")
    S.SetText("Step 2")
    S.SetZoneID("Z")
    S.SetInput(MEMOBUFFER)
    MCQ := NewChoicesQuestion(S)
    C := NewChoice(S)
    C.SetText("A")
    C.SetMemorize(true)
    C.SetNextStateID("S1")
    MCQ.AddChoice(C)
    C = NewChoice(S)
    C.SetText("B")
    C.SetNextStateID("S2")
    S.SetQuestion(MCQ)
    A.SetState(S)

    A.Memo().SetZone("Z", "A")

    if A.Check() == nil {
        fmt.Println("Check OK")
    }
    memo := A.Memo()
    for z, b := range memo.Zones() {
        fmt.Println(z)
        fmt.Printf("%v\n",b.Value())
    }
    // Output:
    // Check OK
    // Z
    // A
}

