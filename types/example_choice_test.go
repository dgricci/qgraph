package types

import (
    "fmt"
)

func ExampleChoice() {
    A := createSimpleAutomat()

    if A.Check() == nil {
        fmt.Println("Check OK")
    }
    state := A.StateByID("Profile")
    question := state.Question()
    choicesQ := question.(*ChoicesQuestion)
    choices := choicesQ.Choices()
    if len(choices) > 0 {
        fmt.Println("Choices OK")
    }
    var c *Choice
    for _, choice := range choices {
        if choice.IsChoice() {
            fmt.Println("  Choice OK")
            c = choice.(*Choice)
            if c.Task() == nil {
                fmt.Println("  Task OK")
            }
            t := c.TaskID()
            c.SetTaskID("unknown")
            if c.Check() != nil {
                fmt.Println("  Missing Task OK")
            }
            c.SetTaskID(t)
            if c.NextState() != nil {
                fmt.Println("  Next OK")
            }
            if c.AnswerState() == state {
                fmt.Println("  State OK")
            }
            if c.Text() != "" {
                fmt.Println("  "+c.Text())
            }
            switch c.Text() {
            case "User" :
                if !c.Memorize() {
                    fmt.Println("  Memorize OK")
                }
            case "Admin":
                if c.Memorize() {
                    fmt.Println("  Memorize OK")
                }
            }
        }
    }
    // Output:
    // Check OK
    // Choices OK
    //   Choice OK
    //   Task OK
    //   Missing Task OK
    //   Next OK
    //   State OK
    //   User
    //   Memorize OK
    //   Choice OK
    //   Task OK
    //   Missing Task OK
    //   Next OK
    //   State OK
    //   Admin
    //   Memorize OK
}

