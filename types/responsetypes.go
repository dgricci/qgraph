package types

import (
    "gitlab.com/dgricci/enum"
)

// ResponseTypes type for response enumeration to simple question.
//
type ResponseTypes  enum.Enumeration

// ResponseType type for response enumeration item.
//
type ResponseType   enum.EnumerationItem

// Some known response's types :
//
// - `INT` for integer numbers ;
//
// - `DBL` for float numbers ;
//
// - `BOOL` for booleans ;
//
// - `STRING` for characters strings ;
//
// - `IInINTERVAL` for integers numbers in an interval ;
//
// - `DInINTERVAL` for float numbers in an interval.
//
var (
    RESPONSETYPES       ResponseTypes
    INT                 ResponseType
    DBL                 ResponseType
    BOOL                ResponseType
    STRING              ResponseType
    IInINTERVAL         ResponseType
    DInINTERVAL         ResponseType
    ResponseTypesNames              = []string{"INT","DBL","BOOL","STRING","IInINTERVAL","DInINTERVAL"}
)
