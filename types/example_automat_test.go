package types

import (
    "fmt"
)

func ExampleAutomat() {
    A := createSimpleAutomat()

    if A.Check() == nil {
        fmt.Println("Check OK")
    }
    for _, id := range A.States() {
        state := A.StateByID(id)
        if state != nil {
            fmt.Println(id)
        }
    }
    if A.StateByID("Foo") == nil {
        fmt.Println("Foo not found")
    }
    if A.Queue() != nil {
        fmt.Println("Queue()")
    }
    if A.Memo() != nil {
        fmt.Println("Memo()")
    }
    if A.Tasks() != nil {
        fmt.Println("Tasks()")
    }
    for _, sid := range A.States(func(s *State) bool {
        zid := s.ZoneID()
        if zid == "LID" || zid == "PID" {
            return true
        }
        return false
    }) {
        state := A.StateByID(sid)
        if state != nil {
            fmt.Println("filtered: "+sid)
        }
    }
    if A.States(FirstOnly)[0] == "Login" {
        fmt.Println("Login is first")
    }
    // Unordered output:
    // Check OK
    // Login
    // Profile
    // Hello
    // Foo not found
    // Queue()
    // Memo()
    // Tasks()
    // filtered: Login
    // filtered: Profile
    // filtered: Hello
    // Login is first
}

