package types

import (
    "fmt"
)

func ExampleChoicesQuestion() {
    A := createSimpleAutomat()

    if A.Check() == nil {
        fmt.Println("Check OK")
    }
    state := A.StateByID("Profile")
    question := state.Question()
    if !question.IsSimple() {
        fmt.Println("Choices OK")
    }
    choicesQ := question.(*ChoicesQuestion)
    if choicesQ.NextState() == nil {
        fmt.Println("Next OK")
    }
    if choicesQ.QuestionState() == state {
        fmt.Println("State OK")
    }
    if choicesQ.Display() {
        fmt.Println("Display OK")
    }
    if !choicesQ.Learn() {
        fmt.Println("Learn OK")
    }
    if len(choicesQ.Choices()) == 2 {
        fmt.Println("Choices OK")
    }
    // Output:
    // Check OK
    // Choices OK
    // Next OK
    // State OK
    // Display OK
    // Learn OK
    // Choices OK
}

func ExampleChoicesQuestion_Learn() {
    A := NewAutomat(map[string]FTask{
        "Dump"  :   func (s *State, r interface{}) error {
            fmt.Printf("%+v\n", s.Automat())
            return nil
        },
    })
    S := NewState(A)
    S.SetID("Learn")
    S.SetText("What is your first name ?")
    DA := NewDefaultAnswer(S)
    DA.SetTaskID("Dump")
    S.SetDefaultAnswer(DA)
    MCQ := NewChoicesQuestion(S)
    MCQ.SetDisplay(true)
    MCQ.SetLearn(true)
    if MCQ.Check() != nil {
        fmt.Println("Check MCQ OK")
    }
    C := NewChoice(S)
    C.SetText("Alice")
    C.SetNextStateID("Learn")
    MCQ.AddChoice(C)
    C = NewChoice(S)
    C.SetText("Bob")
    C.SetNextStateID("Learn")
    MCQ.AddChoice(C)
    S.SetQuestion(MCQ)
    A.SetState(S)

    if A.Check() == nil {
        fmt.Println("Check OK")
    }
    state := A.StateByID("Learn")
    question := state.Question()
    if !question.IsSimple() {
        fmt.Println("Choices OK")
    }
    choicesQ := question.(*ChoicesQuestion)
    if choicesQ.Learn() {
        fmt.Println("Learn OK")
    }
    for _, c := range choicesQ.Choices() {
        if c.IsChoice() {
            fmt.Println("IsChoice OK")
        }
        fmt.Println(c.(*Choice).Text())
    }
    // Unordered output:
    // Check MCQ OK
    // Check OK
    // Choices OK
    // Learn OK
    // IsChoice OK
    // Alice
    // IsChoice OK
    // Bob
}

