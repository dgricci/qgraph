package types

import (
    "testing"
    "bytes"
)

func TestBuffer(t *testing.T) {
    B := NewBuffer()
    v0 := "StringValue"
    B.SetValue(v0)
    if B.Value().(string) != v0 {
        t.Errorf(messages["shouldntHaveFailed"], v0)
    }
    v1 := []byte{'S', 't', 'r', 'i', 'n', 'g', 'V', 'a', 'l', 'u', 'e'}
    B.SetValue(v1)
    if !bytes.Equal(v1,B.Value().([]byte)) {
        t.Errorf(messages["shouldntHaveFailed"], string(v1[:3]))
    }
    if nw, _ := B.Write([]byte(v0)) ; nw == 0 {
        t.Errorf(messages["shouldntHaveFailed"], "Write")
    }
    var v [20]byte
    if n, err := B.Read(v[:]) ; err != nil && n != len(v0) {
        t.Errorf(messages["shouldntHaveFailed"], "Read")
    }
    if v0 != string(v[:len(v0)]) {
        t.Errorf(messages["shouldntHaveFailed"], string(v[:]))
    }
    B.SetValue(1)
    if B.Value().(string) != "1" {
        t.Errorf(messages["shouldntHaveFailed"], "1")
    }
    B.SetValue(float64(1))
    if B.Value().(string) != "1" {
        t.Errorf(messages["shouldntHaveFailed"], "float64(1.0)")
    }

    if n, err := B.Write(nil) ; n != 0 && err != nil {
        t.Errorf(messages["shouldntHaveFailed"], "Write(nil)")
    }
    if n, err := B.Read(v[:]) ; n != 0 && err != nil {
        t.Errorf(messages["shouldntHaveFailed"], "Read nil")
    }

    B.SetValue(v0)
    if n, err := B.Read(v[:]) ; err != nil && n != len(v0) {
        t.Errorf(messages["shouldntHaveFailed"], "Read string")
    }
    if n, err := B.Read(v[0:0]) ; err != nil && n != 0 {
        t.Errorf(messages["shouldntHaveFailed"], "Read string with empty buffer beyond offset")
    }
    B.Reset()
    var vs [5]byte
    if n, err := B.Read(vs[:]) ; err != nil && n != 5 {
        t.Errorf(messages["shouldntHaveFailed"], "Read short string")
    }
    B.SetValue(v1)
    var vl[20]byte
    if n, err := B.Read(vl[:]) ; err != nil && n != 11 {
        t.Errorf(messages["shouldntHaveFailed"], "Read long []byte")
    }
    if n, err := B.Read(vl[:]) ; err == nil || n != 0 {
        t.Errorf(messages["shouldntHaveFailed"], "Read long []byte beyond offset")
    }
    B.Read(vl[:])
    if n, err := B.Read(v[0:0]) ; err != nil && n != 0 {
        t.Errorf(messages["shouldntHaveFailed"], "Read []byte with empty buffer beyond offset")
    }
}
