// Package types for defining types needed for qgraph package
package types

import (
    "github.com/nicksnyder/go-i18n/i18n"

    "gitlab.com/dgricci/enum"
    qu "gitlab.com/dgricci/qgraph/util"
)

// Question interface for holding either simple or multiple choices questions
//
type Question interface {
    IsSimple()              bool                                // simple or multiple choices question ?
    QuestionState()         *State                              // current State for question
    Task()                  FTask                               // task func
    TaskID()                string                              // getter of task's identifier
    SetTaskID(string)                                           // setter of task's identifier
    NextState()             *State                              // next State to go ; when `EXITSTATE` exits the Automaton
    Check()                 error                               // content checker
}

// Answer interface for default answer and choice.
//
type Answer interface {
    IsChoice()              bool                                // multiple choices question ?
    Task()                  FTask                               // task func
    TaskID()                string                              // getter of task's identifier
    SetTaskID(string)                                           // setter of task's identifier
    AnswerState()           *State                              // current State for answer
    NextState()             *State                              // next State to go ; when `EXITSTATE` exits the Automaton
    SetNextStateID(string)                                      // next State's identifier setter
    NextStateID()           string                              // next State's identifier getter
    Check()                 error                               // content checker
}

// init function to set global variables (Enumeration)
//
func init () {
    i18n.MustLoadTranslationFile("i18n/en-us.all.json")
    i18n.MustLoadTranslationFile("i18n/fr-fr.all.json")

    PERIPHERALS     = Peripherals(enum.NewEnumeration("Peripherals", PeripheralsNames))
    OUTPUT, _       = PERIPHERALS.Get("OUTPUT")
    INPUT, _        = PERIPHERALS.Get("INPUT")
    MEMOBUFFER, _   = PERIPHERALS.Get("MEMOBUFFER")
    NONE, _         = PERIPHERALS.Get("NONE")

    RESPONSETYPES   = ResponseTypes(enum.NewEnumeration("ResponseTypes", ResponseTypesNames))
    INT, _          = RESPONSETYPES.Get("INT")
    DBL, _          = RESPONSETYPES.Get("DBL")
    BOOL, _         = RESPONSETYPES.Get("BOOL")
    STRING, _       = RESPONSETYPES.Get("STRING")
    IInINTERVAL, _  = RESPONSETYPES.Get("IInINTERVAL")
    DInINTERVAL, _  = RESPONSETYPES.Get("DInINTERVAL")

    EXITSTATE = &State{
        id      : "",
        toQ     : "",
        dAns    : nil,
        prolog  : "",
        epilog  : "",
        memoZone: "",
        input   : NONE,
        r       : nil,
        output  : NONE,
        w       : nil,
        q       : nil,
        a       : nil,
    }

    qu.Log().Println("gitlab.com/dgricci/qgraph/types loaded")
}
