package types

import (
    "fmt"
    "io"
    "os"

    "gitlab.com/dgricci/qgraph/util"
)

// EXITSTATE a constant for all qgraph for handling exit state :
var EXITSTATE *State

// State type for Petri-graph node informations.
//
type State struct {
    id          string              `yaml:"Id"`                 // identifier of State
    toQ         string              `yaml:"ToQ"`                // text of question related to this State
    dAns        Answer              `yaml:"DefaultAnswer"`      // default answer (no answer given)
    prolog      string              `yaml:"Prolog"`             // function to launch when accessing this State
    epilog      string              `yaml:"Epilog"`             // function to launch when leaving this State
    memoZone    string              `yaml:"Memo"`               // identifier of the placeholder to use
    input       Peripheral          `yaml:"Input"`              // peripheral where to read the answer
    r           io.Reader
    output      Peripheral          `yaml:"Output"`             // peripheral where to write question, result
    w           io.Writer
    q           Question            `yaml:"SQ|MCQ"`             // either a simple question or multiple choices question
    a           *Automat                                        // the automaton of this state
}

// NewState function initializes a `State` struct.
//
func NewState (a *Automat) *State {
    return &State{
        id      : "",
        toQ     : "",
        dAns    : nil,
        prolog  : "",
        epilog  : "",
        memoZone: "",
        input   : INPUT,
        r       : os.Stdin,
        output  : OUTPUT,
        w       : os.Stdout,
        q       : nil,
        a       : a,
    }
}

// ID method gets State's identifier
//
func (s *State) ID () string {
    return s.id
}

// SetID method sets State's identifier
//
func (s *State) SetID (id string) {
    if s != EXITSTATE {
        s.id = id
    }
}

// Text method gets the State's text.
//
func (s *State) Text () string {
    return s.toQ
}

// SetText method assigns the State's text.
//
func (s *State) SetText (t string) {
    if s != EXITSTATE {
        s.toQ= t
    }
}

// DefaultAnswer method returns the state's default answer, if any.
//
func (s *State) DefaultAnswer () Answer {
    return s.dAns
}

// SetDefaultAnswer method assigns an `Answer` to this `State`.
//
func (s *State) SetDefaultAnswer (a Answer) {
    if s != EXITSTATE {
        s.dAns = a
    }
}

// Prolog method returns the task acting as prolog of this state, if any.
//
func (s *State) Prolog () FTask {
    return s.a.tasks[s.prolog]
}

// SetPrologID method assigns the task's identifier acting as prolog of this
// state.
//
func (s *State) SetPrologID (t string) {
    if s != EXITSTATE {
        s.prolog = t
    }
}

// PrologID method returns the task's identifier acting as prolog of this
// state.
//
func (s *State) PrologID () string {
    return s.prolog
}

// Epilog method returns the task acting as epilog of this state, if any.
//
func (s *State) Epilog () FTask {
    return s.a.tasks[s.epilog]
}

// SetEpilogID method assigns the task's identifier acting as epilog of this
// state.
//
func (s *State) SetEpilogID (t string) {
    if s != EXITSTATE {
        s.epilog = t
    }
}

// EpilogID method returns the task's identifier acting as epilog of this
// state.
//
func (s *State) EpilogID () string {
    return s.epilog
}

// Zone method returns the storage zone, if any.
//
func (s *State) Zone () *Buffer {
    return s.a.memo.Zone(s.memoZone)
}

// ZoneID method returns the storage zone identifier of this state, if any.
//
func (s *State) ZoneID () string {
    return s.memoZone
}

// SetZoneID method assigns the storage zone's identifier.
//
func (s *State) SetZoneID (z string) {
    if s != EXITSTATE {
        s.memoZone = z
        if MEMOBUFFER.Is(s.input) { s.SetInputPeripheral(nil) }
        if MEMOBUFFER.Is(s.output) { s.SetOutputPeripheral(nil) }
    }
}

// Input method returns the peripheral used to read answers.
//
func (s *State) Input () Peripheral {
    return s.input
}

// SetInput methos assigns an input peripheral to this `State`.
//
func (s *State) SetInput (i Peripheral) {
    if s != EXITSTATE {
        s.input = i
        s.SetInputPeripheral(nil)
    }
}

// InputPeripheral method returns the reader associated with the peripheral.
// Defaults is `os.Stdin`.
func (s *State) InputPeripheral () io.Reader {
    return s.r
}

// SetInputPeripheral method assigns the reader to be associated with the peripheral.
// when nil is given, default readers are applied :
//
// * INPUT : os.Stdin
//
// * MEMOBUFFER : the Buffer of the zone associated with this state
//
func (s *State) SetInputPeripheral ( reader io.Reader ) {
    if s == EXITSTATE { return }
    if reader != nil {
        s.r = reader
        return
    }
    switch s.input {
    case MEMOBUFFER : s.r = s.Zone()
    default         : s.r = os.Stdin
    }
}

// Output method returns the peripheral used to write answers.
//
func (s *State) Output () Peripheral {
    return s.output
}

// SetOutput method assigns an output peripheral to this `State`.
//
func (s *State) SetOutput (o Peripheral) {
    if s != EXITSTATE {
        s.output = o
        s.SetOutputPeripheral(nil)
    }
}

// OutputPeripheral method returns the writer associated with the peripheral.
// Defaults is `os.Stdout`.
func (s *State) OutputPeripheral () io.Writer {
    return s.w
}

// SetOutputPeripheral method assigns the reader to be associated with the peripheral.
// when nil is given, default readers are applied :
//
// * OUTPUT : os.Stdout
//
// * MEMOBUFFER : the Buffer of the zone associated with this state
//
func (s *State) SetOutputPeripheral ( writer io.Writer ) {
    if s == EXITSTATE { return }
    if writer != nil {
        s.w = writer
        return
    }
    switch s.output {
    case MEMOBUFFER : s.w = s.Zone()
    default         : s.w = os.Stdout
    }
}

// Question method returns the `Question` interface of this state.
//
func (s *State) Question () Question {
    return s.q
}

// SetQuestion method assigns a `Question` interface to this state.
func (s *State) SetQuestion (q Question) {
    if s != EXITSTATE {
        s.q = q
    }
}

// Automat method returns the `Automat` this state belongs to.
//
func (s *State) Automat () *Automat {
    return s.a
}

// Check method validates the `State`.
// It checks what follows :
// * There is at least one State ;
// * if a DefaultAnswer is defined, it must be valid ;
// * All `Prolog` et `Epilog` tasks are defined ;
// * a Question is defined (simple or multiple choices) and is valid ;
// * when using a MemoZone, either a SimpleQuestion or one Choice of a ChoicesQuestion uses it ;
// * when no text of question is given (state under condition) then the input must be MEMOBUFFER and MemoZone must be set. Moreover,
//   it exist one state that sets the MemoZone.
func (s *State) Check () error {
    var err error

    w := s.DefaultAnswer()
    if w != nil {
        if err= w.(*DefaultAnswer).Check() ; err != nil {
            return err
        }
    }
    if s.PrologID() != "" && s.Prolog() == nil {
        return fmt.Errorf(util.T("unknownReferencedTaskAsProlog"), s.PrologID(), s.ID())
    }
    if s.EpilogID() != "" && s.Epilog() == nil {
        return fmt.Errorf(util.T("unknownReferencedTaskAsEpilog"), s.EpilogID(), s.ID())
    }
    q := s.Question()
    if q == nil {
        return fmt.Errorf(util.T("noQuestionDefined"), s.ID())
    }
    if q.IsSimple() {
        if err= q.(*SimpleQuestion).Check() ; err != nil {
            return err
        }
    } else {
        if err= q.(*ChoicesQuestion).Check() ; err != nil {
            return err
        }
    }

    zid := s.ZoneID()
    if zid != "" {// find all states using this storage area
         sz := s.Automat().States(func (e *State) bool {
            return e.ZoneID() == zid
        })
        valid := false
        for _, sid := range sz {
            cs := s.Automat().StateByID(sid)
            if cs.Text() == "" {
                continue
            } // under condition state
            qsid := cs.Question()
            if qsid.IsSimple() {// simple question for storing answer
                valid = true
                break;
            }
            cz := qsid.(*ChoicesQuestion).Choices(func (c Answer) bool {
                return c.(*Choice).Memorize()
            })
            if len(cz) > 0 {// multiple choices question using this storing area
                valid = true
                break
            }
            break
        }
        if !valid {
            return fmt.Errorf(util.T("unusedStorage"), zid, s.ID())
        }
    }

    if s.Text() == "" {
        if !s.Input().Is(MEMOBUFFER) {
            return fmt.Errorf(util.T("underConditionStateBadInput"), s.ID())
        }
        if zid == "" {
            return fmt.Errorf(util.T("underConditionStateStorageNotDefined"), s.ID())
        }
    }

    return nil
}
