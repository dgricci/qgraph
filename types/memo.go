package types

// Memo type for holding storage zones.
//
type Memo struct {
    zones   map[string]*Buffer      `yaml:"Zone.Id"`            // collection of memory's zones
}

// NewMemo function initializes a `Memo` struct.
//
func NewMemo () *Memo {
    return &Memo{
        zones   : make(map[string]*Buffer),
    }
}

// Zone method returns a `*Buffer` from its identifier, if any.
//
func (m *Memo) Zone ( id string ) *Buffer {
    if id == "" { return nil }
    if z, exists := m.zones[id] ; exists {
        return z
    }
    return nil
}

// SetZone method assigns a value to an identified zone.
// Removes any value in this zone if one exists.
//
func (m *Memo) SetZone ( id string, val interface{} ) {
    if id == "" { return }
    if z, exists := m.zones[id] ; exists {
        m.zones[id]= nil
        z.SetValue(nil)
        z= nil
    }
    b := NewBuffer()
    b.SetValue(val)
    m.zones[id]= b
}

// DelZone method removes an identified zone.
//
func (m *Memo) DelZone ( id string ) {
    if z, exists := m.zones[id] ; exists {
        m.zones[id]= nil
        z.SetValue(nil)
        z= nil
        delete(m.zones, id)
    }
}

// Zones method returns all zones.
//
func (m *Memo) Zones () map[string]*Buffer {
    return m.zones
}
