package types

import (
    "fmt"
)

func ExampleState() {
    A := createSimpleAutomat()

    if A.Check() == nil {
        fmt.Println("Check OK")
    }
    state := A.StateByID("Login")
    if state.ID() == "Login" {
        fmt.Println("Login")
    }
    if state.Text() == "What's is your identifier ?" {
        fmt.Println("Text OK")
    }
    if state.DefaultAnswer() != nil {
        fmt.Println("DefaultAnswer OK")
    }
    if state.Prolog() != nil {
        fmt.Println("Banner OK")
    }
    t := state.PrologID()
    state.SetPrologID("unknownP")
    if state.Check() != nil {
        fmt.Println("Check Prolog OK")
    }
    state.SetPrologID(t)
    if state.Epilog() == nil {
        fmt.Println("Epilog OK")
    }
    t = state.EpilogID()
    state.SetEpilogID("unkownE")
    if state.Check() != nil {
        fmt.Println("Check Epilog OK")
    }
    state.SetEpilogID(t)
    if state.ZoneID() != "" {
        fmt.Println("MemoZone OK")
    }
    if state.Zone() == nil {
        fmt.Println("Zone OK")
    }
    if state.Input().Is(INPUT) {
        fmt.Println("INPUT")
    }
    if state.Output().Is(OUTPUT) {
        fmt.Println("OUTPUT")
    }
    if state.Question() != nil {
        fmt.Println("Question OK")
    }
    if state.Automat() == A {
        fmt.Println("Automat OK")
    }
    // Output:
    // Check OK
    // Login
    // Text OK
    // DefaultAnswer OK
    // Banner OK
    // Check Prolog OK
    // Epilog OK
    // Check Epilog OK
    // MemoZone OK
    // Zone OK
    // INPUT
    // OUTPUT
    // Question OK
    // Automat OK
}

