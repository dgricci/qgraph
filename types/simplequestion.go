package types

import (
    "fmt"

    qu "gitlab.com/dgricci/qgraph/util"
)

// SimpleQuestion type for ... question expecting a simple answer !
//
type SimpleQuestion struct {
    rep     ResponseType            `yaml:"Type"`               // type of answer. Defaults to STRING
    fuzz    int                     `yaml:"Fuzzyness"`          // fuzzyness applying to response
    unit    string                  `yaml:"Unit"`               // unit of the response
    lowerB  interface{}             `yaml:"LowerBound.Value"`   // lower bound of response when response is a range
    upperB  interface{}             `yaml:"UpperBound.Value"`   // upper bound of response when response is a range
    inclLow bool                    `yaml:"LowerBound.Include"` // true when lower bound belows to range
    inclUpp bool                    `yaml:"UpperBound.Include"` // true when upper bound belows to range
    task    string                  `yaml:"Task"`               // optional task
    next    string                  `yaml:"Next"`               // identifier of the next State ; when "" exits the Automaton
    s       *State                                              // the current state
}

// NewSimpleQuestion initializes a `SimpleQuestion` struct.
//
func NewSimpleQuestion (s *State) *SimpleQuestion {
    return &SimpleQuestion{
        rep     :   STRING,
        fuzz    :   0,
        unit    :   "",
        lowerB  :   nil,
        upperB  :   nil,
        inclLow :   false,
        inclUpp :   false,
        task    :   "",
        next    :   "",
        s       :   s,
    }
}

// IsSimple method returns `true` for `SimpleQuestion`.
//
func (sq *SimpleQuestion) IsSimple () bool {
    return true
}

// Task method returns the type FTask to run or `nil` if none.
//
func (sq *SimpleQuestion) Task () FTask {
    return sq.s.a.tasks[sq.task]
}

// SetTaskID method assigns the task identifier.
//
func (sq *SimpleQuestion) SetTaskID (t string) {
    sq.task= t
}

// TaskID method returns the task identifier.
//
func (sq *SimpleQuestion) TaskID () string {
    return sq.task
}

// NextState method returns the `State` to go after the current one.
// When `nil` is returned, one leaves the automaton.
//
func (sq *SimpleQuestion) NextState () *State {
    if sq.next == "" {
        return EXITSTATE
    }
    return sq.s.a.states[sq.next]
}

// SetNextStateID method assigns the next `State` identifier to reach.
//
func (sq *SimpleQuestion) SetNextStateID (n string) {
    sq.next= n
}

// NextStateID method returns the next `State` identifier to reach.
//
func (sq *SimpleQuestion) NextStateID () string {
    return sq.next
}

// QuestionState method returns the current `State`.
//
func (sq *SimpleQuestion) QuestionState () *State {
    return sq.s
}

// ResponseType method returns the response's type of the simple question.
//
func (sq *SimpleQuestion) ResponseType () ResponseType {
    return sq.rep
}

// SetResponseType method assigns the response's type of the simple question.
//
func (sq *SimpleQuestion) SetResponseType (r ResponseType) {
    sq.rep= r
}

// ResponseMetadata method returns the response fuzziness and unit.
//
func (sq *SimpleQuestion) ResponseMetadata () (int, string) {
    return sq.fuzz, sq.unit
}

// SetResponseFuzziness method assigns the response fuzziness.
//
func (sq *SimpleQuestion) SetResponseFuzziness (f int) {
    sq.fuzz= f
}

// SetResponseUnit method assigns the response unit.
//
func (sq *SimpleQuestion) SetResponseUnit (u string) {
    sq.unit= u
}

// Interval method returns the response's bounds, if any.
// Interval only exists when response's type is IInINTERVAL or DInINTERVAL.
//
func (sq *SimpleQuestion) Interval () (bool, interface{}, interface{}, bool) {
    switch sq.ResponseType() {
    case IInINTERVAL :
        return sq.inclLow, sq.lowerB.(int64), sq.upperB.(int64), sq.inclUpp
    case DInINTERVAL :
        return sq.inclLow, sq.lowerB.(float64), sq.upperB.(float64), sq.inclUpp
    }
    return false, nil, nil, false
}

// SetInterval method assigns the response's bounds.
//
func (sq *SimpleQuestion) SetInterval (il bool, lo interface{}, up interface{}, iu bool) {
    sq.inclLow = il
    sq.lowerB = lo
    sq.upperB = up
    sq.inclUpp = iu
}

// IsInInterval method checks whether a value lies within interval's bounds.
//
func (sq *SimpleQuestion) IsInInterval ( v interface{} ) bool {
    switch sq.ResponseType() {
    case IInINTERVAL :
        iv := v.(int64)
        if !sq.inclLow && iv == sq.lowerB.(int64) { return false }
        if iv < sq.lowerB.(int64) { return false }
        if !sq.inclUpp && iv == sq.upperB.(int64) { return false }
        if iv > sq.upperB.(int64) { return false }
        return true
    case DInINTERVAL :
        fv := v.(float64)
        if !sq.inclLow && fv == sq.lowerB.(float64) { return false }
        if fv < sq.lowerB.(float64) { return false }
        if !sq.inclUpp && fv == sq.upperB.(float64) { return false }
        if fv > sq.upperB.(float64) { return false }
        return true
    }
    return false
}

// IntervalToString method returns a string representation of the interval
// following this syntax : (]|[)lowerValue..upperValue(]|[)
//
func (sq *SimpleQuestion) IntervalToString ( ) (s string) {
    t := sq.ResponseType()
    if t != IInINTERVAL && t != DInINTERVAL { return } // ""
    if sq.inclLow {
        s += "["
    } else {
        s += "]"
    }
    switch t {
    case IInINTERVAL :
        s += fmt.Sprint(sq.lowerB.(int64))
    case DInINTERVAL :
        s += fmt.Sprint(sq.lowerB.(float64))
    }
    s += ".."
    switch t {
    case IInINTERVAL :
        s += fmt.Sprint(sq.upperB.(int64))
    case DInINTERVAL :
        s += fmt.Sprint(sq.upperB.(float64))
    }
    if sq.inclUpp {
        s += "]" 
    } else {
        s += "["
    }
    return
}

// Check verifies the `SimpleQuestion` content
// It checks what follows :
// * `Next` field must point at an existing State `Id` ;
// * LowerBound < UpperBound
func (sq *SimpleQuestion) Check () error {
    if sq.TaskID() != "" && sq.Task() == nil {
        return fmt.Errorf(qu.T("unknownReferencedTask"), sq.TaskID(), sq.QuestionState().ID())
    }
    if sq.NextState() == nil {
        return fmt.Errorf(qu.T("nextStateNotDefined"), sq.NextStateID(), sq.QuestionState().ID())
    }
    t := sq.ResponseType()
    if t == IInINTERVAL || t == DInINTERVAL {
        // save bounds
        inclL := sq.inclLow
        inclU := sq.inclUpp
        // LowerBound <= lower < UpperBound
        sq.inclLow = true
        sq.inclUpp = false
        if !sq.IsInInterval(sq.lowerB) {
            return fmt.Errorf(qu.T("invalidBounds"), sq.IntervalToString(), sq.QuestionState().ID())
        }
        // restore bounds
        sq.inclLow = inclL
        sq.inclUpp = inclU
        if t == IInINTERVAL {
            // can we have values between lowerB and upperB ?
            if sq.upperB.(int64) - sq.lowerB.(int64) == 1 && !inclL && !inclU {
                return fmt.Errorf(qu.T("invalidBounds"), sq.IntervalToString(), sq.QuestionState().ID())
            }
        }
    }
    return nil
}
