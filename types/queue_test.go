package types

import (
    "testing"
    "fmt"
    "strings"
)

func TestQueue(t *testing.T) {
    A := NewAutomat(nil)
    if A.Check() == nil {
        t.Errorf(messages["shouldHaveFailed"], "No States defined")
    }
    S := NewState(A)
    S.SetID("S1")
    S.SetText("Step 1")
    A.SetState(S)
    if A.Check() == nil {
        t.Errorf(messages["shouldHaveFailed"], "No `Question` set to `State`")
    }
    sq := NewSimpleQuestion(S)
    S.SetQuestion(sq)
    A.Queue().Append("S2")
    if A.Check() == nil {
        t.Errorf(messages["shouldHaveFailed"], "Queued State is not defined")
    }
    if A.Queue().Pop() != "S2" {
        t.Errorf(messages["shouldntHaveFailed"], "S2 as last state")
    }
    if A.Queue().Pop() != "" {
        t.Errorf(messages["shouldntHaveFailed"], "Pop with no history")
    }
    A.Queue().Append(S.ID())
    if A.Check() != nil {
        t.Errorf(messages["shouldntHaveFailed"], "Queued State is defined")
    }
    if len(A.Queue().History()) != 1 {
        t.Errorf(messages["shouldntHaveFailed"], fmt.Sprintf("Queue has 1 state ('%s' found)",strings.Join(A.Queue().History(),", ")))
    }
}
