package types

import (
    "fmt"
)

func ExampleDefaultAnswer() {
    A := createSimpleAutomat()

    if A.Check() == nil {
        fmt.Println("Check OK")
    }
    state := A.StateByID("Login")
    answer := state.DefaultAnswer()
    if answer != nil {
        fmt.Println("DefaultAnswer OK")
    }
    if !answer.IsChoice() {
        fmt.Println("IsChoice OK")
    }
    if answer.Task() == nil {
        fmt.Println("Task OK")
    }
    answer.SetTaskID("unknown")
    if answer.Check() != nil {
        fmt.Println("Check Task OK")
    }
    answer.SetTaskID("")
    if answer.NextState() != nil {
        fmt.Println("Next OK")
    }
    if answer.AnswerState() == state {
        fmt.Println("State OK")
    }
    // Output:
    // Check OK
    // DefaultAnswer OK
    // IsChoice OK
    // Task OK
    // Check Task OK
    // Next OK
    // State OK
}

func ExampleDefaultAnswer_next() {
    A:= NewAutomat(nil)
    S := NewState(A)
    S.SetID("Hello")
    S.SetText("Who are you ?")
    DA := NewDefaultAnswer(S)
    S.SetDefaultAnswer(DA)
    SQ := NewSimpleQuestion(S)
    S.SetQuestion(SQ)
    A.SetState(S)

    if A.Check() == nil {
        fmt.Println("Check OK")
    }
    state := A.StateByID("Hello")
    answer := state.DefaultAnswer()
    if answer != nil {
        fmt.Println("DefaultAnswer OK")
    }
    if !answer.IsChoice() {
        fmt.Println("IsChoice OK")
    }
    if answer.Task() == nil {
        fmt.Println("Task OK")
    }
    if answer.NextState() == EXITSTATE {
        fmt.Println("Next OK")
    }
    if answer.AnswerState() != nil {
        fmt.Println("State OK")
    }
    // Output:
    // Check OK
    // DefaultAnswer OK
    // IsChoice OK
    // Task OK
    // Next OK
    // State OK
}

