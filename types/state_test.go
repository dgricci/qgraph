package types

import (
    "testing"
    "fmt"
    "os"
    "io"
)

func createIOAutomat() *Automat {
    A := NewAutomat(nil)
    S := NewState(A)
    S.SetID("Input")
    S.SetText("prompt")
    S.SetZoneID("1")
    SQ := NewSimpleQuestion(S)
    SQ.SetNextStateID("Output")
    S.SetInput(INPUT)
    S.SetOutput(OUTPUT)
    S.SetQuestion(SQ)
    A.SetState(S)
    A.Memo().SetZone("1", "1")

    S = NewState(A)
    S.SetID("Output")
    S.SetText("prompt")
    S.SetZoneID("2")
    SQ = NewSimpleQuestion(S)
    SQ.SetNextStateID("Memo")
    S.SetInput(INPUT)
    S.SetOutput(OUTPUT)
    S.SetQuestion(SQ)
    A.SetState(S)
    A.Memo().SetZone("2", "2")

    S = NewState(A)
    S.SetID("Memo")
    S.SetInput(MEMOBUFFER)
    S.SetZoneID("2")
    S.SetOutput(MEMOBUFFER)
    SQ = NewSimpleQuestion(S)
    S.SetQuestion(SQ)
    A.SetState(S)

    return A
}

func FileReaderTmp () (fn string, in *os.File, err error ) {
    // file with answers
    fn = fmt.Sprintf("%s%c%s", os.TempDir(), os.PathSeparator, "stdinInputs.txt")
    in, err = os.Create(fn)
    if err != nil { return }
    // Simulate user's inputs
    _, err = in.WriteString("1st\n2nd\n")
    if err != nil { return }
    err = in.Close()
    if err != nil { return }
    in, err = os.Open(fn)
    if err != nil { return }
    return
}

func TestInputPeripheral (t *testing.T) {
    A := createIOAutomat()
    S := A.StateByID("Input")
    if S.InputPeripheral() != os.Stdin {
        t.Errorf(messages["shouldntHaveFailed"], "Input as Stdin")
    }
    S.SetInputPeripheral(nil)
    if S.InputPeripheral() != os.Stdin {
        t.Errorf(messages["shouldntHaveFailed"], "Input as Stdin")
    }

    fn, in, err := FileReaderTmp()
    if err != nil {
        t.Errorf("Failed creating temporary file")
    }
    defer func () {
        os.Remove(fn)
    }()

    S.SetInputPeripheral(in)
    var v string
    fmt.Fscanln(S.InputPeripheral(),&v)
    if "1st" != v {
        t.Errorf(messages["shouldntHaveFailed"], "1st")
    }
    fmt.Fscanln(S.InputPeripheral(),&v)
    if "2nd" != v {
        t.Errorf(messages["shouldntHaveFailed"], "2nd")
    }
    in.Close()
    S.SetInputPeripheral(nil)
}

func TestOutputPeripheral (t *testing.T) {
    A := createIOAutomat()
    S := A.StateByID("Output")
    if S.OutputPeripheral() != os.Stdout {
        t.Errorf(messages["shouldntHaveFailed"], "Output as Stdout")
    }
    S.SetOutputPeripheral(nil)
    if S.OutputPeripheral() != os.Stdout {
        t.Errorf(messages["shouldntHaveFailed"], "Output as Stdout")
    }

    fn := fmt.Sprintf("%s%c%s", os.TempDir(), os.PathSeparator, "stdoutOutputs.txt")
    ut, err := os.Create(fn)
    if err != nil {
        t.Errorf("Failed creating temporary file")
    }
    defer func () {
        os.Remove(fn)
    }()

    S.SetOutputPeripheral(ut)
    fmt.Fprintf(S.OutputPeripheral(),"Question\n")
    fmt.Fprintf(S.OutputPeripheral(),"1\n")
    fmt.Fprintf(S.OutputPeripheral(),"2\n")
    err = ut.Close()
    S.SetOutputPeripheral(nil)
    ut, err = os.Open(fn)
    if err != nil { return }
    S.SetInputPeripheral(ut)
    var v string
    fmt.Fscanln(S.InputPeripheral(),&v)
    if "Question" != v {
        t.Errorf(messages["shouldntHaveFailed"], "Question")
    }
    fmt.Fscanln(S.InputPeripheral(),&v)
    if "1" != v {
        t.Errorf(messages["shouldntHaveFailed"], "1")
    }
    fmt.Fscanln(S.InputPeripheral(),&v)
    if "2" != v {
        t.Errorf(messages["shouldntHaveFailed"], "2")
    }
    ut.Close()
    S.SetInputPeripheral(nil)
}

func TestMemoPeripheral (t *testing.T) {
    A := createIOAutomat()
    S := A.StateByID("Memo")
    if S.InputPeripheral() == os.Stdin {
        t.Errorf(messages["shouldHaveFailed"], "Input Memo as Stdin")
    }
    if S.InputPeripheral() == os.Stdout {
        t.Errorf(messages["shouldHaveFailed"], "Input Memo as Stdout")
    }
    if S.OutputPeripheral() == os.Stdin {
        t.Errorf(messages["shouldHaveFailed"], "Output Memo as Stdin")
    }
    if S.OutputPeripheral() == os.Stdout {
        t.Errorf(messages["shouldHaveFailed"], "Output Memo as Stdout")
    }
    var v string
    fmt.Fscanln(S.InputPeripheral(),&v)
    if "2" != v {
        t.Errorf(messages["shouldntHaveFailed"], "2")
    }
    S.SetZoneID("")
    A.Memo().DelZone("2")
    if _, err := S.OutputPeripheral().Write([]byte{'1'}) ; err != io.EOF {
        t.Errorf(messages["shouldntHaveFailed"], "Write 1")
    }
}

func TestExitState (t *testing.T) {
    EXITSTATE.SetID("anyID")
    if "anyID" == EXITSTATE.ID() {
        t.Errorf(messages["shouldntHaveFailed"], "Change ID EXITSTATE")
    }
    EXITSTATE.SetText("anyText")
    if "anyText" == EXITSTATE.Text() {
        t.Errorf(messages["shouldntHaveFailed"], "Change Text EXITSTATE")
    }
    da := NewDefaultAnswer(EXITSTATE)
    EXITSTATE.SetDefaultAnswer(da)
    if  da == EXITSTATE.DefaultAnswer() {
        t.Errorf(messages["shouldntHaveFailed"], "Change DefaultAnswer EXITSTATE")
    }
    EXITSTATE.SetPrologID("anyID")
    if "anyID" == EXITSTATE.PrologID() {
        t.Errorf(messages["shouldntHaveFailed"], "Change PrologID EXITSTATE")
    }
    EXITSTATE.SetEpilogID("anyID")
    if "anyID" == EXITSTATE.EpilogID() {
        t.Errorf(messages["shouldntHaveFailed"], "Change EpilogID EXITSTATE")
    }
    EXITSTATE.SetZoneID("anyID")
    if "anyID" == EXITSTATE.ZoneID() {
        t.Errorf(messages["shouldntHaveFailed"], "Change ZoneID EXITSTATE")
    }
    EXITSTATE.SetInput(INPUT)
    if INPUT.Is(EXITSTATE.Input()) {
        t.Errorf(messages["shouldntHaveFailed"], "Change Input EXITSTATE")
    }
    EXITSTATE.SetInputPeripheral(os.Stdin)
    if  os.Stdin == EXITSTATE.InputPeripheral() {
        t.Errorf(messages["shouldntHaveFailed"], "Change InputPeripheral EXITSTATE")
    }
    EXITSTATE.SetOutput(OUTPUT)
    if OUTPUT.Is(EXITSTATE.Output()) {
        t.Errorf(messages["shouldntHaveFailed"], "Change Output EXITSTATE")
    }
    EXITSTATE.SetOutputPeripheral(os.Stdout)
    if os.Stdout == EXITSTATE.OutputPeripheral() {
        t.Errorf(messages["shouldntHaveFailed"], "Change OutputPeripheral EXITSTATE")
    }
    sq := NewSimpleQuestion(EXITSTATE)
    EXITSTATE.SetQuestion(sq)
    if  sq == EXITSTATE.Question() {
        t.Errorf(messages["shouldntHaveFailed"], "Change DefaultAnswer EXITSTATE")
    }
}