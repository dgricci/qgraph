package types

// FTask type for defining an API to invoke tasks.
// It is a function with two parameters and returning an `error` or nil if
// task completes correctly :
// * a pointer to the node this task is running ;
// * a response given to this node.
type FTask func (s *State, r interface{}) error
