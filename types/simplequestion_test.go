package types

import (
  "testing"
)

func TestIntervalInt ( t *testing.T ) {
    sq := NewSimpleQuestion(EXITSTATE) // just for testing !
    if sq.IntervalToString() != "" {
        t.Errorf("Unexpected interval definition got '%s', expected ''", sq.IntervalToString())
    }
    if sq.IsInInterval(int64(0)) {
        t.Errorf("Not an interval, expected not in !")
    }
    sq.SetResponseType(IInINTERVAL)
    sq.SetInterval(true,int64(1),int64(0),true)
    if sq.Check() == nil {
        t.Errorf("Invalid interval : '%s'", sq.IntervalToString())
    }
    sq.SetInterval(true,int64(1),int64(1),true)
    if sq.Check() == nil {
        t.Errorf("Invalid interval : '%s'", sq.IntervalToString())
    }
    sq.SetInterval(false,int64(1),int64(2),false)
    if sq.Check() == nil {
        t.Errorf("Invalid interval : '%s'", sq.IntervalToString())
    }
    sq.SetInterval(false,int64(1),int64(2),true)
    if sq.Check() != nil {
        t.Errorf("Interval should be valid : '%s'", sq.IntervalToString())
    }
    if sq.IsInInterval(int64(0)) {
        t.Errorf("1 should not be in '%s", sq.IntervalToString())
    }
    if sq.IsInInterval(int64(1)) {
        t.Errorf("1 should not be in '%s", sq.IntervalToString())
    }
    if sq.IsInInterval(int64(3)) {
        t.Errorf("3 should not be in '%s", sq.IntervalToString())
    }
    if sq.IntervalToString() != "]1..2]" {
        t.Errorf("Expected ']1..2]', got '%s'", sq.IntervalToString())
    }
}

func TestIntervalFloat ( t *testing.T ) {
    sq := NewSimpleQuestion(EXITSTATE) // just for testing !
    sq.SetResponseType(DInINTERVAL)
    sq.SetInterval(true,1.0,0.0,true)
    if sq.Check() == nil {
        t.Errorf("Invalid interval : '%s'", sq.IntervalToString())
    }
    sq.SetInterval(true,1.0,1.0,true)
    if sq.Check() == nil {
        t.Errorf("Invalid interval : '%s'", sq.IntervalToString())
    }
    sq.SetInterval(false,1.0,2.0,false)
    if sq.Check() != nil {
        t.Errorf("Interval should be valid : '%s'", sq.IntervalToString())
    }
    sq.SetInterval(false,1.0,2.0,true)
    if sq.Check() != nil {
        t.Errorf("Interval should be valid : '%s'", sq.IntervalToString())
    }
    if sq.IsInInterval(0.0) {
        t.Errorf("1 should not be in '%s", sq.IntervalToString())
    }
    if sq.IsInInterval(1.0) {
        t.Errorf("1 should not be in '%s", sq.IntervalToString())
    }
    if sq.IsInInterval(3.0) {
        t.Errorf("3 should not be in '%s", sq.IntervalToString())
    }
    if sq.IntervalToString() != "]1..2]" {
        t.Errorf("Expected ']1..2]', got '%s'", sq.IntervalToString())
    }
}
