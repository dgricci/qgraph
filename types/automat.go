package types

import (
    "fmt"

    "gitlab.com/dgricci/qgraph/util"
)

// Automat type for the Petri-graph.
//
type Automat struct {
    states      map[string]*State   `yaml:"States"`             // States (nodes) of this automaton
    queue       *Queue              `yaml:"Queue"`              // Last in, First out visited states list
    memo        *Memo               `yaml:"Memo"`               // Memorization zones (placeholder for data storage)
    nodes       []string                                        // Ordered list of States (order of discovery)
    tasks       map[string]FTask                                // list of tasks
}

// NewAutomat function initializes an `Automat` struct.
//
func NewAutomat (tasks map[string]FTask) *Automat {
    if tasks == nil { tasks= map[string]FTask{} }

    return &Automat{
        states  : make(map[string]*State),
        queue   : NewQueue(),
        memo    : NewMemo(),
        nodes   : make([]string,0),
        tasks   : tasks,
    }
}

// States method returns a filtered states' automaton.
// One can pass a filter func to select states.
//
func (a *Automat) States (filter ...func (*State) bool) []string {
    filterFunc := NoFilter
    if len(filter) > 0 {
        filterFunc= filter[0]
    }
    r := make([]string,0)
    for _, sid := range a.nodes {
        if filterFunc(a.StateByID(sid)) {
            r = append(r, sid)
        }
    }
    return r
}

// NoFilter func is a filter that accept all states
//
func NoFilter (e *State) bool {
    return true
}

// FirstOnly func is a filter that only accept the first state of the automaton
//
func FirstOnly (e *State) bool {
   return e.ID() == e.Automat().nodes[0]
}

// StateByID method returns a given State's automaton.
// `id` is the `State` identifier.
// Returns `nil` if the state does not exist.
//
func (a *Automat) StateByID (id string) *State {
    if id == "" {
        return EXITSTATE
    }
    return a.states[id]
}

// SetState method adds a state to automaton.
//
func (a *Automat) SetState (s *State) {
    a.states[s.id]= s
    a.nodes= append(a.nodes, s.id)
}

// Queue method returns the automaton's queue.
//
func (a *Automat) Queue () *Queue {
    return a.queue
}

// Memo method returns the automaton's storage.
func (a *Automat) Memo () *Memo {
    return a.memo
}

// Tasks method returns the automaton's tasks.
//
func (a *Automat) Tasks () map[string]FTask {
    return a.tasks
}

// Check method verifies information stored in the automaton.
// It checks what follows :
// * There is at least one State ;
// * All `Next` fields must point at an existing State `Id` ;
// * All tasks are defined ;
//
func (a *Automat) Check () error {
    var err error

    if len(a.nodes) == 0 {
        return fmt.Errorf(util.T("noStatesFound"))
    }
    for _, sid := range a.nodes {
        if err = a.states[sid].Check() ; err != nil {
            return err
        }
    }
    if err= a.Queue().Check(a) ; err != nil {
        return err
    }
    return nil
}
