package types

import (
    "fmt"

    "gitlab.com/dgricci/qgraph/util"
)

// DefaultAnswer type for ... optional answer !
//
type DefaultAnswer struct {
    task    string                  `yaml:"Task"`               // function to launch when no answer has been given
    next    string                  `yaml:"Next"`               // identifier of the next State ; when "" exits the Automaton
    s       *State                                              // the current state
}

// NewDefaultAnswer function initializes a `DefaultAnswer` struct.
//
func NewDefaultAnswer (s *State) *DefaultAnswer {
    return &DefaultAnswer{
        task    :   "",
        next    :   "",
        s       :   s,
    }
}

// IsChoice method returns `false` for `DefaultAnswer`.
//
func (da *DefaultAnswer) IsChoice () bool {
    return false
}

// Task method returns the type FTask to run or `nil` if none.
//
func (da *DefaultAnswer) Task () FTask {
    return da.s.a.tasks[da.task]
}

// SetTaskID method assigns the task identifier.
//
func (da *DefaultAnswer) SetTaskID (t string) {
    da.task= t
}

// TaskID method returns the task identifier.
//
func (da *DefaultAnswer) TaskID () string {
    return da.task
}

// NextState method returns the `State` to go after the current one.
// When `nil` is returned, one leaves the automaton.
//
func (da *DefaultAnswer) NextState () *State {
    if da.next == "" {
        return EXITSTATE
    }
    return da.s.a.states[da.next]
}

// SetNextStateID method assigns the next `State` by its identifier to reach.
//
func (da *DefaultAnswer) SetNextStateID (s string) {
    da.next= s
}

// NextStateID method returns the next `State` by its identifier to reach.
//
func (da *DefaultAnswer) NextStateID () string {
    return da.next
}

// AnswerState method returns the current `State`.
//
func (da *DefaultAnswer) AnswerState () *State {
    return da.s
}

// Check verifies the `DefaultAnswer` content
// It checks what follows :
// * `Next` field must point at an existing State `Id` ;
// * `Task` is defined ;
func (da *DefaultAnswer) Check () error {
    sn := da.NextState()
    if sn == nil {
        return fmt.Errorf(util.T("nextStateDefaultAnswerNotDefined"), da.NextStateID(), da.AnswerState().ID())
    }
    if da.TaskID() != "" && da.Task() == nil {
        return fmt.Errorf(util.T("unknownReferencedTaskDefaultAnswer"), da.TaskID(), da.AnswerState().ID())
    }
    return nil
}
