package types

import (
    "testing"
    "fmt"
    "os"

    qu "gitlab.com/dgricci/qgraph/util"
)

var (
    messages = map[string]string{
        "shouldntHaveFailed":   "Unexpected failure with '%s'",
        "shouldHaveFailed"  :   "Unexpected success with '%s'",
    }
)

// createSimpleAutomat function : helper function for examples
//
func createSimpleAutomat() *Automat {
    A := NewAutomat(map[string]FTask{
        "Banner"    :   func (s *State, r interface{}) error {
            fmt.Println("Welcome ...")
            return nil
        },
        "Check"     :   func (s *State, r interface{}) error {
            fmt.Printf("Checking ... '%s' !\n", r.(string))
            return nil
        },
        "Greetings" :   func (s *State, r interface{}) error {
            fmt.Printf("Hello '%s' !\n", r.(string))
            return nil
        },
    })
    S := NewState(A)
    S.SetID("Login")
    S.SetText("What's is your identifier ?")
    DA := NewDefaultAnswer(S)
    DA.SetNextStateID("Login")
    S.SetDefaultAnswer(DA)
    S.SetPrologID("Banner")
    S.SetZoneID("LID")
    SQ := NewSimpleQuestion(S)
    SQ.SetNextStateID("Profile")
    S.SetQuestion(SQ)
    A.SetState(S)

    S = NewState(A)
    S.SetID("Profile")
    S.SetText("Choose your profile")
    S.SetZoneID("PID")
    MCQ := NewChoicesQuestion(S)
    C := NewChoice(S)
    C.SetText("User")
    C.SetMemorize(false)
    C.SetNextStateID("Hello")
    MCQ.AddChoice(C)
    C = NewChoice(S)
    C.SetText("Admin")
    C.SetMemorize(true)
    C.SetNextStateID("Hello")
    MCQ.AddChoice(C)
    S.SetQuestion(MCQ)
    A.SetState(S)

    S = NewState(A)
    S.SetID("Hello")
    S.SetZoneID("LID")
    S.SetInput(MEMOBUFFER)
    S.SetEpilogID("Greetings")
    SQ = NewSimpleQuestion(S)
    SQ.SetTaskID("Check")
    S.SetQuestion(SQ)
    A.SetState(S)

    return A
}

func TestTranslation (t *testing.T) {
    // qgraph/util
    if qu.T("supportedLanguage") == "supportedLanguage" {
        t.Errorf(messages["shouldntHaveFailed"], "supportedLanguage")
    }
    // qgraph/types
    if qu.T("noStatesFound") == "noStatesFound" {
        t.Errorf(messages["shouldntHaveFailed"], "noStatesFound")
    }
}

func TestAll(t *testing.T) {
    var err error
    A := NewAutomat(map[string]FTask{
        "doSomething" :   func (s *State, r interface{}) error {
            fmt.Println("Done !")
            return nil
        },
        "checkFirst"  :   func (s *State, r interface{}) error {
            fmt.Println("Checked !")
            return nil
        },
        "computeIt"   :   func (s *State, r interface{}) error {
            fmt.Println("Computed !")
            return nil
        },
        "processIt"   :   func (s *State, r interface{}) error {
            fmt.Println("Processed !")
            return nil
        },
        "workIt"      :   func (s *State, r interface{}) error {
            fmt.Println("Worked !")
            return nil
        },
    })
    S := NewState(A)
    S.SetID("ID1")
    S.SetText("Question's text")
    DA := NewDefaultAnswer(S)
    DA.SetTaskID("doSomething")
    DA.SetNextStateID("ID2")
    S.SetDefaultAnswer(DA)
    S.SetPrologID("checkFirst")
    S.SetEpilogID("computeIt")
    S.SetZoneID("ZID1")
    S.SetInput(INPUT)
    S.SetOutput(OUTPUT)
    SQ := NewSimpleQuestion(S)
    SQ.SetResponseType(DBL)
    SQ.SetResponseFuzziness(0)
    SQ.SetResponseUnit("m")
    SQ.SetInterval(false,1,2,false)
    SQ.SetNextStateID("ID2")
    S.SetQuestion(SQ)
    A.SetState(S)

    S = NewState(A)
    S.SetID("ID2")
    S.SetText("Choose amongst")
    S.SetZoneID("ZID2")
    MCQ := NewChoicesQuestion(S)
    MCQ.SetDisplay(true)
    MCQ.SetLearn(false)
    MCQ.SetTaskID("workIt")
    C := NewChoice(S)
    C.SetText("First choice")
    C.SetMemorize(true)
    C.SetTaskID("processIt")
    C.SetNextStateID("ID1")
    MCQ.AddChoice(C)
    C = NewChoice(S)
    C.SetText("")
    C.SetMemorize(false)
    C.SetTaskID("processIt")
    C.SetNextStateID("") // exit state
    MCQ.AddChoice(C)
    S.SetQuestion(MCQ)
    A.SetState(S)

    S = NewState(A)
    S.SetID("ID3")
    S.SetZoneID("ZID2")
    S.SetInput(MEMOBUFFER)
    S.SetOutput(NONE)
    SQ = NewSimpleQuestion(S)
    SQ.SetResponseType(INT)
    SQ.SetTaskID("workIt")
    SQ.SetNextStateID("ID1")
    S.SetQuestion(SQ)
    A.SetState(S)

    A.Queue().Append("ID1")
    A.Queue().Append("ID2")
    A.Queue().Append("ID1")

    A.Memo().SetZone("ZID1","1.5")
    A.Memo().SetZone("ZID2","1")

    if err = A.Check() ; err != nil {
        t.Errorf(messages["shouldntHaveFailed"], "all cases : "+err.Error())
    }

    S = A.StateByID("ID1")
    if S.PrologID() != "checkFirst" {
        t.Errorf(messages["shouldntHaveFailed"], "checkFirst")
    }
    if S.EpilogID() != "computeIt" {
        t.Errorf(messages["shouldntHaveFailed"], "computeIt")
    }
    DA = S.DefaultAnswer().(*DefaultAnswer)
    if DA.TaskID() != "doSomething" {
        t.Errorf(messages["shouldntHaveFailed"], "doSomething")
    }
    DA.SetNextStateID("ID4")
    if A.Check() == nil {
        t.Errorf(messages["shouldHaveFailed"], "ID4 undefined")
    }
    DA.SetNextStateID("ID2")

    SQ = S.Question().(*SimpleQuestion)
    SQ.SetNextStateID("ID4")
    if A.Check() == nil {
        t.Errorf(messages["shouldHaveFailed"], "ID4 undefined")
    }
    SQ.SetNextStateID("ID1")

    S = A.StateByID("ID3")
    SQ = S.Question().(*SimpleQuestion)
    SQ.SetTaskID("fakeTask")
    if A.Check() == nil {
        t.Errorf(messages["shouldHaveFailed"], "ID3.fakeTask undefined")
    }
    SQ.SetTaskID("workIt")

    MCQ.SetTaskID("fakeTask")
    if A.Check() == nil {
        t.Errorf(messages["shouldHaveFailed"], "ID2.fakeTask undefined")
    }
    MCQ.SetTaskID("workIt")

    for _, c := range MCQ.Choices() {
        switch c.(*Choice).Text() {
        case "First choice" :
            c.(*Choice).SetNextStateID("ID5")
            if A.Check() == nil {
                t.Errorf(messages["shouldHaveFailed"], "ID5 undefined")
            }
            if c.(*Choice).TaskID() != "processIt" {
                t.Errorf(messages["shouldntHaveFailed"], "processIt")
            }
            c.(*Choice).SetNextStateID("ID1")
        default             :
            if c.(*Choice).NextState() != EXITSTATE {
                t.Errorf(messages["shouldntHaveFailed"], "EXITSTATE at c")
            }
        }
    }
    if A.StateByID("") != EXITSTATE {
        t.Errorf(messages["shouldntHaveFailed"], "EXITSTATE at A")
    }
}

func setup () {
    qu.SetLang("en")
}

func teardown() {
}

func TestMain(m *testing.M) {
    // call flag.Parse() here if TestMain uses flags
    setup()

    res := m.Run()

    teardown()

    os.Exit(res)
}
