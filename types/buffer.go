package types

import (
    "io"
    "fmt"
)

// Buffer type as storage zone.
//
type Buffer struct {
    data    interface{}             `yaml:"Buff"`               // stored data
    offset  int                                                 // See Read()
}

// NewBuffer function initializes a `Buffer` struct.
//
func NewBuffer () *Buffer {
    return &Buffer{
        data:   nil,
        offset: 0,
    }
}

// Value returns the value.
//
func (b *Buffer) Value () interface{} {
    return b.data
}

// Reset method resets the buffer to be empty.
//
func (b *Buffer) Reset () {
    b.offset = 0
}

// Read method implements the Reader interface.
//
func (b *Buffer) Read (p []byte) (n int, err error) {
    if b == nil || b.data == nil {
        return 0, io.EOF
    }
    switch b.data.(type) {
    case string :
        if b.offset >= len(b.data.(string)) {
            b.Reset()
            if len(p) == 0 { return }
            return 0, io.EOF
        }
        n = copy(p, []byte(b.data.(string))[b.offset:])
        b.offset += n
    case []byte :
        if b.offset >= len(b.data.([]byte)) {
            b.Reset()
            if len(p) == 0 { return }
            return 0, io.EOF
        }
        n = copy(p, b.data.([]byte)[b.offset:])
        b.offset += n
    }
    return
}

// SetValue method assigns a value of type `string` or `[]byte`.
//
func (b *Buffer) SetValue (val interface{}) {
    if b.data != nil {
        b.data= nil
    }
    b.Reset()
    switch val.(type) {
    case string :
        if len(val.(string)) > 0 {
            b.data = val.(string)
        }
    case []byte :
        if len(val.([]byte)) > 0 {
            data := make([]byte,len(val.([]byte)))
            copy(data, val.([]byte))
            b.data= data
        }
    case int, int32, int64, float32, float64, bool :
        v := fmt.Sprintf("%v", val)
        b.data = v
    default     :
        return
    }
}

// Write method implements the Writer interface.
//
func (b *Buffer) Write (p []byte) (n int, err error) {
    if b == nil { return 0, io.EOF }
    b.SetValue(p)
    if p == nil { return 0, nil }
    return len(p), nil
}
