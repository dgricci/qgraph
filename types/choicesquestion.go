package types

import (
    "fmt"

    qu "gitlab.com/dgricci/qgraph/util"
)

// ChoicesQuestion type for multiple choices question.
//
type ChoicesQuestion struct {
    displC  bool                    `yaml:"Display"`            // display choices ? defaults to true
    learnC  bool                    `yaml:"Learn"`              // learn new choice ? defaults to false
    task    string                  `yaml:"Task"`               // optional task
    choices []Answer                `yaml:"Choices"`            // eventual choices
    s       *State                                              // the current state
}

// NewChoicesQuestion function initializes a `ChoicesQuestion` struct.
//
func NewChoicesQuestion (s *State) *ChoicesQuestion {
    return &ChoicesQuestion{
        displC  :   true,
        learnC  :   false,
        task    :   "",
        choices :   make([]Answer,0),
        s       :   s,
    }
}

// IsSimple method returns `false` for `ChoicesQuestion`.
//
func (cq *ChoicesQuestion) IsSimple () bool {
    return false
}

// Task method returns the type FTask to run or `nil` if none.
//
func (cq *ChoicesQuestion) Task () FTask {
    return cq.s.a.tasks[cq.task]
}

// SetTaskID method assigns the task identifier.
//
func (cq *ChoicesQuestion) SetTaskID (t string) {
    cq.task= t
}

// TaskID method returns the task identifier.
//
func (cq *ChoicesQuestion) TaskID () string {
    return cq.task
}

// NextState method returns the `State` to go after the current one.
// For `ChoicesQuestion`, it depends on `Choice` made. So, it always
// returns `nil`
//
func (cq *ChoicesQuestion) NextState () *State {
    return nil
}

// QuestionState method returns the current `State`.
//
func (cq *ChoicesQuestion) QuestionState () *State {
    return cq.s
}

// Display method returns `true` if the choices are to be displayed, `false`
// otherwise.
//
func (cq *ChoicesQuestion) Display () bool {
    return cq.displC
}

// SetDisplay method assigns whether or not display the choices.
//
func (cq *ChoicesQuestion) SetDisplay (d bool) {
    cq.displC = d
}

// Learn method returns `true` if one has to take the answer has a new choice,
// `false` otherwise.
//
func (cq *ChoicesQuestion) Learn () bool {
    return cq.learnC
}

// SetLearn assigns whether or not adding a new choice to this multiple
// choices question.
//
func (cq *ChoicesQuestion) SetLearn (l bool) {
    cq.learnC = l
}

// Choices method returns the eventual answers to this multiple choices
// question.
//
func (cq *ChoicesQuestion) Choices (filter ...func (Answer) bool) []Answer {
    filterFunc := func(a Answer) bool {
        return true
    }
    if len(filter) > 0 {
        filterFunc= filter[0] 
    }
    r := make([]Answer,0)
    for _, c := range cq.choices {
        if filterFunc(c) {
            r = append(r, c)
        }
    }
    return r
}

// AddChoice method adds a `Choice` to this multiple choices question.
//
func (cq *ChoicesQuestion) AddChoice (c Answer) {
    cq.choices = append(cq.choices, c)
}

// Check method verifies the `ChoicesQuestion` content.
// It checks what follows :
// * there is a least one choice ;
// * each `Choice` is valid.
func (cq *ChoicesQuestion) Check () error {
    var err error

    if cq.TaskID() != "" && cq.Task() == nil {
        return fmt.Errorf(qu.T("unknownReferencedTask"), cq.TaskID(), cq.QuestionState().ID())
    }
    cqc := cq.Choices()
    if len(cqc) == 0 {
        return fmt.Errorf(qu.T("noChoicesDefined"), cq.QuestionState().ID())
    }
    for _, c := range cqc {
        if c.IsChoice() {
            if err= c.(*Choice).Check() ; err != nil {
                return err
            }
        }
    }
    return nil
}
