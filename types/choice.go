package types

import (
    "fmt"

    qu "gitlab.com/dgricci/qgraph/util"
)

// Choice type for ... a given choice !
//
type Choice struct {
    ToC     string                  `yaml:"ToC"`                // text of choice
    memo    bool                    `yaml:"Memorize"`           // memorize choice ?
    task    string                  `yaml:"Task"`               // function to launch when no answer has been given
    next    string                  `yaml:"Next"`               // identifier of the next State ; when "" exits the Automaton
    s       *State                                              // the current state
}

// NewChoice function initializer a `Choice` struct.
//
func NewChoice (s *State) *Choice {
    return &Choice{
        ToC :   "",
        memo:   false,
        task:   "",
        next:   "",
        s   :   s,
    }
}

// IsChoice method returns `true` for `Choice`
//
func (c *Choice) IsChoice () bool {
    return true
}

// Task method returns the type FTask to run or `nil` if none.
//
func (c *Choice) Task () FTask {
    return c.s.a.tasks[c.task]
}

// SetTaskID method assigns the task identifier.
//
func (c *Choice) SetTaskID (t string) {
    c.task= t
}

// TaskID method returns the task identifier.
//
func (c *Choice) TaskID () string {
    return c.task
}

// NextState method returns the `State` to go after the current one.
// When `nil` is returned, one leaves the automaton.
//
func (c *Choice) NextState () *State {
    if c.next == "" {
        return EXITSTATE
    }
    return c.s.a.states[c.next]
}

// SetNextStateID method assigns the next `State` by its identifier to reach.
//
func (c *Choice) SetNextStateID (s string) {
    c.next= s
}

// NextStateID method returns the next `State` by its identifier to reach.
//
func (c *Choice) NextStateID () string {
    return c.next
}

// AnswerState method returns the current `State`.
//
func (c *Choice) AnswerState () *State {
    return c.s
}

// Text method returns the text of choice.
//
func (c *Choice) Text () string {
    return c.ToC
}

// SetText method assigns the choice's text.
//
func (c *Choice) SetText (t string) {
    c.ToC = t
}

// Memorize method indicates whether or not to store the answer for this
// choice.
//
func (c *Choice) Memorize () bool {
    return c.memo
}

// SetMemorize method assigns whether or not to store the answer for this
// choice.
//
func (c *Choice) SetMemorize (m bool) {
    c.memo = m
}

// Check method verifies the `Choice` content.
// It checks what follows :
// * `Next` state exists ;
// * `Task` is valid.
func (c *Choice) Check () error {
    if c.NextState() == nil {
        return fmt.Errorf(qu.T("nextStateNotDefined"), c.NextStateID(), c.AnswerState().ID())
    }
    if c.TaskID() != "" && c.Task() == nil {
        return fmt.Errorf(qu.T("unknownReferencedTask"), c.TaskID(), c.AnswerState().ID())
    }
    return nil
}
