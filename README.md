% Q-Graph : moteur de dialogue automatisé  
% Didier Richard basé sur le cours du Prof. François Bouillé (1995)  
% 04/01/2018

---

Révision : 
* 28/05/2017 : initialisation du projet
* 04/01/2018 : passage à dep et usage de Makefile

---

# Objectif #

Apprendre le langage golang en mettant en œuvre un concept ancien !

# Qu'est-ce qu'un dialogue ? #

Réaliser une interface conviviale avec les caractéristiques suivantes :

* facilité de réalisation et de maintenance ;
* indépendance du dialogue avec le système ;
* indépendance du dialogue avec la langue utilisée (on doit pouvoir en
  changer).

Pour cela on utilise un **moteur de dialogue** dont l'algorithme est
indépendant du système. Ce moteur travaille sur diverses données qui
représentent l'enchaînement des questions et des réponses : on appelle cela un
**automate**.

## Q-Graphes ##

Le Q-Graphe est l'outil théorique de représentation des dialogues.
Le Q-Graphe est un cas particulier de **Graphe bipartie**.

Un graphe bipartie est un graphe comportant deux ensembles de sommets S1 et
S2 et deux ensembles d'arcs A1 et A2, tels que les arcs de A1 vont d'un sommet
de S1 à un sommet de S2, tandis que les arcs de A2 vont d'un sommet de S2 à un
sommet de S1.

Dans un Q-Graphe, l'un des ensembles de sommets représente les **questions**
et l'autre ensemble de sommets représente les **réponses**.

On distingue deux types de questions :

* les _questions simples_ pour lesquelles le système attend une réponse de
l'utilisateur comme un chaîne de caractères, un entier, un flottant, etc ...
  - _Exemples :_
    + Quel est votre nom ? _une chaîne de caractères est attendue_
    + Quel est votre âge ? _un entier est attendu_
    + Quel est votre taille ? _un flottant est attendu_
* les _questions à choix multiples_ pour lesquelles le système attend une
réponse parmi plusieurs.
  - _Exemple :_
    + Quelle est votre situation de famille ? _1, 2, 3 ou 4 est attendu_
      1. Célibataire
      2. Marié
      3. Union libre
      4. Divorcé

Le Q-Graphe permet de modéliser l'enchaînement des questions à poser tout au long du dialogue.

Après une question à choix multiples (QCM), il est possible d'enchaîner vers des questions différentes
en fonction de la réponse donnée.

Les cercles symbolisant les questions s'appellent des **Places** ou **États**.

Les traits symbolisant les réponses s'appellent des **Transitions**.

![Q-Graphe](./img/fig1.png "Exemple d'un Q-Graphe")

## Questions (QCM) ##

À chaque question simple (QS) ou à choix multiples (QCM), il est possible
d'associer divers paramètres :

* des tâches prologue, épilogue ;
* le texte de la question ;
* les textes des choix (QM) ;
* le périphérique de restitution des textes pour la question ;
* la possibilité d'apprentissage (QCM) permet d'ajouter un nouveau choix ;
* le périphérique d'acquisition de la réponse ;
* la mémorisation de la réponse, fonction ou pas des choix si besoin ;
* une tâche liée à l'état ;
* une tâche liée à chaque choix si besoin ;
* la possibilité de l'absence d'une réponse, d'une tâche associée à cette
  absence de réponse ;
* l'état suivant lié à la question (QS), à un choix (QCM) ou à l'absence de
  réponse.

![État](./img/fig2.png "Une question d'un Q-Graphe")

...

# Installation #

## Dépendences ##

Il est nécessaire d'avoir installé [go](https://golang.org/dl/) et
[glide](https://github.com/Masterminds/glide/releases).

## Compilation ##

Pour compiler cette bibliothèque, il suffit de lancer :

```bash
$ cd $GOPATH/src
$ git clone https://gitlab.com/dgricci/qgraph.git ./gitlab.com/dgricci/qgraph
$ cd ./gitlab.com/dgricci/qgraph && make
```

