// Package qgraph implements a simple Petri-Net engine.
package qgraph

import (
    "fmt"
    "os"
    "github.com/nicksnyder/go-i18n/i18n"

    qu "gitlab.com/dgricci/qgraph/util"
    qt "gitlab.com/dgricci/qgraph/types"
    qo "gitlab.com/dgricci/qgraph/io"
    qy "gitlab.com/dgricci/qgraph/io/yaml"
)

// QGraph holds the serialized form of the Petri-net
//
type QGraph struct {
    fileName    string
    petriNet    *qt.Automat
}

// NewQGraph function initializes an `QGraph` struct.
// If path is "" then the Automat object is created empty.
//
func NewQGraph (path string, tasks map[string]qt.FTask) *QGraph {
    q := &QGraph{
        fileName    :   path,
        petriNet    :   nil,
    }
    if path == "" {
        q.SetAutomat(qt.NewAutomat(tasks))
    } else {
        if _, err := os.Stat(path) ; !os.IsNotExist(err) {
            q.SetAutomat(qo.NewAutomatFromFile(path,tasks))
        } else {
            q.SetAutomat(qy.NewAutomatFromYAML(path,tasks))
        }
    }
    if q.Automat() == nil { return nil }
    return q
}

// Run method launches the `QGraph` execution.
//
func (q *QGraph) Run ( fromID string, opts ...map[string]string ) error {
    var s0 *qt.State
    var err error
    if s0, err = q.startState(fromID) ; s0 == nil {
        _ = qu.LogOnError(err)
        return err
    }

    var sn *qt.State
    var tsk qt.FTask
    var resp interface{}
    //var chc *qt.Choice
    //mm := s0.Automat().Memo()
    hq := s0.Automat().Queue()
    for s0 != qt.EXITSTATE {

        hq.Append(s0.ID()) // current state in history
        // PROLOG
        if tsk = s0.Prolog() ; tsk != nil {
            if err = tsk(s0,nil) ; err != nil {
                _ = qu.LogOnError(fmt.Errorf(qu.T("execPrologFailed"), err))
            }
        }

read_again:
        // DISPLAY QUESTION
        q.DisplayQuestion(s0)

        // READ ANSWER
        if resp = q.ReadAnswer(s0) ; resp == nil {
            if da := s0.DefaultAnswer() ; da != nil {
                if tsk = da.Task() ; tsk != nil {
                    if err = tsk(s0,nil) ; err != nil {
                        _ = qu.LogOnError(fmt.Errorf(qu.T("execTaskFailed"), err))
                    }
                }
                hq.Append(s0.ID())
                s0= da.NextState()
                continue
            }
            _ = qu.LogOnError(fmt.Errorf(qu.T("expectedAnswer")))
            if s0.Text() != "" {
                goto read_again
            }
            // under condition state ...
            err = fmt.Errorf(qu.T("underConditionStateNoAnswerFound", s0.ID()))
            _ = qu.LogOnError(err)
            return err
        }

        // COMMAND
        if sn, err = q.ExecCommand(s0, resp) ; err != nil {
            _ = qu.LogOnError(err)
            goto read_again
        }
        if sn != nil {
            s0 = sn
            continue
        }

        // CHECK ANSWER
        if resp, err = q.ConvertAnswer(s0, resp) ; err != nil {
            _ = qu.LogOnError(err)
            goto read_again
        }

        // NEXT STATE
        sn= s0
        if sn.Question().IsSimple() {
            s0 = sn.Question().NextState()
        } else {
            // choice TODO(dgricci)
            s0 = qt.EXITSTATE
        }
    }
    return fmt.Errorf(qu.T("notYetCompletelyImplemented"))
}

// startState method retrieves the starting node of the petri-net or
// `nil` if non found. In the later case, `err` contains the reason why.
//
// When `tryFirst` parameter is set to `true` then the method tries to find
// the first node declared in the automat.
//
func (q *QGraph) startState ( fromID string, tryFirst ...bool ) (s0 *qt.State, err error) {
    a := q.Automat()
    if a == nil {
        err = fmt.Errorf(qu.T("petrinetCreationFailed"))
        return nil, err
    }
    if fromID == "" {// try using last visited state and pop it!
        fromID = a.Queue().Pop()
    }
    s0 = a.StateByID(fromID)
    if (s0 == nil || s0 == qt.EXITSTATE) && len(tryFirst) > 0 && tryFirst[0] {
        ls := a.States(qt.FirstOnly)
        if len(ls) > 0 {
            s0 = a.StateByID(ls[0])
        } else {
            s0 = qt.EXITSTATE // no State found in automat
        }
    }
    if s0 == qt.EXITSTATE {
        return nil, fmt.Errorf(qu.T("startStateAsExitState"))
    }
    if s0 == nil {
        return nil, fmt.Errorf(qu.T("startStateNotFound"))
    }
    return s0, nil
}

// Filename method returns the placeholder of the automaton.
//
func (q *QGraph) Filename () string {
    return q.fileName
}

// SetFilename methods assigns the name of the file that
// will be used to serialize the automaton.
func (q *QGraph) SetFilename (f string) {
    q.fileName = f
}

// Automat method returns the inner automaton.
//
func (q *QGraph) Automat () *qt.Automat {
    return q.petriNet
}

// SetAutomat method assigns the inner automaton.
//
func (q *QGraph) SetAutomat ( a *qt.Automat ) {
    q.petriNet = a
}

// String method returns the string representation of the automaton.
// The data structure of the automaton is YAML encoded. The string representation
// is as follows :
//
// "fileName (if any)"="YAML encoding"
//
func (q *QGraph) String () string {
    s := `"`+q.Filename()+`"="`
    d, e := qo.SerializeAutomat(q.Automat(),"yaml")
    if e == nil {
        s += string(d)
    }
    s += `"`
    return s
}

// Save method writes all information back to disk.
// If the given argument is set then it represents the file's name,
// otherwise the file's name is the one given at the Automat creation.
// If no file's name has been given at creation time, then os.Stdout is used.
// If the given argument is "" then writes to os.Stdout too.
func (q *QGraph) Save ( fileName ...string ) error {
    if len(fileName) > 0 && fileName[0] != "" {
        return qo.SaveAutomatToFile(q.Automat(), fileName[0])
    }
    if q.Filename() != "" && len(fileName) == 0 {
        return qo.SaveAutomatToFile(q.Automat(), q.Filename())
    }
    // either no fileName provided and inner file's name is empty
    // or fileName==""
    return qo.SaveAutomatToFile(q.Automat()) // to os.Stdout
}

// init function to set global variables
//
func init () {
    i18n.MustLoadTranslationFile("i18n/en-us.all.json")
    i18n.MustLoadTranslationFile("i18n/fr-fr.all.json")

    qu.Log().Println("gitlab.com/dgricci/qgraph loaded")
}
